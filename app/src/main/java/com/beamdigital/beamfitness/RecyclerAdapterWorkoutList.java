package com.beamdigital.beamfitness;

import android.content.Context;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.beamdigital.beamfitness.model.ExerciseEntity;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.List;

public class RecyclerAdapterWorkoutList extends RecyclerView.Adapter<RecyclerAdapterWorkoutList.WorkoutHolder> {

    Context context;
    List<ExerciseEntity> fullList;
    int progress;
    StorageReference storageReference = FirebaseStorage.getInstance().getReference();


    public RecyclerAdapterWorkoutList(Context context, List<ExerciseEntity> fullList) {
        this.context = context;
        this.fullList = fullList;
        this.progress =0;

    }

    @NonNull
    @Override
    public RecyclerAdapterWorkoutList.WorkoutHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.recycler_view_workout_list,parent,false);


        if(viewType == 0)
        {
            WorkoutHolder workoutHolder = new WorkoutHolder(view);
            return workoutHolder;

        }
        else
        {
            view = layoutInflater.inflate(R.layout.recycler_view_workout_list_future,parent,false);
            WorkoutHolder workoutHolder = new WorkoutHolder(view);
            return workoutHolder;
        }

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerAdapterWorkoutList.WorkoutHolder holder, int position) {
        holder.name.setText(""+fullList.get(position).getName());
        holder.duration.setText(""+fullList.get(position).getDuration_min()+"'");


        //INIZIO PEZZO CHE CARICA GIF
        StorageReference ref = storageReference.child("gif/"+fullList.get(position).getVideo());

        ref.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                Glide.with(context).load(uri).dontAnimate().into(holder.pic);
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Glide.with(context).load("https://media.giphy.com/media/nqCH5dsIn99cmSPVrd/giphy.gif").dontAnimate().into(holder.pic);
            }
        });


        if(position==0) {
            holder.progressBarWorkout.setProgress(progress);
        }

    }

    @Override
    public int getItemCount() {
        return fullList.size();

    }


    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public class WorkoutHolder extends RecyclerView.ViewHolder{
        TextView name;
        TextView duration;
        ImageView pic;
        ProgressBar progressBarWorkout;



        public WorkoutHolder(@NonNull View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.exerciseName);
            duration = itemView.findViewById(R.id.exerciseTime);
            pic = itemView.findViewById(R.id.exercisePic);
            progressBarWorkout = itemView.findViewById(R.id.progressBarWorkout);




        }
    }


    public void setProgress(int progress) {
        this.progress=progress;
        notifyDataSetChanged();
    }
    public void setDataset(List<ExerciseEntity> fullList) {
        if(fullList != null || fullList.size()==0) {
            this.fullList = fullList;
            notifyDataSetChanged();
            Log.d("Luca","setDataset andato");
        }
        else {
            Log.d("Luca","andato male");

        }
    }



}
