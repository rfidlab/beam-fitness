package com.beamdigital.beamfitness;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

import eu.beamdigital.beamfitnesslibrary.InfoMuscolo;

@EViewGroup(R.layout.controller_muscolo_dx)
public class ControllerMuscoloDx extends ConstraintLayout implements InfoMuscolo {

    @ViewById(R.id.sottrai)
    TextView decresci;
    @ViewById(R.id.aggiungi)
    TextView cresci;
    @ViewById(R.id.muscolo)
    TextView muscolo;
    @ViewById(R.id.intensita)
    TextView intensita;

    private boolean isActive = false;
    private int intensity = 0;
    private byte code=0;

    public ControllerMuscoloDx(@NonNull Context context) {
        super(context);
    }

    public ControllerMuscoloDx(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public ControllerMuscoloDx(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public ControllerMuscoloDx(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    public String getMuscolo() {
        return muscolo.getText().toString();
    }

    public void setMuscolo(String muscolo) {
        this.muscolo.setText(muscolo);
    }


    private void setIntensity( ) {

        this.intensita.setText(""+intensity);
    }






    @Click(R.id.aggiungi)
    void click_piu(){
        if(intensity + 5 <= 100)
        {
            setIsActive(true);
            intensity=intensity + 5;
            //chiamo setIntensity per scriverlo nella grafica
            setIntensity();
        }
        return;


    }

    @Click(R.id.sottrai)
    void click_meno(){
        if(intensity - 5 >= 0)
        {
            if(intensity-5 == 0){
                setIsActive(false);
            }
            intensity=intensity - 5;
            setIntensity();
        }
        return;

    }











    //LA PARTE RELATIVA ALL'INTERFACE
    @Override
    public byte getTheCode() {
        return code;
    }

    @Override
    public byte getTheIntesity() {
        return (byte)intensity;
    }

    @Override
    public boolean getIsActive() {
        return isActive;
    }

    @Override
    public void setTheCode(int code) {
        this.code = (byte)code;
    }

    @Override
    public void setIsActive(boolean active) {
        this.isActive = active;
    }

    @Override
    public void setIntensity(int val) {
        this.intensity=val;
        this.intensita.setText(""+intensity);

        //in base al valore faccio passare is active a true o false
        setIsActive(val == 0 ? false : true);
    }

}
