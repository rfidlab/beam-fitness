package com.beamdigital.beamfitness;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import eu.beamdigital.beamfitnesslibrary.BluetoothFitnessClient;

public class DemoActivityConnection extends AppCompatActivity implements View.OnClickListener, BluetoothFitnessClient.ChangeBleFlagListener{
    private static final String TAG = MainActivity.class.getSimpleName();
    //static  DemoActivityConnection Main;

    // tasto di collegamento
    private ImageButton connetti;
    private ImageView stato_connessione;
    private TextView batteria;

    //per la connessione alla tuta
    private boolean ble_flag=false;
    private BluetoothFitnessClient client;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.demo_connection);
        //Main=this;
        connetti= findViewById(R.id.connection_button);
        stato_connessione = findViewById(R.id.stato_connessione);
        batteria = findViewById(R.id.battery);





        //开启定位权限
        if(ContextCompat.checkSelfPermission(DemoActivityConnection.this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED){//未开启定位权限
            //开启定位权限,200是标识码
            ActivityCompat.requestPermissions(DemoActivityConnection.this,new String[]{Manifest.permission.ACCESS_FINE_LOCATION},200);
        }else{
            //startLocaion();//开始定位
            Toast.makeText(DemoActivityConnection.this,this.getString(R.string.openedlocation),Toast.LENGTH_LONG).show();
        }
        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.P) {
            Log.i("sdk_info", "sdk < 28 Q");
            if (ActivityCompat.checkSelfPermission(this,
                    Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                    || ActivityCompat.checkSelfPermission(this,
                    Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                String[] strings =
                        {Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION};
                ActivityCompat.requestPermissions(this, strings, 1);
            }
        } else {
            if (ActivityCompat.checkSelfPermission(this,
                    Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                    || ActivityCompat.checkSelfPermission(this,
                    Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED
                    || ActivityCompat.checkSelfPermission(this,
                    "android.permission.ACCESS_BACKGROUND_LOCATION") != PackageManager.PERMISSION_GRANTED) {
                String[] strings = {android.Manifest.permission.ACCESS_FINE_LOCATION,
                        android.Manifest.permission.ACCESS_COARSE_LOCATION,
                        "android.permission.ACCESS_BACKGROUND_LOCATION"};
                ActivityCompat.requestPermissions(this, strings, 2);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode){
            case 200://刚才的识别码
                if(grantResults[0] == PackageManager.PERMISSION_GRANTED){//用户同意权限,执行我们的操作
                    //startLocaion();//开始定位
                }else{//用户拒绝之后,当然我们也可以弹出一个窗口,直接跳转到系统设置页面
                    Toast.makeText(DemoActivityConnection.this,this.getString(R.string.remindopenlocation),Toast.LENGTH_SHORT).show();
                }
                break;
            default:break;
        }
    }


    public  void findDrive(View v){
        Toast.makeText(DemoActivityConnection.this,
                "Mi collego alla tuta", Toast.LENGTH_SHORT).show();
        if(ble_flag){
            startActivity(new Intent(this, DemoActivityController.class));
            return;
        }
        else if (client == null) {
            client = new BluetoothFitnessClient(this);
            client.setListener(this);
            client.addNames("mbody", "BALANX");
        }
        //todo: sistemare questa cosa
        client.startScan();
    }



    @Override
    public void onClick(View v) {


    }

    @Override
    public void bleFlagChanged(boolean b) {
        //se è true passo all'altra activity
        this.ble_flag = b;
        if(b){
            startActivity(new Intent(this, DemoActivityController.class));
            //todo: qui ci va il finish()?
        }
        else
        {
            Log.d(TAG,"VERIFICA TUTA");
            Toast.makeText(DemoActivityConnection.this,
                    "Verifica tuta", Toast.LENGTH_SHORT).show();
        }
    }


}
