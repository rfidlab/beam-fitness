package com.beamdigital.beamfitness;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.beamdigital.beamfitness.database.PlanManager;
import com.beamdigital.beamfitness.model.ExerciseEntity;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static android.view.View.GONE;
import static androidx.core.content.ContextCompat.startActivity;
import static com.beamdigital.beamfitness.database.PlanManager.TrainingPhase.PostTraining;
import static com.beamdigital.beamfitness.database.PlanManager.TrainingPhase.PreTraining;
import static com.beamdigital.beamfitness.database.PlanManager.TrainingPhase.Training;

public class RecyclerAdapterExercises extends RecyclerView.Adapter<RecyclerAdapterExercises.ExerciseHolder> {

    private final Context context;
    private List<ExerciseEntity> exercises;
    private final boolean showDuration;
    private final PlanManager planManager = App.getPlanManager();
    private PlanManager.TrainingPhase trainingPhase;
    private boolean highLighter=true;


    public interface ExerciseClickListener {
        void onclick();
    }

    private ExerciseClickListener listener;

    //questo è il costruttore, potrei inserire un booleano per capire se in questa recycler voglio o no la durata
    public RecyclerAdapterExercises(Context context, List<ExerciseEntity> exercise, boolean showDuration ) {
        this.exercises = exercise;
        this.context = context;
        this.showDuration = showDuration;

        if(context instanceof ExerciseClickListener)
            listener = (ExerciseClickListener) context;
    }
    public RecyclerAdapterExercises(Context context, List<ExerciseEntity> exercise, boolean showDuration, boolean highLighter) {
        this.exercises = exercise;
        this.context = context;
        this.showDuration = showDuration;
        this.highLighter = highLighter;

        if(context instanceof ExerciseClickListener)
            listener = (ExerciseClickListener) context;
    }

    @NonNull
    @Override
    public ExerciseHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater= LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.recycler_view_crea_esercizio,parent,false);
        ExerciseHolder exerciseHolder = new ExerciseHolder(view);
        return exerciseHolder;
    }

    //qui popolo i pezzi della recycler con la roba che ho dato al costruttore
    @Override
    public void onBindViewHolder(@NonNull ExerciseHolder holder, int position) {

        final ExerciseEntity e = exercises.get(position);
        //nome
        holder.name.setText(e.getName());
        //categoria
        holder.category.setText("Power");
        //tag
        holder.tag.setVisibility(GONE);
        //durata
        if(!showDuration) {
            holder.duration.setVisibility(GONE);
        } else {
            holder.duration.setVisibility(View.VISIBLE);
            holder.duration.setText(String.format(Locale.ITALIAN, "%dm", e.getDuration_min()));
        }

        //long press che porta alla modifica dell'esercizio



        if(showDuration && highLighter) {
            onBindSetBackground(holder.itemView, e);
            holder.itemView.setOnClickListener(v -> {
                if (planManager != null) {
                    Log.d("PlanManager", "onBindViewHolder: trainingPhase: " + trainingPhase);
                    switch (trainingPhase) {
                        case Training: {
                            if (planManager.hasExercise(Training, e)) {
                                setBackground(holder.itemView, false);
                                planManager.removeTrainingExercise(e);
                            } else {
                                setBackground(holder.itemView, true);
                                planManager.addTrainingExercise(e);
                            }
                            break;
                        }

                        case PostTraining: {
                            if (planManager.hasExercise(PostTraining, e)) {
                                setBackground(holder.itemView, false);
                                planManager.removePostTrainingExercise(e);
                            } else {
                                setBackground(holder.itemView, true);
                                planManager.addPostTrainingExercise(e);
                            }
                            break;
                        }

                        case PreTraining: {
                            if (planManager.hasExercise(PreTraining, e)) {
                                setBackground(holder.itemView, false);
                                planManager.removePreTrainingExercise(e);
                            } else {
                                setBackground(holder.itemView, true);
                                planManager.addPreTrainingExercise(e);
                            }
                            break;
                        }
                    }
                } else {
                    Toast.makeText(context, "ciao " + holder.name.getText(), Toast.LENGTH_SHORT).show();
                }

                if(listener != null)
                    listener.onclick();
            });


            holder.itemView.setOnLongClickListener(v -> {
                Intent intent= new Intent(context, ViewCreaEsercizio.class);
                intent.putExtra("exercise",exercises.get(holder.getBindingAdapterPosition()).toString());
                startActivity(context, intent , null);

                return false;
            });
        }
    }


    private void onBindSetBackground(View itemView, ExerciseEntity e) {
        if (trainingPhase == null)
            return;

        switch (trainingPhase) {
            case Training: {
                setBackground(itemView, planManager.hasExercise(Training, e));
                break;
            }

            case PostTraining: {
                setBackground(itemView, planManager.hasExercise(PostTraining, e));
                break;
            }

            case PreTraining: {
                setBackground(itemView, planManager.hasExercise(PreTraining, e));
                break;
            }
        }
    }

    private void setBackground(View itemView, boolean selected) {
        itemView.setBackgroundResource(selected ? R.drawable.ic_slot_riepilogo_esercizio_selected : R.drawable.ic_slot_riepilogo_eserciizo);
    }

    @Override
    public int getItemCount() {
        if (exercises ==null)
            return 0;
        return exercises.size();
    }

    public void setDataset(List<ExerciseEntity> exerciseList) {
        if(exerciseList != null) {
            Log.d("ViewCreaScheda", "setDataset: " + exerciseList.size());
            this.exercises = new ArrayList<>(exerciseList);
            notifyDataSetChanged();
        }
    }

    public void setType(PlanManager.TrainingPhase type) {
        this.trainingPhase = type;
    }

    //qui definisco i pezzi del recycler
    public class  ExerciseHolder extends RecyclerView.ViewHolder{
        TextView    name;
        TextView    category;
        ImageView   tag;
        TextView    duration;
        int         id;

        public ExerciseHolder(@NonNull View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.name_exercise);
            category = itemView.findViewById(R.id.category_exercise);
            tag = itemView.findViewById(R.id.tag);
            duration = itemView.findViewById(R.id.duration_exercise);
        }
    }
}
