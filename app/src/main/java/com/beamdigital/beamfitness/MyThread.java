package com.beamdigital.beamfitness;

import android.os.Message;

public class MyThread implements Runnable {
    //---------------------------------------------
    int start   = 0x01;
    int stop    = 0x02;
    //questi due sono troppo simili al momento, devo capire che differenze hanno
    int tre     = 0x03;
    int quattro = 0x04;
    //---------------------------------------------
    private static MyThread mythread=new MyThread();
    boolean flag=false;
    byte step=0; //sembra essere il passo successivo ma ancora lo devo inquadrare bene, viene modifica dentro MainActivity
    byte state=1;//1:start 2:work 3:over
    Message msg;
    int strength=0; //sembra essere l'intensità
    boolean[] arr=new boolean[12];
    public static MyThread getInstance(){
        return mythread;
    }
    @Override
    public void run() {
        try {
            while(flag){
                msg = MyHandler.getInstance().obtainMessage();
                if(state==1){
                    switch(step){
                        case 0:{//0
                            step=1;      // 发送停止信号 invia segnale di stop
                            msg.what=stop;
                        }break;
                        case 1:{        //发送波形、强度控制 invia forma d'onda , controllo intensità
                            step =2;
                            msg.what=0x03;
                            msg.arg1=strength;
                        }break;
                        case 2:{        //发送开始信号 invia segnale di avvio
                            step=0;
                            msg.what=start;
                            state=2;
                        }break;
                    }
                    Thread.sleep(100);
                }else if(state==2){
                    msg.what=0x04; //questo è il caso in cui faceva le verie forma d'onda
                    msg.arg1=strength;
                    try {
                        for(int i=0;i<500;i++){
                            Thread.sleep(10); // a cosa serve?
                            if(state!=2){
                                msg.what=0x00;

                                break;
                            }
                        }
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }else if(state==3){     //发送停止信号 invia segnale di stop
                    msg.what=stop;
                    mythread.flag =false;
                }
                msg.obj=arr;
                MyHandler.getInstance().sendMessage(msg);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
