package com.beamdigital.beamfitness;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.beamdigital.beamfitness.database.AppDatabase;
import com.beamdigital.beamfitness.database.PlanManager;
import com.beamdigital.beamfitness.model.ExerciseEntity;
import com.beamdigital.beamfitness.model.Plan;
import com.beamdigital.beamfitness.realtime.RTDBManager;
import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.google.gson.Gson;
import com.theartofdev.edmodo.cropper.CropImage;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

public class ViewCreaScheda extends AppCompatActivity {

    private static final String TAG = ViewCreaScheda.class.getSimpleName();

    private RecyclerAdapterExercises preTrainingAdapter;
    private RecyclerAdapterExercises trainingAdapter;
    private RecyclerAdapterExercises postTrainingAdapter;
    private final PlanManager planManager = App.getPlanManager();
    private TextView workoutName;
    private TextView riepilogoDurata;
    private TextView deletePlan;
    private final int schedaDuration = 0;
    Uri copertina;
    Uri copertinaCrop;
    long timestamp;
    Plan modifica;


    ImageView imgCopertina;
    boolean update =false;



    //per caricare le immagini
    ActivityResultLauncher<String> mGetContentCopertina = registerForActivityResult(new ActivityResultContracts.GetContent(),
            new ActivityResultCallback<Uri>() {
                @Override
                public void onActivityResult(Uri uri) {
                    copertina=uri;
                    startCrop(uri);
                    timestamp= Calendar.getInstance().getTime().getTime();
                    planManager.setURLCover(timestamp+"_"+FileService.queryName(getApplicationContext(),copertina));
                    //lancia lo start crop e quando finisce il crop parte onActivityResult che piazza l'immagine
                    //imgCopertina.setImageURI(uri);
                    //Glide.with(getApplicationContext()).asGif().load(uri).into(video);

                }
            });

    private void startCrop(Uri imageuri) {
        CropImage.activity(imageuri)
                //.setGuidelines(CropImageView.Guidelines.OFF)
                //.setMultiTouchEnabled(true)
                .setAspectRatio(16,9)
                //.setMinCropResultSize(2000,400)
                //.setMaxCropResultSize(2000,400)
                .setAutoZoomEnabled(false)
                .start(this);

    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE){
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK){
                imgCopertina.setImageURI(result.getUri());
                imgCopertina.setVisibility(View.VISIBLE);
                copertinaCrop=result.getUri();
            }
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(planManager != null)
            planManager.createNewPlan();
        setContentView(R.layout.view_crea_scheda);
        workoutName = findViewById(R.id.workoutName);
        riepilogoDurata = findViewById(R.id.riepilogoDurata);
        imgCopertina = findViewById(R.id.imgCopertina);
        deletePlan = findViewById(R.id.deletePlan);


                RecyclerView recViewRiscaldamento = findViewById(R.id.listaRiscaldamento);
        preTrainingAdapter = new RecyclerAdapterExercises(
            this,
                planManager != null ? planManager.getPreTraining() : new ArrayList<>(0),
                true, false
        );
        recViewRiscaldamento.setLayoutManager((new LinearLayoutManager(this, RecyclerView.VERTICAL, false)));
        recViewRiscaldamento.setAdapter(preTrainingAdapter);

        ///////////////////////////////////////////////////////////////////////////
        RecyclerView recViewAllenamentoR = findViewById(R.id.listaAllenamento);
        trainingAdapter = new RecyclerAdapterExercises(
            this,
                planManager != null ? planManager.getTraining() : new ArrayList<>(0),
                true ,false
        );
        recViewAllenamentoR.setLayoutManager((new LinearLayoutManager(this, RecyclerView.VERTICAL, false)));
        recViewAllenamentoR.setAdapter(trainingAdapter);

        ///////////////////////////////////////////////////////////////////////////
        RecyclerView recViewDefaticamento = findViewById(R.id.listaDefaticamento);
        postTrainingAdapter = new RecyclerAdapterExercises(
            this,
                planManager != null ? planManager.getPostTraining() : new ArrayList<>(0),
                true , false
        );
        recViewDefaticamento.setLayoutManager((new LinearLayoutManager(this, RecyclerView.VERTICAL, false)));
        recViewDefaticamento.setAdapter(postTrainingAdapter);


        //parte dove carico le info per fare il modifica scheda
        try {
            Bundle bundle = getIntent().getExtras();
            modifica= new Gson().fromJson(   bundle.getString("plan")  , Plan.class);
            //con set plan risolvo il problema di plan e planManager planManager.setPLan(Plan)
            //setto il nome
            workoutName.setText(modifica.getName());

            //INIZIO PEZZO CHE CARICA GIF
            StorageReference storageReference = FirebaseStorage.getInstance().getReference();
            StorageReference ref = storageReference.child("copertina/"+modifica.getCover());

            ref.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                @Override
                public void onSuccess(Uri uri) {
                    Glide.with(getApplicationContext()).load(uri).into(imgCopertina);
                    imgCopertina.setVisibility(View.VISIBLE);
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    //Toast.makeText(ViewCreaEsercizio.this, "FORSE MANCA IL VIDEO DELL'ESERCIZIO", Toast.LENGTH_SHORT).show();
                    imgCopertina.setVisibility(View.VISIBLE);
                    Glide.with(getApplicationContext()).load("https://media.giphy.com/media/nqCH5dsIn99cmSPVrd/giphy.gif").dontAnimate().into(imgCopertina);
                }
            });

            planManager.setPlan(modifica);
            update=true;
            deletePlan.setVisibility(View.VISIBLE);
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    @Override
    protected void onResume() {
        super.onResume();
        if(planManager != null) {
            riepilogoDurata.setText(String.format(Locale.ITALIAN, "%dm", planManager.duration()));
            ArrayList<ExerciseEntity> preTraining = planManager.getPreTraining();
            ArrayList<ExerciseEntity> training = planManager.getTraining();
            ArrayList<ExerciseEntity> posttraining = planManager.getPostTraining();
            Log.d(TAG, "onResume: preTraining: " + preTraining.size());
            Log.d(TAG, "onResume: training: " + training.size());
            Log.d(TAG, "onResume: posttraining: " + posttraining.size());
            preTrainingAdapter.setDataset(preTraining);
            trainingAdapter.setDataset(training);
            postTrainingAdapter.setDataset(posttraining);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
//        if(planManager != null)
//            planManager.destroyPlan();
    }

/*    public void click_nome(View view) {
        //TODO: sfruttare quello già esistente in BeamActivity
        final Dialog d = new Dialog(ViewCreaScheda.this);
        d.setContentView(R.layout.name_workout);
        Button b1 = d.findViewById(R.id.si);
        Button b2 = d.findViewById(R.id.no);

        final EditText nw = d.findViewById(R.id.allenamento);

        b1.setOnClickListener(v -> {
            workoutName.setTextColor(getResources().getColor(R.color.black, null));
            workoutName.setText(nw.getText().toString());
            d.dismiss();
        });

        b2.setOnClickListener(v -> d.dismiss());

        d.show();
    }*/


    //todo: i prossimi add# servono per passare all'activity di scelta dell'esercizio
    public void addRiscaldamento(View view) {
        Intent intent = new Intent(this, ViewCercaEsercizio.class);
        intent.putExtra("ex", "Aggiungi Riscaldamento");
        intent.putExtra("time", schedaDuration);

        startActivity(intent);
        return;
    }

    public void addAllenamento(View view) {
        Intent intent = new Intent(this, ViewCercaEsercizio.class);
        intent.putExtra("ex", "Aggiungi Allenamento");
        intent.putExtra("time", schedaDuration);

        startActivity(intent);
        return;
    }

    public void addDefaticamento(View view) {
        Intent intent = new Intent(this, ViewCercaEsercizio.class);
        intent.putExtra("ex", "Aggiungi Defaticamento");
        intent.putExtra("time", schedaDuration);

        startActivity(intent);
        return;
    }

    public void savePlan(View view) {

        RTDBManager manager = RTDBManager.getInstance();
        if(planManager != null) {

            //Insert or update in case we also have the cover image
            if(copertinaCrop != null)
            {

                FirebaseStorage storage = FirebaseStorage.getInstance();
                StorageReference storageRef = storage.getReference();
                StorageReference imageRef = storageRef.child("copertina/"+timestamp+"_"+FileService.queryName(getApplicationContext(),copertina));
                UploadTask uploadTask = imageRef.putFile(copertinaCrop);

                uploadTask.addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> task) {

                        try {
                            if(update)
                            {
                                planManager.setURLCover(timestamp+"_"+FileService.queryName(getApplicationContext(),copertina));
                                planManager.setName(workoutName.getText().toString());
                                planManager.validatePlan(getApplicationContext());
                                planManager.savePlan(getApplicationContext());
                                manager.updataPlansToRTDG(planManager.getPlan());


                            }
                            else{
                                planManager.setURLCover(timestamp+"_"+FileService.queryName(getApplicationContext(),copertina));
                                planManager.setName(workoutName.getText().toString());
                                planManager.validatePlan(getApplicationContext());
                                planManager.savePlan(getApplicationContext());
                                manager.addAllPlansToRTDB(planManager.getPlan());

                            }
                            finish();

                        } catch (Exception e) {
                            Toast.makeText(getApplicationContext(), "save"+e.getMessage(), Toast.LENGTH_SHORT).show();
                            e.printStackTrace();
                        }

                    }
                });
            }
            //If we do not have to add the cover image, we do the next section
            else
            {

                if(update)
                {
                    planManager.setName(workoutName.getText().toString());
                    planManager.validatePlan(getApplicationContext());
                    planManager.savePlan(getApplicationContext());
                    manager.updataPlansToRTDG(planManager.getPlan());
                }
                else{
                    planManager.setName(workoutName.getText().toString());
                    planManager.validatePlan(getApplicationContext());
                    planManager.savePlan(getApplicationContext());
                    manager.addAllPlansToRTDB(planManager.getPlan());

                }
                finish();
            }


            /*uploadTask.addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception exception) {
                    Toast.makeText(getApplicationContext(), "Errore: "+exception.toString(), Toast.LENGTH_SHORT).show();

                }
            }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    // taskSnapshot.getMetadata() contains file metadata such as size, content-type, etc.
                    // ...
                    Toast.makeText(getApplicationContext(), "Salvato su storage", Toast.LENGTH_SHORT).show();

                }
            });*/

        } else {
            Toast.makeText(this, R.string.db_error, Toast.LENGTH_SHORT).show();
        }
    }

    public void click_add_copertina(View view) {
        mGetContentCopertina.launch("image/*");


    }

    public void deletePlan(View view) {
        RTDBManager manager = RTDBManager.getInstance();
        //cancello il plan da firebase
        manager.removePlanToRTDB(modifica);
        //oltre a cancellare il plan dal db va cancellato anche dentro il telefono
        AppDatabase.getDatabase(this).getPlanDao().delete(modifica);
        finish();
        Toast.makeText(this, "Scheda eliminata", Toast.LENGTH_SHORT).show();

    }
}
