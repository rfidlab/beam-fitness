package com.beamdigital.beamfitness;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


public class RecyclerAdapterCarousel extends RecyclerView.Adapter<RecyclerAdapterCarousel.CarouselViewHolder> {

    String TAG = "RecyclerAdapterCarousel";
    Context context;

    public RecyclerAdapterCarousel(Context context) {
        this.context = context;
    }

    @NonNull
    @Override
    public RecyclerAdapterCarousel.CarouselViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater= LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.carousel,parent,false);
        RecyclerAdapterCarousel.CarouselViewHolder viewHolder = new RecyclerAdapterCarousel.CarouselViewHolder(view);

        switch (viewType){
            case 0: {
                view = layoutInflater.inflate(R.layout.carousel,parent,false);
                viewHolder = new RecyclerAdapterCarousel.CarouselViewHolder(view);
                break;
            }
            case 1:{
                view = layoutInflater.inflate(R.layout.carousel_one,parent,false);
                viewHolder = new RecyclerAdapterCarousel.CarouselViewHolder(view);

                break;
            }
            case 2:{
                view = layoutInflater.inflate(R.layout.carousel_two,parent,false);
                viewHolder = new RecyclerAdapterCarousel.CarouselViewHolder(view);
                BFCManager bfcManager = ViewCentrale.get().getBfcManager();
                viewHolder.connect.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        bfcManager.connect(context);

                    }
                });

                //applico l'idea di Simonetta
                viewHolder.itemView.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        bfcManager.connect(context);
                    }
                });

                break;
            }

        }

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerAdapterCarousel.CarouselViewHolder holder, int position) {

        ViewGroup.LayoutParams layoutParams = holder.itemView.getLayoutParams();

        //gestione di cosa sparisce quando il carosello si aziona

/*        if (imageShown) {
            //zero
            holder.network_zero.setVisibility(View.VISIBLE);
            holder.countDisponibili.setVisibility(View.VISIBLE);
            holder.textDisponibiliNetwork.setVisibility(View.VISIBLE);
            holder.ricarica.setVisibility(View.VISIBLE);
            //one
            holder.network_one.setVisibility(View.VISIBLE);
            holder.riscatta.setVisibility(View.VISIBLE);
            //two
        } else {
            //zero
            holder.network_zero.setVisibility(View.GONE);
            holder.countDisponibili.setVisibility(View.GONE);
            holder.textDisponibiliNetwork.setVisibility(View.GONE);
            holder.ricarica.setVisibility(View.GONE);
            //one
            holder.network_one.setVisibility(View.VISIBLE);
            holder.riscatta.setVisibility(View.VISIBLE);
            //two
        }*/



        BFCManager bfcManager = ViewCentrale.get().getBfcManager();
        //imposto lo stato della connessione nel carosello
        if (bfcManager.isFlag_connessione())
        {
            Log.d("Luca","notify" + holder.status);
            //connesso
            if (holder.status != null) {
                holder.status.setText("Connesso");
                holder.status.setTextColor(Color.GREEN);

                holder.connect.setText("Disconnetti");
            }
        }
        else
        {
            if (holder.status != null) {
                holder.status.setText("Disconnesso");
                holder.status.setTextColor(Color.RED);

                holder.connect.setText("Connetti");


            }

        }



    }

    @Override
    public int getItemCount() {
        return 3;
    }

    boolean imageShown = true;
    public void showImage(int height) {
        imageShown = true;
        notifyDataSetChanged();
    }

    public void hideImage(int height) {
        imageShown = false;
        notifyDataSetChanged();
    }



    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public class CarouselViewHolder extends RecyclerView.ViewHolder{

        //carousel_zero
        ImageView network_zero;
        TextView countDisponibili;
        TextView textDisponibiliNetwork;
        Button ricarica;

        //carousel_one
        ImageView network_one;
        TextView countNetwork;
        TextView textNetwork;
        Button riscatta;

        //carousel_two
        TextView status;
        Button connect;

        public CarouselViewHolder(@NonNull View itemView) {

            super(itemView);
            //carousel_zero
            network_zero = itemView.findViewById(R.id.imageView2);
            countDisponibili = itemView.findViewById(R.id.numberNetwork);
            textDisponibiliNetwork = itemView.findViewById(R.id.textDisponibiliNetwork);
            ricarica = itemView.findViewById(R.id.ricarica);

            //carousel_one
            network_one = itemView.findViewById(R.id.imageView2);
            countNetwork = itemView.findViewById(R.id.invitationProgress);
            textNetwork = itemView.findViewById(R.id.invitationText);
            riscatta = itemView.findViewById(R.id.riscattaPremio);


            //carousel_two
            status = itemView.findViewById(R.id.connection_status);
            connect = itemView.findViewById(R.id.tastConnetti);


        }
    }
}
