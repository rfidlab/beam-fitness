package com.beamdigital.beamfitness;

import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.beamdigital.beamfitness.model.Muscolo;
import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.gson.Gson;
import com.beamdigital.beamfitness.database.AppDatabase;
import com.beamdigital.beamfitness.model.ExerciseEntity;
import com.beamdigital.beamfitness.model.Plan;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import eu.beamdigital.beamfitnesslibrary.BluetoothFitnessClient;
import eu.beamdigital.beamfitnesslibrary.Exercise;

public class ViewAllenamento extends AppCompatActivity {
    private static final String TAG = ViewAllenamento.class.getSimpleName();


    //storage firebase
    StorageReference storageReference = FirebaseStorage.getInstance().getReference();

    //REGISTRO DEI MUSCOLI CON RELATIVA POTENZA DA UTILIZZARE PER TUTTO L'ALLENAMENTO
    //todo: mettere una variabile
    Map<Byte,Byte> muscleLedger = new LinkedHashMap<>();

    //animazione del tasto play
    ObjectAnimator scaleDown;

    //flag per i suoni
    boolean soundsEnabled = true;
    boolean nextSoundRest=false;
    //media player
    private MediaPlayer player;

    Context context = this;
    //flag per sapere se stiamo in modalità demo
    boolean demostrationMode =false;

    // gestione pausa riprendi in fase di allenamento per la parte dei muscoli
    int time_scheda = 0;
    int progress_scheda = 0;
    long backup_durata_scheda=0;
    long durata_scheda=0;
    private float onePercent_durata_scheda =-1;

    //gestione progress bar esercizio
    int progress_esercizio=0;
    int onePercent_durata_esercizio=0;



    //parte grafica
    ImageButton play;
    TextView workoutStatus;
    TextView progressBarText;
    ProgressBar progressBarExercise;
    ImageView workoutVideo;
    ImageButton play_button;
    ImageButton sound_button;

    //recycler view
    RecyclerView workoutList;
    RecyclerAdapterWorkoutList recyclerAdapterWorkoutList;

    Plan plan;

    //questo lo usiamo solamente per dare ordini alla tuta
    Exercise exercise= new Exercise();

    //Orologio per il countDown
    private android.os.CountDownTimer CountDownTimer;

    //al momento li divido, ma probabilmente la cosa migliore è tenerli tutti insieme
    private List<ExerciseEntity> preTraining;
    private List<ExerciseEntity> training;
    private List<ExerciseEntity> postTraining;
    //infatti qui sono tutti insieme
    private List<ExerciseEntity> fullList= new ArrayList<>();


    //GESTIONE CONNESSIONE
    //connessione alla libreria
    private BluetoothFitnessClient client;


    //indice per gli esercizi
    private int indexExercise=0;

    //Utile a gestire tempi allenamento in ms
    private int durata_esercizio = -1;
    private int stimolazione_esercizio = -1;
    private int riposo_esercizio = -1;

    //questi di backup li uso per rimettere "a posto" quelli di sopra dopo delle interazioni particolari come la fine dell'allenamento o la messa in pausa
    private int backup_durata_esercizio = -1;
    private int backup_stimolazione_esercizio = -1;
    private int backup_riposo_esercizio = -1;



    //USO QUESTO BOOLEANO PER FARE PLAY/PAUSA
    private Boolean onTrack=false;
    private Boolean onSetup = true;

    //gestione dell'allenamento
    ///fine è orario di fine scheda
    long fine=0;
    boolean stimolare=false;
    boolean riposare = false;
    long prossima_stimolazione = 0;
    long prossimo_riposo=0;

    //gestione del tempo espressa in timestamp
    final int secondo = 1000;
    final int minuto = 60*secondo;
    final int ora = 60*minuto;

    //per la gestione della potenza dei muscoli
    public static ArrayList<Integer>  indexMuscle = new ArrayList<>();



    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.view_allenamento);
        getWindow(). addFlags (WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        //popolo il muscleLedger
        fillLedger(muscleLedger);

        play =  findViewById(R.id.play_button);
        workoutStatus = findViewById(R.id.workoutStatus);
        workoutVideo = findViewById(R.id.workoutVideo);
        progressBarText = findViewById(R.id.progressBarText);
        progressBarExercise = findViewById(R.id.progressBarExercise);
        play_button = findViewById(R.id.play_button);
        sound_button = findViewById(R.id.sound_button);




        //uso il bundle per prendere il nome del plan da avviare
        Bundle bundle = getIntent().getExtras();
        int idPlan =Integer.parseInt(bundle.get("plan").toString());
        //vediamo se siamo in modalità demo
        demostrationMode = bundle.getBoolean("demo");

        //prendo il plan
        plan = AppDatabase.getDatabase(this).getPlanDao().getID(idPlan);
        //questi 3 sono provvisori
        preTraining = plan.getPretraining();
        training = plan.getTraining();
        postTraining= plan.getPosttraining();

        fullList.addAll(plan.getPretraining());
        fullList.addAll(plan.getTraining());
        fullList.addAll(plan.getPosttraining());

        //mi prendo la durata complessiva dell'allenamento
        for(ExerciseEntity esercizio : fullList)
        {
            durata_scheda+=(esercizio.getDuration_min()*minuto);
        }
        backup_durata_scheda=durata_scheda;
        onePercent_durata_scheda =durata_scheda/100;


        workoutList = findViewById(R.id.workoutList);
        workoutList.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
        recyclerAdapterWorkoutList = new RecyclerAdapterWorkoutList(this, fullList);
        workoutList.setAdapter(recyclerAdapterWorkoutList);

        client = ViewCentrale.get().getBfcManager().client;


        //carico frequenza e onda
        new Thread(()-> {
            try {
                client.setWaveForm(fullList.get(0).getWave_form());

                Thread.sleep(60);
                client.setFrequency(fullList.get(0).getFrequency());

            } catch (Exception e) {
                Log.d(TAG, "errore in sendStartExercise ");
            }
        }).start();

        workoutVideo.post(new Runnable() {
            @Override
            public void run() {

                //INIZIO PEZZO CHE CARICA GIF
                StorageReference ref = storageReference.child("gif/"+fullList.get(0).getVideo());

                ref.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                    @Override
                    public void onSuccess(Uri uri) {
                        Glide.with(getApplicationContext()).asGif().load(uri).into(workoutVideo);
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Glide.with(context).asGif().load("https://media.giphy.com/media/nqCH5dsIn99cmSPVrd/giphy.gif").into(workoutVideo);

                    }
                });

            }
        });



        //animazione tasto play
        play.setImageDrawable(getDrawable(R.drawable.ic_play_plan_pulsing));

        scaleDown = ObjectAnimator.ofPropertyValuesHolder(
                play,
                PropertyValuesHolder.ofFloat("scaleX", 1.1f),
                PropertyValuesHolder.ofFloat("scaleY", 1f));
        scaleDown.setDuration(310);

        scaleDown.setRepeatCount(ObjectAnimator.INFINITE);
        scaleDown.setRepeatMode(ObjectAnimator.REVERSE);
        scaleDown.start();



    }



    public void setupMuscles(View view) {
        //todo: forse basta un controllo su onTrack tipo if (onTrack) fai questa cosa. Ma va spostato in cima
        if(onTrack)
        {
            play.setImageDrawable(getDrawable(R.drawable.ic_play_plan));
        }

        if(client != null && demostrationMode ==false)
        {
            client.sendStopCommand();

        }

        if (onSetup== false)
        {
            onSetup=true;
            if(onTrack) {
                startPause();
            }
        }


        Intent intent = new Intent(this, ViewSetupMuscles.class);
        intent.putExtra("muscleList", fullList.get(indexExercise).toString());
        intent.putExtra("gif", fullList.get(indexExercise).getVideo());


        Log.d(TAG,"remaining time"+durata_scheda);
        intent.putExtra("time", durata_scheda);
        if(fine == 0)
        {
            intent.putExtra("progress", 0);

        }
        else
            intent.putExtra("progress", progress_scheda);



        startActivityForResult(intent, 0);
        return;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == 0) {
            if(resultCode == RESULT_OK) {
                fullList.set(indexExercise       ,        new Gson().fromJson(   data.getStringExtra("result")  , ExerciseEntity.class) );
                //todo: cosa fare in caso di stop? gli esercizi ripartono tutti con i muscoli a 20?

            }
        }
    }

    public void play(View view)
    {
        onSetup=false;
        if(onTrack==false)
        {
            scaleDown.end();
            resetAnimation(scaleDown);

            play.setImageDrawable(getDrawable(R.drawable.ic_pause));
            startExercise();
        }
        else
        {
            play.setImageDrawable(getDrawable(R.drawable.ic_play_plan));
            if(client != null && demostrationMode ==false)
            {
                client.sendStopCommand();

            }
            startPause();

        }

    }

    private void startPause() {
        onTrack =false;
        pauseTimer();
        workoutStatus.post(new Runnable() {
            @Override
            public void run() {
                workoutStatus.setText("Pausa");
            }
        });
        durata_esercizio = (int) (fine - Calendar.getInstance().getTime().getTime());
        //calcolo il tempo restante dell'esercizio che stiamo svolgendo

        //ripristino la durata della scheda al valore originale
        durata_scheda=backup_durata_scheda;
        //sottraggo tutti gli esercizio già svolti
        Log.d(TAG,"Exercise number: "+indexExercise);

        for(int i=0;i<indexExercise;i++)
        {
            durata_scheda-=fullList.get(i).getDuration_min()*minuto;
        }
        //scarto il tempo dell'esercizio già svolto
        durata_scheda=durata_scheda-(backup_durata_esercizio-durata_esercizio);

        Log.d(TAG,"remaining time: "+durata_scheda);
        //CALCOLO TEMPO PER LA PROGRESS BAR DI SETUP MUSCLES
        time_scheda = (int)durata_scheda;
        progress_scheda = (int)(((backup_durata_scheda-durata_scheda)/ onePercent_durata_scheda)+1);




        //SE STIAMO STIMOLANDO
        if(stimolare==true && riposare == false){
            stimolazione_esercizio = (int)((prossima_stimolazione+(stimolazione_esercizio)) - Calendar.getInstance().getTime().getTime());

            Log.d(TAG,"Stimulation to complete: " + stimolazione_esercizio);
        }
        //SE STIAMO RIPOSANDO
        else if(riposare==true && stimolare==false){
            riposo_esercizio = (int)((prossimo_riposo+(riposo_esercizio)) - Calendar.getInstance().getTime().getTime());

            Log.d(TAG,"Rest to complete: " + riposo_esercizio);
        }

    }

    private void startExercise() {
        //THIS FLAG DENOTE THAT THE SUIT IS IN WORKING PHASE
        onTrack = true;

        //LOADING OF SOUNDS
        if(soundsEnabled)
        {
            try {
                AssetFileDescriptor afd = getAssets().openFd("stimola.wav");
                player = new MediaPlayer();
                player.setDataSource(afd.getFileDescriptor(), afd.getStartOffset(), afd.getLength());
                afd.close();
                player.prepare();
                player.setVolume(1.f, 1.f);
                player.start();
                Thread.sleep(20);
                nextSoundRest=true;
            } catch (IOException | InterruptedException e) {
                e.printStackTrace();
            }

        }

        new Thread(()-> {
            try
            {
                /*FIRST WHILE, IS THERE AN EXERCISE TO DO? ARE WE ONTRACK? IF YES WE ARE GOING TO LOAD A SERIES OF USEFULL INFO:
                - MUSCLESLIST
                - STARTING TIME

                 */

                while (indexExercise < fullList.size() && onTrack) {


                    Log.d(TAG, "we are going to do the exercise called: " + fullList.get(indexExercise).getName());


                    //DATI SUI MUSCOLI DA USARE
                    exercise.muscle_list = new ArrayList<>();
                    exercise.muscle_list.addAll(fullList.get(indexExercise).getMuscle_list());
                    Date inizio = Calendar.getInstance().getTime();


                    //IF THEY ARE != WE ARE RESUMING FROM A PAUSE
                    if(backup_riposo_esercizio == riposo_esercizio && backup_stimolazione_esercizio == stimolazione_esercizio)
                    {
                        //INFO ABOUT THE EXERCISE, DURATION, STIMULATION, BACKUP
                        durata_esercizio = fullList.get(indexExercise).duration_min * minuto;
                        stimolazione_esercizio = fullList.get(indexExercise).stimulation_sec * secondo;
                        riposo_esercizio = fullList.get(indexExercise).rest_sec * secondo;

                        //A BACKUP COPY TO READ WHEN WE GO BACK FROM PAUSE
                        backup_durata_esercizio = durata_esercizio;
                        backup_stimolazione_esercizio = stimolazione_esercizio;
                        backup_riposo_esercizio = riposo_esercizio;

                        //PROGRESS BAR PERCENTAGE
                        onePercent_durata_esercizio= durata_esercizio/100;


                        //TIMESTAMP OF NEXT STIMULATION AND NEXT REST
                        prossima_stimolazione = inizio.getTime();
                        prossimo_riposo = prossima_stimolazione + (stimolazione_esercizio) + 1;
                        //fine = (inizio.getTime() + (durata_scheda));

                    }
                    //WE CHECK ONLY THE REST BECAUSE A REST GOES ALWAYS AFTER A STIMULATION
                    else if(backup_riposo_esercizio == riposo_esercizio) {
                        //IF THE 2 REST ARE == IT MEANS WE HAVE TO STIMULATE
                        prossima_stimolazione = inizio.getTime();
                        prossimo_riposo = prossima_stimolazione + (stimolazione_esercizio) + 1;
                    }
                    else
                    {
                        //IF THE 2 REST ARE DIFFERENT IT MEANS WE HAVE TO REST
                        prossimo_riposo = inizio.getTime();
                    }
                    //HERE WE SET THE TIMESTAMP OF THE END OF THE EXERCISE
                    fine = (inizio.getTime() + (durata_esercizio));


                    progressBarText.post(new Runnable() {
                        @Override
                        public void run() {
                            startTimer();
                        }
                    });


                    //USEFUL LOG TO CHECK IF ALL THE INFORMATION ARE AS YOU EXPECT
/*                    Log.d(TAG, "plan duration                     " + durata_scheda);
                    Log.d(TAG, "exercise duration                     " + durata_esercizio);
                    Log.d(TAG, "backup exercise duration              " + backup_durata_esercizio);
                    Log.d(TAG, "stimulation                " + stimolazione_esercizio);
                    Log.d(TAG, "backup stimulation         " + backup_stimolazione_esercizio);
                    Log.d(TAG, "rest                      " + riposo_esercizio);
                    Log.d(TAG, "backup rest               " + backup_riposo_esercizio);
                    Log.d(TAG, "next stimulation (timestamp)                 " + prossima_stimolazione);
                    Log.d(TAG, "next rest   (timestamp)                    " + prossimo_riposo);
                    Log.d(TAG, "exrcise starting time                     " + inizio.getTime());
                    Log.d(TAG, "exercise end time                       " + fine);*/

                    //FLAGS TO SEE IF WE ARE IN REST OR STIMULATION PHASE
                    stimolare = false;
                    riposare = false;

                    //HERE WE REALLY ACTIVATE THE SUIT
                    /*SECOND WHILE, ONCE WE HAVE RECOGNIZED THE EXERCISE AND SET INFORMATION LIKE
                    - START TIME
                    - END TIME
                    - MUSCLE LIST
                    - STIMULATION AND REST DURATION (SPECIALLY IF WE RESUME FROM A PAUSE)

                    WE CHECK IF THIS EXERCISE HAS TO BE COMPLETED
                    */
                    while (inizio.getTime() < fine && onTrack) {

                        //MAYBE USELESS BUT COMFORTABLE VALUE
                        Date now = Calendar.getInstance().getTime();

                        // IF NOW IS IN THE STIMULATION INTERVAL WE STIMULATE
                        // BUT IF BACKUP != RIPOSO IT IS TIME TO REST
                        while ((prossima_stimolazione <= now.getTime() && now.getTime() <= prossima_stimolazione + (stimolazione_esercizio) && onTrack) && backup_riposo_esercizio == riposo_esercizio) {

                            if (stimolare == false) {
                                //SETUP THE FLAG
                                stimolare = true;
                                riposare = false;

                                //UPDATE THE TEXVIEW
                                workoutStatus.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        workoutStatus.setText("Stimolazione");
                                    }
                                });
                                Log.d(TAG, "working " + (fine - now.getTime()) + " (sec) " + stimolazione_esercizio);

                                //IT IS TIME TO COMMUNICATE WITH THE SUIT
                                if(client != null && demostrationMode ==false)
                                {
                                    client.sendStartExercise(exercise);

                                }
                                //SETUP THE NEXT TIMESTAMP REST (MAYBE REPETED)
                                prossimo_riposo = prossima_stimolazione + (stimolazione_esercizio) + 1;
                            }

                            //CHECK IF THE EXERCISE IS OVER OR NOT
                            now = Calendar.getInstance().getTime();
                            if (now.getTime() >= fine) {
                                Log.d(TAG, "exercise ended");
                                pauseTimer();
                                break;

                            }
                            //IF THE STIMULATION IS GOING TO FINISH I REPRODUCE THE REST SOUND
                            if(onTrack&&soundsEnabled&&nextSoundRest&&(1100>((stimolazione_esercizio-(now.getTime()-(prossima_stimolazione))))))
                            {
                                try {
                                    AssetFileDescriptor afd = getAssets().openFd("riposo.wav");
                                    player = new MediaPlayer();
                                    player.setDataSource(afd.getFileDescriptor(), afd.getStartOffset(), afd.getLength());
                                    afd.close();
                                    player.prepare();
                                    player.setVolume(1.f, 1.f);
                                    player.start();
                                    //Thread.sleep(20);
                                    nextSoundRest=false;
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            }
                        }


                        // IF NOW IS IN THE REST INTERVAL WE REST
                        while (prossimo_riposo <= now.getTime() && now.getTime() <= prossimo_riposo + (riposo_esercizio) && onTrack) {
                            if (riposare == false) {
                                //TEXVIEW UPDATE
                                workoutStatus.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        workoutStatus.setText("Riposo");
                                    }
                                });
                                Log.d(TAG, "rest " + (fine - now.getTime()) + " (sec) " + riposo_esercizio);

                                //UPDATE THE FLAGS
                                riposare = true;
                                stimolare = false;

                                //NOTICE THE SUIT THAT HAS TO STOP THE STIMULATION
                                if(client != null && demostrationMode ==false)
                                {
                                    client.sendStopCommand();

                                }

                                //CALCULATE NEXT STIMULATION TIMESTAMP
                                prossima_stimolazione = prossimo_riposo + (riposo_esercizio) + 1;
                            }

                            //CHECK IF THE PLAN/EXERCISE IS OVER OR NOT
                            now = Calendar.getInstance().getTime();
                            if (now.getTime() >= fine) {
                                pauseTimer();
                                break;
                            }

                            //IF THE REST IS GOING TO FINISH I REPRODUCE THE STIMULATION SOUND
                            if(onTrack&&soundsEnabled&&!nextSoundRest&&(1100>((riposo_esercizio-(now.getTime()-(prossimo_riposo))))))
                            {
                                try {
                                    AssetFileDescriptor afd = getAssets().openFd("stimola.wav");
                                    player = new MediaPlayer();
                                    player.setDataSource(afd.getFileDescriptor(), afd.getStartOffset(), afd.getLength());
                                    afd.close();
                                    player.prepare();
                                    player.setVolume(1.f, 1.f);
                                    player.start();
                                    //Thread.sleep(20);
                                    nextSoundRest=true;
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            }
                        }

                        //finito il giro riaggiorniamo i valori prendendoli dal backup
                        // AT THIS POINT WE HAVE DONE A CYCLE OF STIMULATION + REST (OR JUST A REST)
                        // IT IS TIME TO RELOAD ORIGINAL STIMALATION/REST INFO (THIS SHOULD BE NECESSARY ONLY AFTER A PAUSE, BUT WE DO IIN BOTH CASE)
                        if(onTrack==true){
                            stimolazione_esercizio = backup_stimolazione_esercizio;
                            riposo_esercizio = backup_riposo_esercizio;}
                        //aggiorniamo inizio
                        inizio = Calendar.getInstance().getTime();
                    }
                    //HERE FINISHES THE SECOND WHILE
                    Log.d(TAG, "exercise ended on interrupted");

                    //IF WE ARE STILL ON TRACK IT IS TIME TO UPDATE SOME VARIABLES
                    if(onTrack)
                    {
                        //aggiorno il muscleLedger prima di passare al prossimo sercizio
                        //UPDATE muscleLedger BECAUSE IF I INCRESED/DECREASED A MUSCLE IN THE NEXT EXERCISE I WANT THE SAME VALUE AND NOT THE DEFAULT VALUE
                        updateLedger(fullList.get(indexExercise).muscle_list);
                        //INCRESE INDEX EXERCISE, USEFUL IN FIRST WHILE
                        indexExercise++;
                        //UPDATE DURATION OF THE PLA
                        durata_scheda=durata_scheda-durata_esercizio;

                        //UPDATE OF REC VIEW IN ORDER TO REMOVE THE EXERCISE ALREADY DONE
                        ((Activity) this).runOnUiThread(()-> {

                            recyclerAdapterWorkoutList.setDataset(fullList.subList(indexExercise,(fullList.size())));
                        });

                        //recyclerAdapterWorkoutList.setDataset(fullList.subList(indexExercise,fullList.size()-1));
                        //preparo onda e frequnza per il prossimo esercizio
                        //carico frequenza e onda
                        //IF WE ARE NOT IN DEMOSTRATION MODE (RED ENGIN IN VIEWCENTRALE)
                        // WE SEND TO THE SUIT INFORMATION ABOUT FREQUENCY AND WAVEFORM OF THE NEXT EXERCIISE
                        //todo: check if the part after && is correct
                        if(demostrationMode ==false && indexExercise < fullList.size())
                        {
                            new Thread(()-> {
                                try {
                                    client.setWaveForm(fullList.get(indexExercise).getWave_form());

                                    Thread.sleep(60);
                                    client.setFrequency(fullList.get(indexExercise).getFrequency());

                                } catch (Exception e) {
                                    Log.d("Activity Allenamento", "error in sendStartExercise ");
                                }
                            }).start();
                        }
                        //UPDATE INFORMATION MUSCLE BASED ON THE INFO SAVED ON THE muscleLedger
                        for(int k=0;k<fullList.get(indexExercise).muscle_list.size();k++)
                        {
                            //prendo il valore dell'intensità
                            Byte intesity= muscleLedger.get(fullList.get(indexExercise).muscle_list.get(k).getTheCode());
                            //aggiorno tale valore
                            fullList.get(indexExercise).muscle_list.get(k).setIntensity(intesity);
                        }



                        //EVERY TIME WE FINISH AN EXERCISE OF A PLAN WE GO ON PAUSE
                        play.setImageDrawable(getDrawable(R.drawable.ic_play_plan));
                        if(client != null && demostrationMode ==false)
                        {
                            client.sendStopCommand();

                        }
                        //AND OF COURSE WE ARE NOT ON TRACK
                        onTrack =false;

                        pauseTimer();
                        workoutStatus.post(new Runnable() {
                            @Override
                            public void run() {
                                workoutStatus.setText("Pausa");
                            }
                        });

                        //LOADING OF THE NEXT GIF
                        workoutVideo.post(new Runnable() {
                            @Override
                            public void run() {
                                StorageReference ref = storageReference.child("gif/"+fullList.get(indexExercise).getVideo());

                                ref.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                    @Override
                                    public void onSuccess(Uri uri) {
                                        Glide.with(getApplicationContext()).asGif().load(uri).into(workoutVideo);
                                    }
                                }).addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull Exception e) {
                                        //IN CASE OF FAILURE IT LOADS THIS ONE
                                        Glide.with(context).asGif().load("https://media.giphy.com/media/nqCH5dsIn99cmSPVrd/giphy.gif").into(workoutVideo);

                                    }
                                });

                            }
                        });

                        //RESET OF THE EXERCISE'S PROGRESSBAR
                        this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                recyclerAdapterWorkoutList.setProgress(0);

                            }
                        });

                        //ANIMATION OF PLAY BUTTON
                        play.post(new Runnable() {
                            @Override
                            public void run() {
                                play.setImageDrawable(getDrawable(R.drawable.ic_play_plan_pulsing));
                                scaleDown = ObjectAnimator.ofPropertyValuesHolder(
                                        play,
                                        PropertyValuesHolder.ofFloat("scaleX", 1.1f),
                                        PropertyValuesHolder.ofFloat("scaleY", 1f));
                                scaleDown.setDuration(310);
                                scaleDown.setRepeatCount(ObjectAnimator.INFINITE);
                                scaleDown.setRepeatMode(ObjectAnimator.REVERSE);
                                scaleDown.start();
                            }
                        });

                        endedExercise();
                        Thread.sleep(60);
                    }
                }
                Log.d(TAG, "workot completed");
            }catch (Exception e) {
                Log.d(TAG, "Ops we have an error: "+e);
                e.printStackTrace();
                pauseTimer();
            }}).start();
    }



    private void endedExercise() {
        //lo chiamiamo quando finsce un esercizio
        durata_esercizio = -1;
        stimolazione_esercizio = -1;
        riposo_esercizio = -1;

        backup_durata_esercizio = -1;
        backup_stimolazione_esercizio = -1;
        backup_riposo_esercizio = -1;



        //gestione dell'allenamento
        ///fine è orario di fine scheda
        fine=0;
        stimolare=false;
        riposare = false;
        prossima_stimolazione = 0;
        prossimo_riposo=0;





    }

    public void click_stop(View view) {
        //todo:posizionare la prossima riga in un posto migliore
        //Glide.with(context).asGif().load("https://media.giphy.com/media/xOr2qyP3HHpLopMbLj/giphy.gif").dontAnimate().into(workoutVideo);

        workoutVideo.setImageDrawable(null);
        durata_scheda=backup_durata_scheda;
        pauseTimer();
        onTrack = false;
        endedExercise();
        indexExercise=0;
        if(demostrationMode ==false)
        {
            client.sendStopCommand();
        }
        workoutStatus.setText("Premi play per iniziare");
        progressBarExercise.setProgress(0);
        progressBarText.setText("");
        play.setImageDrawable(getDrawable(R.drawable.ic_play_plan));
        //resetto la recy view
        recyclerAdapterWorkoutList.setProgress(0);
        //resetto la scheda
        ((Activity) this).runOnUiThread(()-> {

            recyclerAdapterWorkoutList.setDataset(fullList);
        });
        //resetto onda e frequnza per il primo eserciizo
        //carico frequenza e onda
        if(demostrationMode ==false)
        {
            new Thread(()-> {
                try {
                    client.setWaveForm(fullList.get(0).getWave_form());

                    Thread.sleep(60);
                    client.setFrequency(fullList.get(0).getFrequency());

                } catch (Exception e) {
                    Log.d("Activity Allenamento", "error in sendStartExercise ");
                }
            }).start();
        }

        workoutVideo.post(new Runnable() {
            @Override
            public void run() {
                //INIZIO PEZZO CHE CARICA GIF
                StorageReference ref = storageReference.child("gif/"+fullList.get(0).getVideo());

                ref.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                    @Override
                    public void onSuccess(Uri uri) {
                        Glide.with(getApplicationContext()).asGif().load(uri).into(workoutVideo);
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Glide.with(context).asGif().load("https://media.giphy.com/media/nqCH5dsIn99cmSPVrd/giphy.gif").into(workoutVideo);

                    }
                });

            }
        });

        //animo tasto play e gli faccio cambiare colore
        play.setImageDrawable(getDrawable(R.drawable.ic_play_plan_pulsing));
        scaleDown.start();

    }

    public void startTimer()
    {

        CountDownTimer = new android.os.CountDownTimer(durata_scheda,secondo){
            @Override
            public void onTick(long millisUntilFinished) {
                String tempoRestante = String.format(Locale.ITALIAN, "%02d:%02d",
                        TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished),
                        TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) -
                                TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished)));
                progressBarText.setText(tempoRestante);

                progressBarExercise.setProgress((int)((backup_durata_scheda -TimeUnit.MILLISECONDS.toMillis(millisUntilFinished))/ onePercent_durata_scheda)+1);
                recyclerAdapterWorkoutList.setProgress((int)(backup_durata_esercizio - (fine - Calendar.getInstance().getTime().getTime()))/onePercent_durata_esercizio);
            }
            @Override
            public void onFinish() {
                String tempoRestante = String.format(Locale.ITALIAN, "%02d:%02d",
                        TimeUnit.MILLISECONDS.toMinutes(0),
                        TimeUnit.MILLISECONDS.toSeconds(0) -
                                TimeUnit.MINUTES.toSeconds(0));
                progressBarText.setText(tempoRestante);
                progressBarExercise.setProgress(100);
            }
        }.start();
    }

    public void pauseTimer()
    {
        if(fine != 0)
        {
            CountDownTimer.cancel();
        }


    }

    public void sounds(View view) {
        //todo: avere un flag per sapere se i suoni sono attivi o meno
        soundsEnabled =!soundsEnabled;

        //in base al flag cambiare l'icona dei suoni
        if(soundsEnabled)
        {
            sound_button.setImageDrawable(getDrawable(R.drawable.ic_sounds_yes));

        }
        else
        {
            sound_button.setImageDrawable(getDrawable(R.drawable.ic_sounds_no));

        }


    }

    private void resetAnimation(ObjectAnimator scalereset){
        play.setImageDrawable(getDrawable(R.drawable.ic_play_plan));
        scalereset = ObjectAnimator.ofPropertyValuesHolder(
                play,
                PropertyValuesHolder.ofFloat("scaleX", 1f),
                PropertyValuesHolder.ofFloat("scaleY", 1f));
        scalereset.setDuration(0);
        //scalereset.setRepeatMode(ObjectAnimator.REVERSE);
        scalereset.start();

        scaleDown = ObjectAnimator.ofPropertyValuesHolder(
                play,
                PropertyValuesHolder.ofFloat("scaleX", 1.1f),
                PropertyValuesHolder.ofFloat("scaleY", 1f));
        scaleDown.setDuration(310);

        scaleDown.setRepeatCount(ObjectAnimator.INFINITE);
        scaleDown.setRepeatMode(ObjectAnimator.REVERSE);
    }

    private void fillLedger(Map<Byte, Byte> muscleLedger) {
        Byte index = 1;
        Byte value = 20;
        for(int i=1;i<=20;i++)
        {
            muscleLedger.put(index,value);
            index++;
        }
    }
    private void updateLedger(ArrayList<Muscolo> muscle_list) {
        for(int i=0;i < muscle_list.size();i++){
            muscleLedger.put(muscle_list.get(i).getTheCode(),muscle_list.get(i).getTheIntesity());
        }
    }


}
