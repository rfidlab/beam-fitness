package com.beamdigital.beamfitness;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import static android.view.View.GONE;

public class RecyclerAdapterMuscles extends RecyclerView.Adapter<RecyclerAdapterMuscles.MuscleHolder> {

    Context context;
    String[] muscles;
    boolean showPower;

    public RecyclerAdapterMuscles(Context context, String[] muscles, boolean showPower) {
        this.context=context;
        this.muscles=muscles;
        this.showPower=showPower;
    }

    @NonNull
    @Override
    public RecyclerAdapterMuscles.MuscleHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.recycler_view_muscoli,parent,false);
        MuscleHolder  muscleHolder = new MuscleHolder(view);
        return muscleHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerAdapterMuscles.MuscleHolder holder, int position) {

        if(!showPower)
            holder.power.setVisibility(GONE);

        holder.muscle.setText(muscles[position]);

    }


    @Override
    public int getItemCount() {
            return muscles.length;
    }

    public class MuscleHolder extends RecyclerView.ViewHolder{

        TextView muscle;
        TextView power;
        public MuscleHolder(@NonNull View itemView) {
            super(itemView);
            muscle = itemView.findViewById(R.id.recyView_name_muscle);
            power = itemView.findViewById(R.id.recView_powerMuscle);
        }
    }

    public void setDataset(String[] selectedMuscles){
        this.muscles = selectedMuscles;
        notifyDataSetChanged();
    }
}
