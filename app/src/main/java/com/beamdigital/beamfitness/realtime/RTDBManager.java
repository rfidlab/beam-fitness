package com.beamdigital.beamfitness.realtime;

import android.os.Build;
import android.util.Log;

import androidx.annotation.NonNull;

import com.beamdigital.beamfitness.database.AppDatabase;
import com.beamdigital.beamfitness.database.ExerciseDao;
import com.beamdigital.beamfitness.dto.ExerciseDTO;
import com.beamdigital.beamfitness.dto.PlanDTO;
import com.beamdigital.beamfitness.model.ExerciseEntity;
import com.beamdigital.beamfitness.model.Plan;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.Iterator;

import eu.beamdigital.beamfitnesslibrary.Exercise;

// todo classe temporanea per gestire il db realtime di firebase
public class RTDBManager {

    private static final String TAG = "RTDBManager";
    
    private static RTDBManager instance;

    private int countPlans=0;
    private int countExercises=0;

    public static RTDBManager getInstance() {
        if(instance == null)
            instance = new RTDBManager();
        if(instance.countExercises==0)
        {
            instance.getAllExercises(task -> {
                if(task.isComplete() && task.isSuccessful()) {
                    DataSnapshot result = task.getResult();
                    if(result != null) {
                        for (DataSnapshot next : result.getChildren()) {
                            ExerciseDTO value = next.getValue(ExerciseDTO.class);
                            if (value != null) {
                                instance.countExercises=instance.countExercises+1;
                            }
                        }
                    }
                }
            });
        }
        if(instance.countPlans==0)
        {
            instance.getAllPlans(task -> {
                if(task.isComplete() && task.isSuccessful()) {
                    DataSnapshot result = task.getResult();
                    if(result != null) {
                        for (DataSnapshot next : result.getChildren()) {
                            PlanDTO value = next.getValue(PlanDTO.class);
                            if (value != null) {
                                instance.countPlans=instance.countPlans+1;
                            }
                        }
                    }
                }
            });
        }

        return instance;
    }

    private DatabaseReference getReference(String refName) {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        return database.getReference(refName);
    }

    private DatabaseReference getExercisesRef() {
        return getReference("exercises");
    }

    private DatabaseReference getPlansRef() {
        return getReference("plans");
    }

    public void getAllPlans(OnCompleteListener<DataSnapshot> completeListener) {
        getPlansRef().get().addOnCompleteListener(completeListener);
    }

    /// todo: luca guardami
    public void getAllExercises(OnCompleteListener<DataSnapshot> completeListener) {
        getExercisesRef().get().addOnCompleteListener(completeListener);
    }

    public void addAllPlansToRTDB(Plan...rest) {
        for(Plan p : rest) {
            Log.d(TAG, "addAllPlansToRTDB: p: " + p.getCover());
            countPlans=countPlans+1;
            p.id_plan=countPlans;
            getPlansRef().push().setValue(new PlanDTO(p));
        }
    }

    public void updataPlansToRTDG(Plan plan) {
        instance.getAllPlans(task -> {
            if(task.isComplete() && task.isSuccessful()) {
                DataSnapshot result = task.getResult();
                if(result != null) {
                    for (DataSnapshot next : result.getChildren()) {
                        Log.d("Luca","key "+next.getKey());
                        Log.d("Luca","key "+next.getChildren().toString());

                        PlanDTO value = next.getValue(PlanDTO.class);
                        if (value != null) {
                            if(value.getId()==plan.id_plan)
                            {
                                getPlansRef().child(next.getKey()).setValue(new PlanDTO(plan));

                            }
                        }
                    }
                }
            }
        });
    }

    public void updateExerciseToRTDB(ExerciseEntity exercise){
        //per prima cosa aggiorno l'esercizio all'interno del RTDB
        instance.getAllExercises(task -> {
            if(task.isComplete() && task.isSuccessful()) {
                DataSnapshot result = task.getResult();
                if(result != null) {
                    for (DataSnapshot next : result.getChildren()) {
                        ExerciseDTO value = next.getValue(ExerciseDTO.class);
                        if (value != null) {
                            if(value.getId()==exercise.id_exercise)
                            {
                                getExercisesRef().child(next.getKey()).setValue(new ExerciseDTO(exercise));
                            }
                        }
                    }
                }
            }
        });
        //QUESTA VARIABILE MI FA SAPERE SE DEVO AGGIORNARE IL PIANO, QUESTO ACCADE QUANDO TROVA UN ESERCIZIO CON LO STESSO ID DI QUELLO DATO IN ARGOMENTO
        boolean updatePlan=false;
        //a questo punto cerco un tutte le schede l'esercizio per poterlo aggiornare
        instance.getAllPlans(task -> {
            if(task.isComplete() && task.isSuccessful()) {
                DataSnapshot result = task.getResult();
                if(result != null) {
                    for (DataSnapshot next : result.getChildren()) {
                        Log.d("Luca","key "+next.getKey());
                        Log.d("Luca","key "+next.getChildren().toString());

                        PlanDTO value = next.getValue(PlanDTO.class);
                        if (value != null) {
                            //mi salvo il plan
                            Plan plan=value.revertDTO();
                            //cerco in riscaldamento
                            for(int i=0;i< plan.getPretraining().size();i++)
                            {
                                if(plan.getPretraining().get(i).id_exercise == exercise.id_exercise)
                                {
                                    //updatePlan=true;
                                    //todo: modifica quell'esercizio dentro plan
                                    plan.updatePreTraining(i,exercise);
                                }
                            }
                            //cero in allenamento
                            for(int i=0;i< plan.getTraining().size();i++)
                            {
                                if(plan.getTraining().get(i).id_exercise == exercise.id_exercise)
                                {
                                    //updatePlan=true;
                                    //todo: modifica quell'esercizio dentro plan
                                    plan.updateTraining(i,exercise);
                                }
                            }
                            //cero in postAllenamento
                            for(int i=0;i< plan.getPosttraining().size();i++)
                            {
                                if(plan.getPosttraining().get(i).id_exercise == exercise.id_exercise)
                                {
                                    //updatePlan=true;
                                    //todo: modifica quell'esercizio dentro plan
                                    plan.updatePostTraining(i,exercise);
                                }
                            }

                            instance.getPlansRef().child(next.getKey()).setValue(new PlanDTO(plan));
                        }
                    }
                }
            }
        });
    }



    public void addAllExercisesToRTDB(ExerciseEntity...rest) {
        for(ExerciseEntity e : rest) {
            Log.d(TAG, "addAllExercisesToRTDB: e: " + e);
            //todo:potrei modificare l'id qui dentro
            countExercises=countExercises+1;
            e.id_exercise=countExercises;
            getExercisesRef().push().setValue(new ExerciseDTO(e));
        }
    }

    public void removePlanToRTDB(Plan plan){
        instance.getAllPlans(task -> {
            if(task.isComplete() && task.isSuccessful()) {
                DataSnapshot result = task.getResult();
                if(result != null) {
                    for (DataSnapshot next : result.getChildren()) {

                        PlanDTO value = next.getValue(PlanDTO.class);
                        if (value != null) {
                            if(value.getId()==plan.id_plan)
                            {
                                //LO RIMUOVO DA FIREBASE
                                next.getRef().removeValue();
                                //LO RIMUOVO DAL TELEFONO
                            }
                        }
                    }
                }
            }
        });
    }

    public void removeExerciseToRTDB(ExerciseEntity exercise){
        //per prima cosa rimuovo l'esercizio all'interno del RTDB
        instance.getAllExercises(task -> {
            if(task.isComplete() && task.isSuccessful()) {
                DataSnapshot result = task.getResult();
                if(result != null) {
                    for (DataSnapshot next : result.getChildren()) {
                        ExerciseDTO value = next.getValue(ExerciseDTO.class);
                        if (value != null) {
                            if(value.getId()==exercise.id_exercise)
                            {
                                next.getRef().removeValue();
                            }
                        }
                    }
                }
            }
        });

        //ORA LO CERCO NEI VARI PLAN E LO RIMUOVO
        instance.getAllPlans(task -> {
            if(task.isComplete() && task.isSuccessful()) {
                DataSnapshot result = task.getResult();
                if(result != null) {
                    for (DataSnapshot next : result.getChildren()) {
                        Log.d("Luca","key "+next.getKey());
                        Log.d("Luca","key "+next.getChildren().toString());

                        PlanDTO value = next.getValue(PlanDTO.class);
                        if (value != null) {
                            //mi salvo il plan
                            Plan plan=value.revertDTO();
                            //cerco in riscaldamento
                            for(int i=0;i< plan.getPretraining().size();i++)
                            {
                                if(plan.getPretraining().get(i).id_exercise == exercise.id_exercise)
                                {
                                    //updatePlan=true;
                                    //todo: modifica quell'esercizio dentro plan
                                    plan.removePreTraining(i);
                                }
                            }
                            //cero in allenamento
                            for(int i=0;i< plan.getTraining().size();i++)
                            {
                                if(plan.getTraining().get(i).id_exercise == exercise.id_exercise)
                                {
                                    //updatePlan=true;
                                    //todo: modifica quell'esercizio dentro plan
                                    plan.removeTraining(i);
                                }
                            }
                            //cero in postAllenamento
                            for(int i=0;i< plan.getPosttraining().size();i++)
                            {
                                if(plan.getPosttraining().get(i).id_exercise == exercise.id_exercise)
                                {
                                    //updatePlan=true;
                                    //todo: modifica quell'esercizio dentro plan
                                    plan.removePostTraining(i);
                                }
                            }

                            instance.getPlansRef().child(next.getKey()).setValue(new PlanDTO(plan));
                        }
                    }
                }
            }
        });

    }

}
