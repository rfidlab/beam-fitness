package com.beamdigital.beamfitness;

import android.os.Handler;
import android.os.Message;

public class MyHandler extends Handler {
    private static MyHandler handler =new MyHandler();          //饿汉式单例模式 modello singleton affamato
    private int j=0; //questa variabile in futuro a noi non servirà, ma dovremmo includere nel messaggio il tipo di waveform
    private  boolean[] arr1; //questo forse è l'array delle zone muscolari?
    private MyHandler(){
    }
    public static MyHandler getInstance(){
        return handler;
    }

    @Override
    public void handleMessage(Message msg) {//UI线程中执行 (esegui nel thread dell'user interface)
        //todo: capire bene che significa questo valore, potrebbe essere l'elenco dei muscoli selezionati, QUASI SICURO è COSì
        arr1=(boolean[]) msg.obj;
        switch(msg.what){
            case 0x01:{     //start
                MainActivity.Main.sendStartCommand();
            }break;
            case 0x02:{     //over
                MainActivity.Main.sendStopCommand();
            }break;
            case 0x03:{ //设置强度,并切换波形 imposta intensita e cambia la forma d'onda
                //VEDI TABELLA 3
                byte[] buff=new byte[10];
                for(int i=1;i<11;i++){
                    buff[0]=(byte)0xA8;     //帧头 intestazione fram
                    buff[1]=(byte)i;        //肌肉区 area muscolare

                    buff[2]=(byte)0x01;     //指令操作 0x00：清除数据 0x01:设置数据 operazione d'istruzione 00: elimina i dati 01: imposta i dati
                    //boolean b = arr[i];

                    // nel doc indica un muscle area switch state
                    /// da 1 a 8 sono i muscoli con indici che vanno da 0 a 7 nell'array
                    /// più precisamente sono i muscoli che vanno da abdominal muscle a waist
                    if(i<=8){                //0-7组输出 uscita di gruppo (credo serva per indicare la fine sessione)
                        if (arr1[i-1]){     //开关指令 cambia istruzione
                            buff[3]=0x01;
                        }else{
                            buff[3]=0x00;
                        }
                    }else if(i==9||i==10){
                        /// i === 9 bicipital muscle
                        /// i == 10 è externus abdominis
                        if (arr1[i]){           //开关指令
                            buff[3]=0x01;
                        }else{
                            buff[3]=0x00;
                        }
                    }


                    buff[4]=(byte)msg.arg1;//强度指令 istruzioni per forza (valore di intensità)
                    buff[5]=(byte)0x00;
                    buff[6]=(byte)0x00;
                    buff[7]=(byte)0x00;
                    buff[8]=(byte)0x00; //questi sempre a zero, anche negli esempi
                    buff[9]=(byte)0x8A; //chiusura
                    MainActivity.Main.writeRXCharacteristic(buff);
                    try {
                        Thread.sleep(20);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }break;
            case 0x04:{ //发送心跳 invia battito cardiaco
                //QUESTA è L'ULTIMA TABELLA CHE STA VERSO LA FINE
                byte[] buff=new byte[10];
                j++;if(j>4)j=0;
                for(int i=1;i<11;i++){
                    buff[0]=(byte)0xA8; //INIZIA SEMPRE IN QUESTO MODO, QUINDI COME LI DIVIDO?
                    buff[1]=(byte)i;   //muscle area
                    buff[2]=(byte)0x01;  //01 setting data 02:heartbeat command
                    if(i<=8){                //0-7组输出
                        if (arr1[i-1]){     //开关指令 cambia istruzione (muscle area switch state)
                            buff[3]=0x01;
                        }else{
                            buff[3]=0x00;
                        }
                    }else if(i==9||i==10){
                        if (arr1[i]){           //开关指令 popolera l'array con msg.obj quindi potrebbero essere le aree muscolari?
                            buff[3]=0x01;
                        }else{
                            buff[3]=0x00;
                        }
                    }
                    buff[4]=(byte)msg.arg1;
                    buff[5]=(byte)0x00;
                    buff[6]=(byte)0x00;
                    buff[7]=(byte)0x00;
                    buff[8]=(byte)0x00; //stanno sempre a 0
                    buff[9]=(byte)0x8A; //fine
                    MainActivity.Main.writeRXCharacteristic(buff);
                    try {
                        Thread.sleep(20);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                //HO COMMENTATO QUESTA PARTE PERCHè NON DOVREBBE SERVIRE PIù
                /*
                byte[] buff2=new byte[3];
                buff2[0]=(byte)0xA6;
                //buff2[1]=(byte)(0x01<<j); //quindi ogni volta che passi per questo case fa un tipo di onda diversa
                buff2[1]=(byte)(0x01); //gli facciamo fare solo sinewave
                buff2[2]=(byte)0x6A;
                MainActivity.Main.writeRXCharacteristic(buff2);
                 */
            }break;
            //CASI SET WAVE
            case 0x05: {//sineWave01
                MainActivity.Main.setSineWave01();
            }break;
            case 0x06: {//tringelWave
                MainActivity.Main.setTrinagleWave();
            }break;
            case 0x07: {//sawtoolWave
                MainActivity.Main.setSawtoothWave();
            }break;
            case 0x08: {//exponentialWave
                MainActivity.Main.setExponentialWave();
            }break;
            case 0x09: { //sineWave04
                MainActivity.Main.setSineWave04();
            }break;
            case 0x11: { //sineWave04
                    //setto l'onda
                    MainActivity.Main.setTrinagleWave();
                    //seleziono il muscolo da stimolare
                    byte[] buff=new byte[10];
                    buff[0]=(byte)0xA8;
                    buff[1]=(byte)2; //muscolo
                    buff[2]=(byte)0x01; //setting data
                    buff[3]=0x01; // questo va capito bene, lui fa delle casistiche che non mi sono chiarissime
                    buff[4]=(byte)msg.arg1;//强度指令 istruzioni per forza (valore di intensità)
                    buff[5]=(byte)0x00;
                    buff[6]=(byte)0x00;
                    buff[7]=(byte)0x00;
                    buff[8]=(byte)0x00; //questi sempre a zero, anche negli esempi
                    buff[9]=(byte)0x8A; //chiusura
                    MainActivity.Main.writeRXCharacteristic(buff);
                }break;

        }

    }
}
