package com.beamdigital.beamfitness.fragment;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.util.DBUtil;

import com.beamdigital.beamfitness.R;
import com.beamdigital.beamfitness.RecyclerAdapterFilters;
import com.beamdigital.beamfitness.RecyclerAdapterVertical;
import com.beamdigital.beamfitness.database.AppDatabase;
import com.beamdigital.beamfitness.database.ExerciseDao;
import com.beamdigital.beamfitness.database.PlanDao;
import com.beamdigital.beamfitness.database.PlanManager;
import com.beamdigital.beamfitness.dto.ExerciseDTO;
import com.beamdigital.beamfitness.dto.PlanDTO;
import com.beamdigital.beamfitness.model.ExerciseEntity;
import com.beamdigital.beamfitness.model.Muscolo;
import com.beamdigital.beamfitness.model.Plan;
import com.beamdigital.beamfitness.realtime.RTDBManager;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import static eu.beamdigital.beamfitnesslibrary.Constants.COSINE_WAVE;
import static eu.beamdigital.beamfitnesslibrary.Constants.EXPONENTIAL_WAVE;
import static eu.beamdigital.beamfitnesslibrary.Constants.SAWTOOTH_WAVE;
import static eu.beamdigital.beamfitnesslibrary.Constants.SINE_WAVE;
import static eu.beamdigital.beamfitnesslibrary.Constants.TRIANGLE_WAVE;

/// todo questo deve essere usato sia per la sezione Home sia per sezione Esplora
public class HomeFragment extends Fragment implements Observer<List<Plan>> {

    private static final String TAG = HomeFragment.class.getSimpleName();

    /// todo: luca guardami - ottieni l'istanza del manager del database realtime
    RTDBManager manager = RTDBManager.getInstance();

    private String m = null;

    public void setMessaggio(String m) {
        this.m = m;
    }

    //exercises
    RecyclerView listaEsercizi;
    RecyclerAdapterVertical recyclerAdapterExercises;
    //filters
    RecyclerView filter;
    RecyclerAdapterFilters recyclerAdapterFilters;

    LiveData<List<Plan>> plansLiveData;



    @Nullable
    @Override
    public View onCreateView(@Nullable LayoutInflater inflater, @Nullable  ViewGroup container, @Nullable  Bundle savedInstanceState) {
        // per avere il context dell'activity chiamante
        View rootView = inflater.inflate(R.layout.home_fragment, container, false);

        PlanDao planDao = AppDatabase.getDatabase(getContext()).getPlanDao();
        plansLiveData = planDao.findAll();
        ExerciseDao exerciseDao = AppDatabase.getDatabase(getContext()).ExerciseDao();

        plansLiveData.observe(this, this);



        /// su rootview puoi fare il findviewbyid degli oggetti che hai definito nel layout
        TextView messaggio = rootView.findViewById(R.id.titolo);

        if(m != null) {
            messaggio.setText("Filtri");
            Log.d("Luca","filtriamo HomeFrag");
            filter = rootView.findViewById(R.id.filters);
            recyclerAdapterFilters = new RecyclerAdapterFilters(getContext());
            filter.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.HORIZONTAL, false));
            filter.setAdapter(recyclerAdapterFilters);

        }
        else{
            messaggio.setText("");

        }

        recyclerAdapterExercises = new RecyclerAdapterVertical(getContext());
        listaEsercizi = rootView.findViewById(R.id.exerciseRecView);
        listaEsercizi.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false));
        listaEsercizi.setAdapter(recyclerAdapterExercises);

        manager.getAllExercises(task -> {
            if(task.isComplete() && task.isSuccessful()) {
                DataSnapshot result = task.getResult();
                if(result != null) {
                    for (DataSnapshot next : result.getChildren()) {
                        ExerciseDTO value = next.getValue(ExerciseDTO.class);
                        if (exerciseDao != null && value != null) {
                            ExerciseEntity tmp = exerciseDao.getID(value.getId());
                            if(tmp == null) {
                                Log.d(TAG, "onCreateView: inserting exercise " + value.getId());
                                exerciseDao.insert(value.revertDTO());
                            } else {
                                Log.d(TAG, "onCreateView: avoiding duplicate of excercise " + value.getId());
                            }
                        }
                    }
                }
            }
        });

        manager.getAllPlans(task -> {
            if(task.isComplete() && task.isSuccessful()) {
                DataSnapshot result = task.getResult();
                if(result != null) {
                    for (DataSnapshot next : result.getChildren()) {
                        PlanDTO value = next.getValue(PlanDTO.class);
                        if (planDao != null && value != null) {
                            Plan tmp = planDao.getID(value.id);
                            if(tmp == null) {
                                Log.d(TAG, "onCreateView: inserting plan " + value.id);
                                planDao.insert(value.revertDTO());
                            } else {
                                Log.d(TAG, "onCreateView: avoiding duplicate of plan " + value.id);
                            }
                        }
                    }
                }
            }
        });


        return rootView;
    }

    @Override
    public void onChanged(List<Plan> plans) {
//        if(plans.size()==0)
//        {
//            plansGenerator(plans);
//        }

        Log.d(TAG, "onChanged: plans.size: " + plans.size());
        Plan[] pls = new Plan[plans.size()];
        plans.toArray(pls);
        Log.d(TAG, "onChanged: plans: " + Arrays.toString(pls));
        recyclerAdapterExercises.setDataset(plans);
    }



    private ExerciseEntity newExerciseEntity(String name, int frequency, int wave, int stimulation, int rest, int duration, ArrayList<Muscolo> muscleList){
         AppDatabase db;
        db = AppDatabase.getDatabase(getContext());

        ExerciseEntity fine= new ExerciseEntity();

        //NOME
        fine.name= name;
        //FREQUENZA
        fine.frequency = (byte) frequency;
        //ONDA
        if (wave==0){
            fine.wave_form = SINE_WAVE;
        }
        else if (wave==1){
            fine.wave_form = TRIANGLE_WAVE;
        }
        else if (wave==2){
            fine.wave_form = COSINE_WAVE;
        }
        else if (wave==3){
            fine.wave_form = SAWTOOTH_WAVE;
        }
        else if (wave==4){
            fine.wave_form = EXPONENTIAL_WAVE;
        }

        //STIMOLAZIONE
        fine.stimulation_sec= (byte) stimulation;
        //RIPOSO
        fine.rest_sec= (byte) rest;
        //DURATA
        fine.duration_min= (byte) duration;

        //muscoli
        fine.muscle_list=muscleList;

        Thread thread = new Thread(() -> {
            db.ExerciseDao().insert(fine);
        });
        thread.start();

        return fine;
    }

    private void plansGenerator(List<Plan> plans) {

        Muscolo addominaliLaterali = new Muscolo("Addominali Laterali", 1, true, 20);
        Muscolo petto = new Muscolo("Petto", 2, true, 20);
        Muscolo braccioSinistro = new Muscolo("Braccio Sinistro", 3, true, 20);
        Muscolo lombari = new Muscolo("Lombari", 4, true, 20);
        Muscolo quadricipiteSinistro = new Muscolo("Quadricipite Sinistro", 5, true, 20);
        Muscolo quadricipiteDestro = new Muscolo("Quadricipite Destro", 6, true, 20);
        Muscolo glutei = new Muscolo("Glutei", 7, true, 20);
        Muscolo spalle = new Muscolo("Spalle", 8, true, 20);
        Muscolo braccioDestro = new Muscolo("Braccio Destro", 9, true, 20);
        Muscolo addominali = new Muscolo("Addominali", 10, true, 20);

        ExerciseEntity addome = newExerciseEntity("Addominali",25,1,5,5,3, new ArrayList<Muscolo>(Arrays.asList(petto,addominaliLaterali)));
        addome.setId_exercise(1);
        ExerciseEntity braccia = newExerciseEntity("Braccia",25,1,5,5,3, new ArrayList<Muscolo>(Arrays.asList(braccioDestro,braccioSinistro)));
        braccia.setId_exercise(2);
        ExerciseEntity gambe = newExerciseEntity("Gambe",25,1,5,5,3, new ArrayList<Muscolo>(Arrays.asList(quadricipiteDestro,quadricipiteSinistro)));
        gambe.setId_exercise(3);
        ExerciseEntity body = newExerciseEntity("Full-Body",25,1,5,5,3, new ArrayList<Muscolo>(Arrays.asList(addominali,braccioDestro,spalle,glutei,quadricipiteDestro,quadricipiteSinistro,addominaliLaterali,petto,braccioSinistro,lombari)));
        body.setId_exercise(4);

//        AppDatabase.getDatabase(getContext()).ExerciseDao().insert(addome);
//        AppDatabase.getDatabase(getContext()).ExerciseDao().insert(braccia);
//        AppDatabase.getDatabase(getContext()).ExerciseDao().insert(gambe);
//        AppDatabase.getDatabase(getContext()).ExerciseDao().insert(body);

        /// todo: luca guardami - scrivi uno o più esercizi sul database realtime
        manager.addAllExercisesToRTDB(addome, braccia, gambe, body);

        PlanManager pm = new PlanManager();
        //CREATI GLI ESERCIZI, PASSIAMO ALLE SCHEDE
        Plan plan = new Plan();
        //ALLENAMENTO LUNEDI
        plan.setId_plan(1);
        plan.setName("Lunedì");
        plan.setPretraining(new ArrayList<ExerciseEntity>(Arrays.asList(addome)));
        plan.setTraining(new ArrayList<ExerciseEntity>(Arrays.asList(body,addome)));
        plan.setPosttraining(new ArrayList<ExerciseEntity>(Arrays.asList(gambe,body)));
        plans.add(plan);
        pm.setPlan(plan);
        pm.savePlan(getContext());


        //ALLENAMENTO MERCOLEDì
        Plan plan1 = new Plan();
        plan1.setId_plan(2);
        plan1.setName("Mercoledì");
        plan1.setPretraining(new ArrayList<ExerciseEntity>(Arrays.asList(braccia)));
        plan1.setTraining(new ArrayList<ExerciseEntity>(Arrays.asList(body,gambe,braccia)));
        plan1.setPosttraining(new ArrayList<ExerciseEntity>(Arrays.asList(gambe,addome)));
        plans.add(plan1);
        pm = new PlanManager();
        pm.setPlan(plan1);
        pm.savePlan(getContext());


        //ALLENAMENTO VENERDì
        Plan plan2 = new Plan();
        plan2.setId_plan(3);
        plan2.setName("Venerdì");
        plan2.setPretraining(new ArrayList<ExerciseEntity>(Arrays.asList(body,gambe)));
        plan2.setTraining(new ArrayList<ExerciseEntity>(Arrays.asList(addome,gambe,braccia)));
        plan2.setPosttraining(new ArrayList<ExerciseEntity>(Arrays.asList(body,gambe)));
        plans.add(plan2);
        pm = new PlanManager();
        pm.setPlan(plan2);
        pm.savePlan(getContext());

        Log.d("Luca","Non avevamo allenamenti");
        /// todo: luca guardami - scrivi uno o più schede sul database realtime
        manager.addAllPlansToRTDB(plan, plan1, plan2);
    }

}
