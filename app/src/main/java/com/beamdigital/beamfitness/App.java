package com.beamdigital.beamfitness;

import android.app.Application;

import com.beamdigital.beamfitness.database.PlanManager;

public class App extends Application {

    private static final PlanManager planManager = new PlanManager();

    public static PlanManager getPlanManager() {
        return App.planManager;
    }
}
