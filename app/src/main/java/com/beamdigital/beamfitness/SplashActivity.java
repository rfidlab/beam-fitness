package com.beamdigital.beamfitness;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

public class SplashActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //todo: abbiamo permessi da chiedere? Al momento non mi sembra
        proceed();
    }

    private void proceed() {
        startActivity(new Intent(this, LoginActivity.class));
        finish();
    }
}
