package com.beamdigital.beamfitness;

import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class SignUpActivity extends AppCompatActivity {

    private Button sign_up_complete;
    private EditText email;
    private EditText password;

    protected void onCreate (Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView (R.layout.sign_up);
        sign_up_complete.findViewById(R.id.signUpComplete);
        email.findViewById(R.id.campoEmail);
        password.findViewById(R.id.campoEmail);
    }


    private void complete_sign_up() {
        if(email.length()>0 && password.length()>0)
        {
            if(email.toString().contains("@")){
                //todo: passa alla main Activity
            }
            else
            {
                Toast.makeText(SignUpActivity.this, "Devi inserire un indirizzo email", Toast.LENGTH_SHORT).show();
            }

        }
        else
        {
            Toast.makeText(SignUpActivity.this, "Uno dei due campi è vuoto", Toast.LENGTH_SHORT).show();
        }

    }
}
