package com.beamdigital.beamfitness;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.beamdigital.beamfitness.database.AppDatabase;
import com.beamdigital.beamfitness.database.ExerciseDao;
import com.beamdigital.beamfitness.database.PlanManager;
import com.beamdigital.beamfitness.model.ExerciseEntity;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class ViewCercaEsercizio extends AppCompatActivity implements Observer<List<ExerciseEntity>>, RecyclerAdapterExercises.ExerciseClickListener {
    private static final String TAG = ViewCercaEsercizio.class.getSimpleName();

    RecyclerView listExercise;
    RecyclerAdapterExercises recyclerAdapterExercises;
    PlanManager planManager = App.getPlanManager();


    TextView title;
    TextView riepilogoDurata;

    private LiveData<List<ExerciseEntity>> exercisesLiveData;

    //todo: il mockup dice aggiungi a riscaldamento/allenamento/defaticamento c'è un modo
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.view_cerca_esercizio);

        Bundle bundle = getIntent().getExtras();



        title = findViewById(R.id.title_cerca_eserciizo);
        title.setText(bundle.getString("ex", ""));

        riepilogoDurata= findViewById(R.id.riepilogoDurata);
        riepilogoDurata.setText(String.format(Locale.ITALIAN, "%dm", planManager.duration()));
    }

    private PlanManager.TrainingPhase getType(String ex) {
        if("Aggiungi Riscaldamento".equals(ex))
            return PlanManager.TrainingPhase.PreTraining;

        if("Aggiungi Allenamento".equals(ex))
            return PlanManager.TrainingPhase.Training;

        if("Aggiungi Defaticamento".equals(ex))
            return PlanManager.TrainingPhase.PostTraining;

        return null;
    }

    //todo: qui l'utente ha scelto il tipo di esercizio che vuole fare e quindi va aggiunto alla scheda
    public void clickEsercizio(View view) {

    }

    public void creaEsercizio(View view) {
        //todo: passare all'altra activity, chiudendo questa qui
        startActivity(new Intent(this, ViewCreaEsercizio.class));
    }

    @Override
    public void onChanged(List<ExerciseEntity> exerciseList) {
        recyclerAdapterExercises.setDataset(exerciseList);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(exercisesLiveData != null)
            exercisesLiveData.removeObservers(this);
    }

    @Override
    public void onclick() {
        riepilogoDurata.setText(String.format(Locale.ITALIAN, "%dm", planManager.duration()));
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();

        Bundle bundle = getIntent().getExtras();

        //questo va bene qui
        AppDatabase db = AppDatabase.getDatabase(this);
        ExerciseDao exerciseDao = db.ExerciseDao();
        exercisesLiveData = exerciseDao.findAll();
        exercisesLiveData.observe(this, this);

        //recyclerView
        listExercise = findViewById(R.id.lista_esercizi);
        //settiamo il LayoutManager
        listExercise.setLayoutManager((new LinearLayoutManager(this, RecyclerView.VERTICAL, false)));
        //todo: fatti 2 costruttori per quando non vuoi far vedere la potenza
        PlanManager.TrainingPhase type = getType(bundle.getString("ex"));
        recyclerAdapterExercises = new RecyclerAdapterExercises(this, new ArrayList<>(0),true);
        recyclerAdapterExercises.setType(type);

        //intestiamo l'adapter alla nostra recycler view
        listExercise.setAdapter(recyclerAdapterExercises);
    }
}
