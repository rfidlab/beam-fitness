package com.beamdigital.beamfitness;

import android.Manifest;
import android.app.Dialog;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.CountDownTimer;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.room.Room;

import com.beamdigital.beamfitness.database.AppDatabase;
import com.beamdigital.beamfitness.database.ExerciseDao;
import com.beamdigital.beamfitness.model.ExerciseEntity;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import eu.beamdigital.beamfitnesslibrary.BluetoothFitnessClient;
import eu.beamdigital.beamfitnesslibrary.Exercise;
import eu.beamdigital.beamfitnesslibrary.InfoMuscolo;

import static eu.beamdigital.beamfitnesslibrary.Constants.COSINE_WAVE;
import static eu.beamdigital.beamfitnesslibrary.Constants.EXPONENTIAL_WAVE;
import static eu.beamdigital.beamfitnesslibrary.Constants.SAWTOOTH_WAVE;
import static eu.beamdigital.beamfitnesslibrary.Constants.SINE_WAVE;
import static eu.beamdigital.beamfitnesslibrary.Constants.TRIANGLE_WAVE;


@EActivity(R.layout.demo_controller)

public class DemoActivityController extends AppCompatActivity implements BluetoothFitnessClient.ChangeBleFlagListener{


    //tasti di connessione bluetooth con la tuta
    @ViewById(R.id.sesto)
    ImageButton connection_bt;

    //elenco dei muscoli, per cambiare l'ordine basta cambiare il numero ed utilizzare il gisuto ControllerMuscolo
    @ViewById(R.id.uno)
    ControllerMuscoloSx addominali_lat;
    @ViewById(R.id.due)
    ControllerMuscoloSx petto;
    @ViewById(R.id.tre)
    ControllerMuscoloSx braccio_dx;
    @ViewById(R.id.quattro)
    ControllerMuscoloSx lombari;
    @ViewById(R.id.cinque)
    ControllerMuscoloSx quadricipite_dx;
    @ViewById(R.id.sei)
    ControllerMuscoloDx quadricipite_sx;
    @ViewById(R.id.sette)
    ControllerMuscoloDx glutei;
    @ViewById(R.id.otto)
    ControllerMuscoloDx spalle;
    @ViewById(R.id.nove)
    ControllerMuscoloDx braccio_sx;
    @ViewById(R.id.dieci)
    ControllerMuscoloDx addominali;

    //le 5 opzioni che abbiamo nella parte alta
    @ViewById(R.id.valore_frequenza)
    TextView frequenza;
    @ViewById(R.id.valore_onda)
    TextView onda;
    @ViewById(R.id.valore_stimolazione)
    TextView stimolazione;
    @ViewById(R.id.valore_riposo)
    TextView riposo;
    @ViewById(R.id.valore_durata)
    TextView durata;

    // la textview nel box video
    @ViewById(R.id.box_video)
    ConstraintLayout box_video;
    @ViewById(R.id.video)
    TextView video;

    //box allenamento
    @ViewById(R.id.box_allenamento)
    ConstraintLayout box_allenamento;
    @ViewById(R.id.tempo_restante)
    TextView countDown;
    @ViewById(R.id.pausa)
    ImageButton pausa;
    @ViewById(R.id.play)
    ImageButton play;
    @ViewById(R.id.stato_allenamento)
    TextView statoAllenamento;


    //Utile a gestire tempi allenamento (in timestamp)
    private int durata_scheda;
    private int stimolazione_scheda;
    private int riposo_scheda;

    //questi di backup li uso per rimettere "a posto" quelli di sopra dopo delle interazioni particolari come la fine dell'allenamento o la messa in pausa
    private int backup_durata_scheda;
    private int backup_stimolazione_scheda;
    private int backup_riposo_scheda;

    //USO QUESTO BOOLEANO PER FARE PLAY/PAUSA
    private Boolean onTrack=null;

    //gestione dell'allenamento
    ///fine è orari di fine scheda
    long fine=0;
    boolean stimolare=false;
    boolean riposare = false;
    long prossima_stimolazione = 0;
    long prossimo_riposo=0;

    //GESTIONE CONNESSIONE
    //connessione alla libreria
    private BluetoothFitnessClient client;
    private boolean flag_connessione;

    //gestione infomrazioni da inviare a tuta
    private final Exercise exercise = new Exercise();

    //Orologio per il countDown
    private CountDownTimer CountDownTimer;

    //gestione del tempo espressa in timestamp
    final int secondo = 1000;
    final int minuto = 60*secondo;
    final int ora = 60*minuto;

    private AppDatabase db;
    private ExerciseDao exerciseDao;


    String[] scheda;

    @AfterViews
    protected void onCreate() {
        //i muscoli
        attivaMuscoli();


        //questo va bene qui
       db = Room.databaseBuilder(getApplicationContext(),
                AppDatabase.class, "database-exercise").build();
        exerciseDao = db.ExerciseDao();

        Thread loadT = new Thread(new Runnable() {
            @Override
            public void run() {
                scheda = db.ExerciseDao().loadNameExercises();
            }
        });
        loadT.start();





        if(ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED){//未开启定位权限
            //开启定位权限,200是标识码
            ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.ACCESS_FINE_LOCATION},200);
        }else{
            //startLocaion();//开始定位
            Toast.makeText(this,this.getString(R.string.openedlocation),Toast.LENGTH_LONG).show();
        }
        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.P) {
            Log.i("sdk_info", "sdk < 28 Q");
            if (ActivityCompat.checkSelfPermission(this,
                    Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                    || ActivityCompat.checkSelfPermission(this,
                    Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                String[] strings =
                        {Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION};
                ActivityCompat.requestPermissions(this, strings, 1);
            }
        } else {
            if (ActivityCompat.checkSelfPermission(this,
                    Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                    || ActivityCompat.checkSelfPermission(this,
                    Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED
                    || ActivityCompat.checkSelfPermission(this,
                    "android.permission.ACCESS_BACKGROUND_LOCATION") != PackageManager.PERMISSION_GRANTED) {
                String[] strings = {android.Manifest.permission.ACCESS_FINE_LOCATION,
                        android.Manifest.permission.ACCESS_COARSE_LOCATION,
                        "android.permission.ACCESS_BACKGROUND_LOCATION"};
                ActivityCompat.requestPermissions(this, strings, 2);
            }
        }

    }


    //MI SONO SCRITTO A PARTE IL METODO PER POPOLARE ED INIZIALIZZARE TUTTI I MUSCOLI
    private void attivaMuscoli(){

        addominali_lat.setMuscolo("Addominali lat.");
        //ci pensa il metodo a portare da int a byte
        addominali_lat.setTheCode(1);

        petto.setMuscolo("Petto");
        petto.setTheCode(2);

        braccio_dx.setMuscolo("Braccio dx");
        braccio_dx.setTheCode(3);

        lombari.setMuscolo("Lombari");
        lombari.setTheCode(4);

        quadricipite_dx.setMuscolo("Quadricipite dx");
        quadricipite_dx.setTheCode(5);

        quadricipite_sx.setMuscolo("Quadricipite sx");
        quadricipite_sx.setTheCode(6);

        glutei.setMuscolo("Glutei");
        glutei.setTheCode(7);

        spalle.setMuscolo("Spalle");
        spalle.setTheCode(8);

        braccio_sx.setMuscolo("Braccio sx");
        braccio_sx.setTheCode(9);

        addominali.setMuscolo("Addominali");
        addominali.setTheCode(10);

        //qui mi carico tutta la tuta dentro ensercizio
        List<InfoMuscolo> tuta = new ArrayList<>();
        tuta.add(addominali_lat);
        tuta.add(petto);
        tuta.add(braccio_dx);
        tuta.add(lombari);
        tuta.add(quadricipite_dx);
        tuta.add(quadricipite_sx);
        tuta.add(glutei);
        tuta.add(spalle);
        tuta.add(braccio_sx);
        tuta.add(addominali);
        exercise.setMuscle_list(tuta);


        //todo: popolare il multicommand con questi muscoli
    }


    //QUESTO METODO MI SEMPLIFICA LA PARTE DEI PICKER
    private Dialog set_default_dialog(int res_id_label) {
        Log.d("TAG", "set_default_dialog: " + getString(res_id_label));
        final Dialog d = new Dialog(DemoActivityController.this);
        d.setContentView(R.layout.prova_picker);

        TextView label = d.findViewById(R.id.picker_label);
        label.setText(res_id_label);
        Button b2 = d.findViewById(R.id.annulla);
        b2.setOnClickListener(v -> d.dismiss());

        return d;
    }

    //METODO PER SETTARE LA FREQUENZA (in Hz)
    public void click_frequenza(View view) {
        if(flag_connessione==true)
        {
            if(onTrack==null || fine == 0)
            {
                final Dialog d = set_default_dialog(R.string.frequency_label);
                Button b1 = d.findViewById(R.id.accetta);
                final NumberPicker np = d.findViewById(R.id.picker);
                np.setMaxValue(100);
                np.setMinValue(1);
                np.setWrapSelectorWheel(false);

                b1.setOnClickListener(v -> {
                    frequenza.setText(String.format(Locale.ITALIAN, "%d Hz", np.getValue()));
                    exercise.frequency = (byte) np.getValue();
                    d.dismiss();
                });

                d.show();

            }
            else{
                Toast.makeText(DemoActivityController.this, "Se l'allenamento è in corso non puoi modificare questo valore", Toast.LENGTH_SHORT).show();
            }

        }
        else
        {
            Toast.makeText(DemoActivityController.this, "Non sei ancora connesso alla tuta", Toast.LENGTH_SHORT).show();
        }

    }

    //METODO PER SETTARE LA FORMA D'ONDA
    public void click_onda(View view) {
        if(flag_connessione==true )
        {
            if(onTrack==null || fine == 0)
            {
                final String[] wave = new String[]{ "Sinusoidale", "Triangolare", "Quadra", "Seghettata", "Esponenziale" };
                final Dialog d = set_default_dialog(R.string.wave_form_label);
                Button b1 = d.findViewById(R.id.accetta);
                final NumberPicker np = d.findViewById(R.id.picker);
                np.setMaxValue(wave.length-1);
                np.setMinValue(0);
                np.setDisplayedValues(wave);
                np.setWrapSelectorWheel(false);

                b1.setOnClickListener(v -> {
                    onda.setText(wave[np.getValue()]);
                    if (np.getValue()==0){
                        exercise.wave_form = SINE_WAVE;
                    }
                    else if (np.getValue()==1){
                        exercise.wave_form = TRIANGLE_WAVE;
                    }
                    else if (np.getValue()==2){
                        exercise.wave_form = COSINE_WAVE;
                    }
                    else if (np.getValue()==3){
                        exercise.wave_form = SAWTOOTH_WAVE;
                    }
                    else if (np.getValue()==4){
                        exercise.wave_form = EXPONENTIAL_WAVE;
                    }
                    d.dismiss();
                });

                d.show();

            }
            else{
                Toast.makeText(DemoActivityController.this, "Se l'allenamento è in corso non puoi modificare questo valore", Toast.LENGTH_SHORT).show();
            }

        }
        else
        {
            Toast.makeText(DemoActivityController.this, "Non sei ancora connesso alla tuta", Toast.LENGTH_SHORT).show();

        }

    }

    //METODO PER SETTARE LA DURATA DELLA STIMOLAZIONE
    public void click_stimolazione(View view) {
        if(flag_connessione==true )
        {
            if(onTrack==null || fine == 0)
            {
                final Dialog d = set_default_dialog(R.string.time_label);
                Button b1 = d.findViewById(R.id.accetta);
                final NumberPicker np = d.findViewById(R.id.picker);
                np.setMaxValue(10);
                np.setMinValue(0);
                np.setWrapSelectorWheel(false);

                b1.setOnClickListener(v -> {
                    if(np.getValue()==1)
                    {
                        stimolazione.setText(String.format(Locale.ITALIAN, "%d secondo", np.getValue()));

                    }
                    else
                    {
                        stimolazione.setText(String.format(Locale.ITALIAN, "%d secondi", np.getValue()));
                    }
                    exercise.stimulation_sec= np.getValue();
                    stimolazione_scheda = (int) TimeUnit.SECONDS.toMillis(np.getValue());
                    //todo: inserire anche il stimolazione_scheda_background?

                    d.dismiss();
                });

                d.show();

            }
            else{
                Toast.makeText(DemoActivityController.this, "Se l'allenamento è in corso non puoi modificare questo valore", Toast.LENGTH_SHORT).show();
            }

        }
        else
        {
            Toast.makeText(DemoActivityController.this, "Non sei ancora connesso alla tuta", Toast.LENGTH_SHORT).show();

        }

    }

    //METODO PER SETTARE LA DURATA DEL RIPOSO
    public void click_riposo(View view) {

        if(flag_connessione==true)
        {
            if(onTrack==null || fine == 0)
            {
                final Dialog d = set_default_dialog(R.string.rest_label);
                Button b1 = d.findViewById(R.id.accetta);
                final NumberPicker np = d.findViewById(R.id.picker);
                np.setMaxValue(60);
                np.setMinValue(0);
                np.setWrapSelectorWheel(false);

                b1.setOnClickListener(v -> {
                    if(np.getValue()==1)
                    {
                        riposo.setText(String.format(Locale.ITALIAN, "%d secondo", np.getValue()));

                    }
                    else{
                        riposo.setText(String.format(Locale.ITALIAN, "%d secondi", np.getValue()));
                    }
                    exercise.rest_sec= np.getValue();
                    riposo_scheda = (int) TimeUnit.SECONDS.toMillis(np.getValue());
                    //todo: inserire anche riposo_scheda_backup??
                    d.dismiss();
                });
                d.show();
            }
            else{
                Toast.makeText(DemoActivityController.this, "Se l'allenamento è in corso non puoi modificare questo valore", Toast.LENGTH_SHORT).show();
            }

        }
        else
        {
            Toast.makeText(DemoActivityController.this, "Non sei ancora connesso alla tuta", Toast.LENGTH_SHORT).show();

        }





    }

    //METODO PER SETTARE LA DURATA DELLA SESSIONE DI ALLENAMENTO
    public void click_durata(View view) {
        if(flag_connessione==true)
        {
            if(onTrack==null || fine == 0)
            {
                final Dialog d = set_default_dialog(R.string.program_label);
                Button b1 = d.findViewById(R.id.accetta);
                final NumberPicker np = d.findViewById(R.id.picker);
                np.setMaxValue(60);
                np.setMinValue(1);
                np.setWrapSelectorWheel(false);

                b1.setOnClickListener(v -> {
                    if(np.getValue()==1){
                        durata.setText(String.format(Locale.ITALIAN, "%d minuto", np.getValue()));
                    }
                    else{
                        durata.setText(String.format(Locale.ITALIAN, "%d minuti", np.getValue()));
                    }
                    exercise.duration_min=np.getValue();
                    durata_scheda=( int )TimeUnit.MINUTES.toMillis(np.getValue());
                    //todo: inserire anche durata_scheda_background??
                    d.dismiss();
                });

                d.show();

            }
            else{
                Toast.makeText(DemoActivityController.this, "Se l'allenamento è in corso non puoi modificare questo valore", Toast.LENGTH_SHORT).show();
            }

        }
        else
        {
            Toast.makeText(DemoActivityController.this, "Non sei ancora connesso alla tuta", Toast.LENGTH_SHORT).show();

        }



    }

    //I SEI PULSANTI IN BASSO

    //SALVA
    @Click(R.id.primo)
    void click_salva()
    {
        ///todo: controlla che tutti i campi siano compilati
        if(isFilled(exercise))
        {
            if(onTrack==null || onTrack!= true)
            {
                final Dialog d = new Dialog(DemoActivityController.this);
                d.setContentView(R.layout.name_workout);
                Button b1 = (Button) d.findViewById(R.id.si);
                Button b2 = (Button) d.findViewById(R.id.no);

                //final NumberPicker np = (NumberPicker) d.findViewById(R.id.numberPicker1);
                final EditText nw = (EditText) d.findViewById(R.id.allenamento);

                b1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Thread thread = new Thread(new Runnable() {
                            @Override
                            public void run() {
                                //todo: controlla che quel nome non sia già presente, se è presente fai update

                                ExerciseEntity ex = new ExerciseEntity();
                                ex.name = nw.getText().toString();
                                ex.frequency = exercise.frequency;
                                ex.wave_form = exercise.wave_form;
                                ex.stimulation_sec = exercise.stimulation_sec;
                                ex.rest_sec = exercise.rest_sec;
                                ex.duration_min = exercise.duration_min;
                                ex.video=exercise.video;
                                if(db.ExerciseDao().findByName(ex.name)==null)
                                {
                                    db.ExerciseDao().insert(ex);
                                }
                                else {
                                    ex.id_exercise=db.ExerciseDao().findPKByName(ex.name);
                                    db.ExerciseDao().update(ex);
                                }


                            }
                        });
                        d.dismiss();
                        thread.start();
                        Toast.makeText(DemoActivityController.this, "Esercizio salvato", Toast.LENGTH_SHORT).show();

                        //todo: questo lo lasciamo? Serve per fare in modo di ricaricare subito la nuova scheda
                        Thread loadT = new Thread(new Runnable() {
                            @Override
                            public void run() {
                                scheda = db.ExerciseDao().loadNameExercises();
                            }
                        });
                        loadT.start();


                    }
                });
                b2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        d.dismiss();
                    }
                });
                d.show();
            }
            else
            {
                Toast.makeText(DemoActivityController.this, "Allenamento in corso, premi salva per abilitare l'operazione di salvataggio", Toast.LENGTH_SHORT).show();

            }

        }


    }

    //CARICA ESERCIZIO
    @Click(R.id.secondo)
    void click_impostaEsercizio()
    {
        if(flag_connessione==true)
        {
            if(fine == 0)
            {
                //todo: definire le condizioni in cui può o non essere cliccabile
                if(frequenza.getText().toString().length()>0 && onda.getText().toString().length()>0 && stimolazione.getText().toString().length()>0 && riposo.getText().toString().length()>0 && durata.getText().toString().length()>0)
                {
                    if(exercise.video!= null)
                    {
                        video.setText(exercise.video);

                    }
                    else
                    {
                        video.setText("Premi il tasto play per iniziare l'allenamento");
                    }
                    box_video.setVisibility(View.VISIBLE);
                    box_allenamento.setVisibility(View.VISIBLE);
                    onTrack = false;
                    backup_riposo_scheda=riposo_scheda;
                    backup_stimolazione_scheda=stimolazione_scheda;
                    backup_durata_scheda=durata_scheda;

                    aggiornaCountDown(Calendar.getInstance().getTime().getTime()+durata_scheda+1);
                    //TODO: IMPOSTO QUI FREQUENZA E FORMA D'ONDA, SONO CONVITNO BASTA FARLO UNA VOLTA SOLA
                    client.preStart(exercise);
                }
                else
                {
                    Toast.makeText(DemoActivityController.this, "Non hai inserito tutte le informazioni necessarie", Toast.LENGTH_SHORT).show();
                }

            }
            else
            {
                Toast.makeText(DemoActivityController.this, "l'allenamento è iniziato", Toast.LENGTH_SHORT).show();

            }

        }
        else
        {
            Toast.makeText(DemoActivityController.this, "Non sei ancora connesso alla tuta", Toast.LENGTH_SHORT).show();
        }
    }

    //SVUOTA IMPOSTAZIONI
    @Click(R.id.terzo)
    void click_annulla()
    {
        //todo: definire le condizioni in cui può o non essere cliccabile
        // todo: posso cliccarlo quando sono in pausa? In play no. A fine allenamento si
        if(flag_connessione==true )
        {
            if(onTrack==null || fine == 0)
            {
                box_video.setVisibility(View.GONE);
                box_allenamento.setVisibility(View.GONE);
                frequenza.setText("");
                onda.setText("");
                stimolazione.setText("");
                riposo.setText("");
                durata.setText("");
                onTrack=null;
                addominali_lat.setIntensity(0);
                petto.setIntensity(0);
                braccio_dx.setIntensity(0);
                lombari.setIntensity(0);
                quadricipite_dx.setIntensity(0);
                quadricipite_sx.setIntensity(0);
                glutei.setIntensity(0);
                spalle.setIntensity(0);
                braccio_sx.setIntensity(0);
                addominali.setIntensity(0);
                exercise.cleanMuscle();
            }
            else
            {
                Toast.makeText(DemoActivityController.this, "In fase di allenamento non puoi pulire i campi", Toast.LENGTH_SHORT).show();
            }
        }
        else
        {
                Toast.makeText(DemoActivityController.this, "Non sei ancora connesso alla tuta", Toast.LENGTH_SHORT).show();
        }



    }

    //IMPORTA ESERCIZIO
    @Click(R.id.quarto)
    void click_loadExercise()
    {
        if(flag_connessione==true)
        {
            if(fine == 0)
            {

                //todo: vedi differneza .start e .run


                if(scheda!=null && scheda.length>0 )
                {
                    final Dialog d = set_default_dialog(R.string.import_label);
                    Button b1 = d.findViewById(R.id.accetta);
                    final NumberPicker np = (NumberPicker) d.findViewById(R.id.picker);
                    np.setMaxValue(scheda.length-1);
                    np.setMinValue(0);
                    np.setDisplayedValues(scheda);
                    np.setWrapSelectorWheel(false);

                    b1.setOnClickListener(v -> {

                        Thread setExerciseT = new Thread(new Runnable() {
                            @Override
                            public void run() {
                                //PRENDI IL NOME ASSOCIATO AL NUMERO
                                //CERCA IL RECORD ASSOCIATO A QUEL NOME
                                ExerciseEntity exerciseEntity = db.ExerciseDao().findByName(scheda[np.getValue()]);
                                //POPOLA
                                exercise.frequency = exerciseEntity.frequency;
                                frequenza.setText((int) exercise.frequency +" Hz");

                                exercise.wave_form = exerciseEntity.wave_form;
                                if (exercise.wave_form==SINE_WAVE){
                                    onda.setText("Sinusoidale");
                                }
                                else if (exercise.wave_form==TRIANGLE_WAVE){
                                    onda.setText("Triangolare");
                                }
                                else if (exercise.wave_form==COSINE_WAVE){
                                    onda.setText("Quadra");
                                }
                                else if (exercise.wave_form==SAWTOOTH_WAVE){
                                    onda.setText("Seghettata");
                                }
                                else if (exercise.wave_form==EXPONENTIAL_WAVE){
                                    onda.setText("Esponenziale");
                                }


                                exercise.stimulation_sec=exerciseEntity.stimulation_sec;
                                if(exercise.stimulation_sec==1){
                                    stimolazione.setText(String.format(Locale.ITALIAN, "%d secondo", exercise.stimulation_sec));
                                }
                                else{
                                    stimolazione.setText(String.format(Locale.ITALIAN, "%d secondi", exercise.stimulation_sec));
                                }

                                exercise.rest_sec=exerciseEntity.rest_sec;
                                if(exercise.rest_sec==1){
                                    riposo.setText(String.format(Locale.ITALIAN, "%d secondo", exercise.rest_sec));
                                }
                                else{
                                    riposo.setText(String.format(Locale.ITALIAN, "%d secondi", exercise.rest_sec));
                                }


                                exercise.duration_min=exerciseEntity.duration_min;
                                if(exercise.duration_min==1){
                                    durata.setText(String.format(Locale.ITALIAN, "%d minuto", exercise.duration_min));
                                }
                                else{
                                    durata.setText(String.format(Locale.ITALIAN, "%d minuti", exercise.duration_min));
                                }

                                exercise.video = exerciseEntity.video;

                                video.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        video.setText(exercise.video);
                                    }
                                });


                                stimolazione_scheda=(int) TimeUnit.SECONDS.toMillis(exercise.stimulation_sec);
                                riposo_scheda=(int) TimeUnit.SECONDS.toMillis(exercise.rest_sec);
                                durata_scheda=( int )TimeUnit.MINUTES.toMillis(exercise.duration_min);

                            }
                        });
                        setExerciseT.start();

                        d.dismiss();
                    });

                    d.show();


                }
                else
                {
                    Toast.makeText(DemoActivityController.this, "Non ci sono esercizi da poter caricare", Toast.LENGTH_SHORT).show();
                }




            }
            else
            {
                Toast.makeText(DemoActivityController.this, "l'allenamento è iniziato", Toast.LENGTH_SHORT).show();

            }

        }
        else
        {
            Toast.makeText(DemoActivityController.this, "Non sei ancora connesso alla tuta", Toast.LENGTH_SHORT).show();

        }

    }




    //VIDEO
    @Click(R.id.quinto)
    void click_video()
    {
        if(flag_connessione==true) {
            if(fine==0)
                loadVideo();
            else
                Toast.makeText(DemoActivityController.this, "L'allenamento è in corso", Toast.LENGTH_SHORT).show();

        }
        else
        {
            Toast.makeText(DemoActivityController.this, "Non sei ancora connesso alla tuta", Toast.LENGTH_SHORT).show();
        }
    }

    //COLLEGATI ALLA TUTA
    @Click(R.id.sesto)
    void click_connect_bt()
    {
        //todo: definire le condizioni in cui può o non essere cliccabile, ad esempio se è già connesso farei in modo che non posso cliccarlo
        if(flag_connessione!=true)
        {
            if (client == null) {
                client = new BluetoothFitnessClient(this);
                client.setListener(this);
                client.addNames("mbody", "BALANX");
            }
            client.startScan();
            //todo: facciamo diventare start scan boolean per avere una risposta da dare all'utente?
            Toast.makeText(DemoActivityController.this, "Mi collego alla tuta", Toast.LENGTH_SHORT).show();

        }
        else
        {
            Toast.makeText(DemoActivityController.this, "Sei già connesso alla tuta", Toast.LENGTH_SHORT).show();

        }

    }

    //LISTENER COLLEGAMENTO TUTA
    @Override
    public void bleFlagChanged(boolean b) {

        connection_bt.setBackground(b ? getDrawable(R.drawable.ic_collegato_bt) : getDrawable(R.drawable.ic_collega_bt));
        //ho spostato questo sopra questo perchè altrimenti in alcune situazioni rimaneva true
        flag_connessione = b;
        if (b) {
            if(client != null) {
                client.getBattery();
                //todo: se ci disconnettiamo, la tuta continua a stimolare, quindi mi ricollego e chiude quello che stava facendo
                client.sendStopCommand();
            }
        } else {
            //todo: qui dentro servirà un new runnable per gestire questi eventi
            box_video.post(new Runnable() {
                @Override
                public void run() {
                    box_video.setVisibility(View.GONE);
                    box_allenamento.setVisibility(View.GONE);
                }
            });
            //todo:aggiungere anche la chiusra del timer, oprta onTrack a false e fine a 0


        }
        //flag_connessione = b;
    }


    //TASTO PLAY E PAUSA
    @Click(R.id.play)
    void click_play()
    {
        if(client != null && onTrack==false) {

            //faccio partire il countdown
            startTimer();
            //LA PARTE IMPORTANTE DEL METODO
            new Thread(()-> {
                try {
                    //QUESTO BOOLEANO SEGNA SE STIAMO STIMOLANDO O NO
                    onTrack =true;
                    play.setBackground(getDrawable(R.drawable.ic_tasto_stop));
                    Date inizio=Calendar.getInstance().getTime();
                    //QUESTO IF/ELSE GESTISCE LA RIPRESA DA UNA PAUSA
                    if(backup_riposo_scheda==riposo_scheda) {
                        prossima_stimolazione = inizio.getTime();
                        //todo: quel poccolissimo +1? Pareri^
                        prossimo_riposo = prossima_stimolazione + (stimolazione_scheda) + 1;
                    }
                    else
                    {
                        prossimo_riposo = inizio.getTime();
                    }

                    Log.d ("dati Luca", "durata scheda"+durata_scheda);

                    //definisco a che ora termina l'esercizio
                    fine = inizio.getTime()+durata_scheda+1;

                    Log.d ("dati Luca", "inizio scheda"+inizio.getTime());
                    Log.d ("dati Luca", "fine scheda" + fine);

                    stimolare=false;
                    riposare = false;


                    while(inizio.getTime() <fine && onTrack)
                    {

                        Date now=Calendar.getInstance().getTime();

                        while((prossima_stimolazione <= now.getTime() && now.getTime() <= prossima_stimolazione+(stimolazione_scheda) && onTrack) && backup_riposo_scheda==riposo_scheda)
                        {

                            if(stimolare==false)
                            {
                                video.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        statoAllenamento.setText("Stimolazione");
                                        //aggiornaCountDown(fine);
                                    }
                                });
                                Log.d("dati Luca","allena "+ now.getTime()+" (secondi) "+stimolazione_scheda);

                                stimolare = true;
                                riposare = false;
                                //todo: bisogna inviare i comandi
                                client.sendStartExercise(exercise);
                                //todo: pareri su quel +1? Secondo me va messo tutti a +1
                                prossimo_riposo=prossima_stimolazione+(stimolazione_scheda)+1;
                            }

                            now=Calendar.getInstance().getTime();
                            //todo: definire un metodo fineAllenamento()
                            if(now.getTime() >= fine){ video.setText("Finito"); break;}

                        }
                        //todo: ricontrolla sto if, sembra inutile
                        if(onTrack==true){stimolazione_scheda=backup_stimolazione_scheda;}


                        while(prossimo_riposo <= now.getTime() && now.getTime() <= prossimo_riposo+(riposo_scheda) && onTrack)
                        {

                            if(riposare==false)
                            {
                                video.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        statoAllenamento.setText("Riposo");
                                        //aggiornaCountDown(fine);
                                    }
                                });
                                Log.d("dati Luca","riposa "+ now.getTime()+" (secondi) "+riposo_scheda);

                                riposare = true;
                                stimolare = false;
                                client.sendStopCommand();
                                prossima_stimolazione=prossimo_riposo+(riposo_scheda)+1;
                            }

                            now=Calendar.getInstance().getTime();
                            if(now.getTime() >= fine){ statoAllenamento.setText("Finito"); break;}


                        }
                        //todo: questo if probabilmente non serve
                        if(onTrack==true){stimolazione_scheda=backup_stimolazione_scheda;riposo_scheda=backup_riposo_scheda;}
                        //aggiornaCountDown(fine);

                        inizio=Calendar.getInstance().getTime();

                    }
                    Log.d("dati Luca","FATTO ");
                    client.sendStopCommand();
                    //TODO:VERIFICA SE PAUSE TIMER QUI VA BENE, PRIMA STAVA DENTRO CLICK_PAUSA
                    pauseTimer();

                } catch (Exception e) {
                    Log.d("dati Luca", "Qualcosa è andato storto "+e);
                    e.printStackTrace();
                    pauseTimer();
                }
            }).start();

        }
        else{
            //todo: gestire la fine dell'allenamento prendendo spunto da quello che fai per stoppare
            pauseTimer();
            onTrack=false;

            box_video.setVisibility(View.GONE);
            box_allenamento.setVisibility(View.GONE);
            play.setBackground(getDrawable(R.drawable.ic_sei));
            if(exercise.video==null)
            {
                video.setText("Video");
            }
            else
            {

            }

            //todo:forse anche qui serve runnable
            statoAllenamento.setText("");

            fine=0;
            durata_scheda=backup_durata_scheda;
            stimolazione_scheda=backup_stimolazione_scheda;
            riposo_scheda=backup_riposo_scheda;

            Log.d("dati Luca","durata "+durata_scheda+" stimolazione "+stimolazione_scheda+" ripooso "+riposo_scheda );
            Log.d("dati Luca","backup_durata "+backup_durata_scheda+" backup_stimolazione "+backup_stimolazione_scheda+" backup_ripooso "+backup_riposo_scheda );


        }


    }

    @Click(R.id.pausa)
    void click_pausa()
    {
        if (onTrack==true){
            Toast.makeText(DemoActivityController.this,
                    "Pausa", Toast.LENGTH_SHORT).show();


            onTrack=false;
            play.setBackground(getDrawable(R.drawable.ic_sei));

            durata_scheda= (int) (fine - Calendar.getInstance().getTime().getTime());
            //todo:credo serva anche qui un runnable
            statoAllenamento.setText("Pausa");


            //SE STIAMO STIMOLANDO
            if(stimolare==true && riposare == false){
                stimolazione_scheda= (int)((prossima_stimolazione+(stimolazione_scheda)) - Calendar.getInstance().getTime().getTime());

                Log.d("dati Luca","Stimolazione da recuperare:" +stimolazione_scheda+" timestamp");
            }
            //SE STIAMO RIPOSANDO
            else if(riposare==true && stimolare==false){
                riposo_scheda= (int)((prossimo_riposo+(riposo_scheda)) - Calendar.getInstance().getTime().getTime());

                Log.d("dati Luca","Riposo da recuperare:" +riposo_scheda+" timestamp");
            }

        }
        else
        {
            Toast.makeText(DemoActivityController.this,
                    "Sei già in pausa", Toast.LENGTH_SHORT).show();

        }

    }



    private void aggiornaCountDown(long fine){



        Date now=Calendar.getInstance().getTime();

        long timestamp_residuo = (fine-now.getTime())%ora;;
        long countdown_minuti=0;
        long countdown_secondi=0;


        countdown_minuti =  timestamp_residuo/minuto;
        countdown_secondi =  timestamp_residuo % minuto;
        countdown_secondi =  countdown_secondi/secondo;

/*        if(countdown_minuti == 0)
        {
            if(countdown_secondi >9)
                countDown.setText(String.format("%02d s", countdown_secondi));
            else
                countDown.setText(String.format("%01d s", countdown_secondi));

        }
        else if(countdown_secondi == 0)
        {
            if(countdown_minuti >9)
                countDown.setText(String.format("%02d m", countdown_minuti));
            else
                countDown.setText(String.format("%01d m", countdown_minuti));

        }
        else{
            if(countdown_minuti >9 && countdown_secondi >9)
                countDown.setText(String.format("%02d m  %02d s", countdown_minuti, countdown_secondi));
            else if(countdown_minuti >9 && countdown_secondi <10)
                countDown.setText(String.format("%02d m  %01d s", countdown_minuti, countdown_secondi));
            else if(countdown_minuti <10 && countdown_secondi >9)
                countDown.setText(String.format("%01d m  %02d s", countdown_minuti, countdown_secondi));
            else
                countDown.setText(String.format("%01d m  %01d s", countdown_minuti, countdown_secondi));

        }*/

        countDown.setText(String.format("%02d:%02d", countdown_minuti, countdown_secondi));
        Log.d("dati Luca","Mancano :"+ countdown_minuti+" minuti "+countdown_secondi+" secondi");

    }



    public void startTimer()
    {
        CountDownTimer = new android.os.CountDownTimer(durata_scheda,secondo){
            @Override
            public void onTick(long millisUntilFinished) {
                String tempoRestante = String.format(Locale.ITALIAN, "%02d:%02d",
                            TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished),
                            TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) -
                                    TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished)));
                countDown.setText(tempoRestante);
            }
            @Override
            public void onFinish() {
                String tempoRestante = String.format(Locale.ITALIAN, "%02d:%02d",
                            TimeUnit.MILLISECONDS.toMinutes(0),
                            TimeUnit.MILLISECONDS.toSeconds(0) -
                                    TimeUnit.MINUTES.toSeconds(0));
                countDown.setText(tempoRestante);
            }
        }.start();
    }

    public void pauseTimer()
    {
        CountDownTimer.cancel();


    }


    //CONTROLLO PRIMA DI SALVARE IL FILE SE TUTTI I CAMPI SONO PIENI
    private boolean isFilled(Exercise exercise){
        boolean result = true;
        if(exercise.frequency == null){
            Toast.makeText(DemoActivityController.this, "Campo frequenza assente!", Toast.LENGTH_SHORT).show();
            result=false;}
        else if(exercise.wave_form == null){
            Toast.makeText(DemoActivityController.this, "Campo onda assente!", Toast.LENGTH_SHORT).show();
            result=false;}
        else if(exercise.stimulation_sec == null){
            Toast.makeText(DemoActivityController.this, "Campo stimolazione assente!", Toast.LENGTH_SHORT).show();
            result=false;}
        else if(exercise.rest_sec == null){
            Toast.makeText(DemoActivityController.this, "Campo riposo assente!", Toast.LENGTH_SHORT).show();
            result=false;}
        else if(exercise.duration_min == null){
            Toast.makeText(DemoActivityController.this, "Campo durata assente!", Toast.LENGTH_SHORT).show();
            result=false;}

        return result;
    }

    //Utilizzo questo metodo e l'override a onActivityResult per tirare fuori il nome del video
    private void loadVideo(){
        if(fine==0) {
            Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
            //SOPRA action_get_content?
            intent.addCategory(Intent.CATEGORY_OPENABLE);
            intent.setType("video/*");
            //startActivityForResult(intent, 104);
            try {
                startActivityForResult(Intent.createChooser(intent, "Scegli un video da caricare"), 104);
            } catch (Exception ex) {
                System.out.println("browseClick :" + ex);//android.content.ActivityNotFoundException ex
            }
        }
        else{
            Toast.makeText(DemoActivityController.this, "Puoi caricare un video solo se l'allenamento non è in corso", Toast.LENGTH_SHORT).show();
        }
    }

    //metodo per il path del video
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 104) {
            if (resultCode == RESULT_OK) {
                try {
                    Uri uri = data.getData();
                    String filename;
                    String mimeType = getContentResolver().getType(uri);
                    if (mimeType == null) {
                        String path = getPath(this, uri);
                        if (path == null) {
                            //filename = FilenameUtils.getName(uri.toString());
                            filename = uri.toString();
                            } else {
                                File file = new File(path);
                                filename = file.getName();
                            }
                        } else {
                            Uri returnUri = data.getData();
                            Cursor returnCursor = getContentResolver().query(returnUri, null, null, null, null);
                            int nameIndex = returnCursor.getColumnIndex(OpenableColumns.DISPLAY_NAME);
                            int sizeIndex = returnCursor.getColumnIndex(OpenableColumns.SIZE);
                            returnCursor.moveToFirst();
                            filename = returnCursor.getString(nameIndex);
                            String size = Long.toString(returnCursor.getLong(sizeIndex));
                        }
                        File fileSave = getExternalFilesDir(null);
                        String sourcePath = getExternalFilesDir(null).toString();
                        try {
                            copyFileStream(new File(sourcePath + "/" + filename), uri,this);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    exercise.video=filename;
                    video.setText(filename);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }
    private void copyFileStream(File dest, Uri uri, Context context)
            throws IOException {
        InputStream is = null;
        OutputStream os = null;
        try {
            is = context.getContentResolver().openInputStream(uri);
            os = new FileOutputStream(dest);
            byte[] buffer = new byte[1024];
            int length;

            while ((length = is.read(buffer)) > 0) {
                os.write(buffer, 0, length);

            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            is.close();
            os.close();
        }
    }

    //metodo per il path del video
    public static String getPath(Context context, Uri uri) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            // DocumentProvider
            if (DocumentsContract.isDocumentUri(context, uri)) {
                // ExternalStorageProvider
                if (isExternalStorageDocument(uri)) {
                    final String docId = DocumentsContract.getDocumentId(uri);
                    final String[] split = docId.split(":");
                    final String type = split[0];

                    if ("primary".equalsIgnoreCase(type)) {
                        return Environment.getExternalStorageDirectory() + "/" + split[1];
                    }
                    // TODO handle non-primary volumes
                }
                // DownloadsProvider
                else if (isDownloadsDocument(uri)) {
                    final String id = DocumentsContract.getDocumentId(uri);
                    final Uri contentUri = ContentUris.withAppendedId(Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));
                    return getDataColumn(context, contentUri, null, null);
                }
                // MediaProvider
                else if (isMediaDocument(uri)) {
                    final String docId = DocumentsContract.getDocumentId(uri);
                    final String[] split = docId.split(":");
                    final String type = split[0];
                    Uri contentUri = null;
                    if ("image".equals(type)) {
                        contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                    } else if ("video".equals(type)) {
                        contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                    } else if ("audio".equals(type)) {
                        contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                    }
                    final String selection = "_id=?";
                    final String[] selectionArgs = new String[]{split[1]};
                    return getDataColumn(context, contentUri, selection, selectionArgs);
                }
            }
        }
        // MediaStore (and general)
        else if ("content".equalsIgnoreCase(uri.getScheme())) {
            // Return the remote address
            if (isGooglePhotosUri(uri))
                return uri.getLastPathSegment();
            return getDataColumn(context, uri, null, null);
        }
        // File
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }
        return null;
    }

    //metodo per il path del video
    public static String getDataColumn(Context context, Uri uri, String selection, String[] selectionArgs) {
        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {column};
        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs, null);
            if (cursor != null && cursor.moveToFirst()) {
                final int index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(index);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }

    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is DownloadsProvider.
     */
    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is MediaProvider.
     */
    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is Google Photos.
     */
    public static boolean isGooglePhotosUri(Uri uri) {
        return "com.google.android.apps.photos.content".equals(uri.getAuthority());
    }


}
