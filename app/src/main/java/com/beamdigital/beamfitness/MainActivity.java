package com.beamdigital.beamfitness;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import java.util.ArrayList;

import eu.beamdigital.beamfitnesslibrary.BluetoothFitnessClient;
import eu.beamdigital.beamfitnesslibrary.Muscle;
import eu.beamdigital.beamfitnesslibrary.MyAdapter;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, BluetoothFitnessClient.ChangeBleFlagListener {
    private static final String TAG = MainActivity.class.getSimpleName();
    private Context mContext;
    private GridView grid_photo;
    private BaseAdapter mAdapter = null;
    private ArrayList<Muscle> mData = null;

    private   MyThread thread =MyThread.getInstance();
    private final int len_vero_arr=12; //onde evitare problemi usiamo una variabile che ricorda la vera lunghezza di arr
    boolean[] arr  = new boolean[22]; //da 12 a 17 per includere le onde a 21 per aggiungere i 5 programmi
    static  MainActivity Main;
    private ImageButton btn_start,btn_end,btn_increase,btn_decrease;
    private TextView tv_strength_num;
    private boolean start_flag=false;
    private boolean ble_flag=false;

    private BluetoothFitnessClient client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Main=this;

        //开启定位权限
        if(ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED){//未开启定位权限
            //开启定位权限,200是标识码
            ActivityCompat.requestPermissions(MainActivity.this,new String[]{Manifest.permission.ACCESS_FINE_LOCATION},200);
        }else{
            //startLocaion();//开始定位
            Toast.makeText(MainActivity.this,this.getString(R.string.openedlocation),Toast.LENGTH_LONG).show();
        }
        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.P) {
            Log.i("sdk_info", "sdk < 28 Q");
            if (ActivityCompat.checkSelfPermission(this,
                    Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                    || ActivityCompat.checkSelfPermission(this,
                    Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                String[] strings =
                        {Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION};
                ActivityCompat.requestPermissions(this, strings, 1);
            }
        } else {
            if (ActivityCompat.checkSelfPermission(this,
                    Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                    || ActivityCompat.checkSelfPermission(this,
                    Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED
                    || ActivityCompat.checkSelfPermission(this,
                    "android.permission.ACCESS_BACKGROUND_LOCATION") != PackageManager.PERMISSION_GRANTED) {
                String[] strings = {android.Manifest.permission.ACCESS_FINE_LOCATION,
                        android.Manifest.permission.ACCESS_COARSE_LOCATION,
                        "android.permission.ACCESS_BACKGROUND_LOCATION"};
                ActivityCompat.requestPermissions(this, strings, 2);
            }
        }

        grid_photo = (GridView) findViewById(R.id.grid_view);
        tv_strength_num =findViewById(R.id.tv_strength_num);
        btn_decrease=findViewById(R.id.btn_decrease);
        btn_increase=findViewById(R.id.btn_increase);
        btn_start=findViewById(R.id.btn_start);
        btn_end  =findViewById(R.id.btn_end);

        mData = new ArrayList<Muscle>();
        mData.add(new Muscle(R.drawable.muscle_select,this.getString(R.string.belly2)));
        mData.add(new Muscle(R.drawable.muscle_select,this.getString(R.string.chest)));
        mData.add(new Muscle(R.drawable.muscle_select,this.getString(R.string.triceps)));
        mData.add(new Muscle(R.drawable.muscle_select,this.getString(R.string.back)));
        mData.add(new Muscle(R.drawable.muscle_select,this.getString(R.string.backthigh)));
        mData.add(new Muscle(R.drawable.muscle_select,this.getString(R.string.thigh)));
        mData.add(new Muscle(R.drawable.muscle_select,this.getString(R.string.hip)));
        mData.add(new Muscle(R.drawable.muscle_select,this.getString(R.string.waist)));
        mData.add(new Muscle(R.drawable.test,this.getString(R.string.test)));
        mData.add(new Muscle(R.drawable.muscle_select,this.getString(R.string.biceps)));
        mData.add(new Muscle(R.drawable.muscle_select,this.getString(R.string.belly)));
        mData.add(new Muscle(R.drawable.selectall,this.getString(R.string.selectall)));
        //WaveForm
        mData.add(new Muscle(R.drawable.muscle_select,this.getString(R.string.sine_wave)));
        mData.add(new Muscle(R.drawable.muscle_select,this.getString(R.string.triangle_wave)));
        mData.add(new Muscle(R.drawable.muscle_select,this.getString(R.string.sawtooth_wave)));
        mData.add(new Muscle(R.drawable.muscle_select,this.getString(R.string.expo_wave)));
        mData.add(new Muscle(R.drawable.muscle_select,this.getString(R.string.sine_wave2)));
        //Programmi specifici
        //Sine wave 01 su muscolo 1
        mData.add(new Muscle(R.drawable.selectall,this.getString(R.string.P1)));
        //triangle wave su muscolo 2
        mData.add(new Muscle(R.drawable.selectall,this.getString(R.string.P2)));
        //Sawtool wave su muscolo 3
        mData.add(new Muscle(R.drawable.selectall,this.getString(R.string.P3)));
        //Exponential wave su muscolo 4
        mData.add(new Muscle(R.drawable.selectall,this.getString(R.string.P4)));
        //Sine wave04 su muscolo 5
        mData.add(new Muscle(R.drawable.selectall,this.getString(R.string.P5)));



        mAdapter = new MyAdapter<Muscle>(mData, R.layout.muscle_grid_icon) {
            @Override
            public void bindView(ViewHolder holder, Muscle obj) {
                holder.setImageResource(R.id.img_icon, obj.getiId());
                holder.setText(R.id.txt_icon, obj.getiName());
            }
        };
        grid_photo.setAdapter(mAdapter);
        grid_photo.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int i, long id) {
            Log.i("test",i+": è stato cliccato");
            switch(i){
                    case 0:
                        if(arr[0]==true){
                            arr[0]=false;
                            mData.get(i).setiId(R.drawable.muscle_select);
                        }else{
                            arr[0]=true;
                            mData.get(i).setiId(R.drawable.muscle_select_2);
                        }break;
                    case 1:
                        if(arr[1]==true){
                            arr[1]=false;
                            mData.get(i).setiId(R.drawable.muscle_select);
                        }else{
                            arr[1]=true;
                            mData.get(i).setiId(R.drawable.muscle_select_2);
                        }   break;
                    case 2:
                        if(arr[2]==true){
                            arr[2]=false;
                            mData.get(i).setiId(R.drawable.muscle_select);
                        }else{
                            arr[2]=true;
                            mData.get(i).setiId(R.drawable.muscle_select_2);
                        }break;
                    case 3:
                        if(arr[3]==true){
                            arr[3]=false;
                            mData.get(i).setiId(R.drawable.muscle_select);
                        }else{
                            arr[3]=true;
                            mData.get(i).setiId(R.drawable.muscle_select_2);
                        }break;
                    case 4:
                        if(arr[4]==true){
                            arr[4]=false;
                            mData.get(i).setiId(R.drawable.muscle_select);
                        }else{
                            arr[4]=true;
                            mData.get(i).setiId(R.drawable.muscle_select_2);
                        }break;
                    case 5:
                        if(arr[5]==true){
                            arr[5]=false;
                            mData.get(i).setiId(R.drawable.muscle_select);
                        }else{
                            arr[5]=true;
                            mData.get(i).setiId(R.drawable.muscle_select_2);
                        }break;
                    case 6:
                        if(arr[6]==true){
                            arr[6]=false;
                            mData.get(i).setiId(R.drawable.muscle_select);
                        }else{
                            arr[6]=true;
                            mData.get(i).setiId(R.drawable.muscle_select_2);
                        }break;
                    case 7:
                        if(arr[7]==true){
                            arr[7]=false;
                            mData.get(i).setiId(R.drawable.muscle_select);
                        }else{
                            arr[7]=true;
                            mData.get(i).setiId(R.drawable.muscle_select_2);
                        }break;
                    case 8:
                        if(arr[8]==true){
                            arr[8]=false;
                            mData.get(i).setiId(R.drawable.test);
                        }else{
                            arr[8]=true;
                            mData.get(i).setiId(R.drawable.test_1);
                        }break;
                    case 9:
                        if(arr[9]==true){
                            arr[9]=false;
                            mData.get(i).setiId(R.drawable.muscle_select);
                        }else{
                            arr[9]=true;
                            mData.get(i).setiId(R.drawable.muscle_select_2);
                        }break;
                    case 10:
                        if(arr[10]==true){
                            arr[10]=false;
                            mData.get(i).setiId(R.drawable.muscle_select);
                        }else{
                            arr[10]=true;
                            mData.get(i).setiId(R.drawable.muscle_select_2);
                        }break;
                    case 11:
                        if(arr[11]==true){
                            mData.get(0).setiId(R.drawable.muscle_select);
                            mData.get(1).setiId(R.drawable.muscle_select);
                            mData.get(2).setiId(R.drawable.muscle_select);
                            mData.get(3).setiId(R.drawable.muscle_select);
                            mData.get(4).setiId(R.drawable.muscle_select);
                            mData.get(5).setiId(R.drawable.muscle_select);
                            mData.get(6).setiId(R.drawable.muscle_select);
                            mData.get(7).setiId(R.drawable.muscle_select);
                            //mData.get(8).setiId(R.drawable.test);
                            mData.get(9).setiId(R.drawable.muscle_select);
                            mData.get(10).setiId(R.drawable.muscle_select);
                            mData.get(11).setiId(R.drawable.selectall);
                            for(int index=0;index<len_vero_arr;index++)
                                if(index==8){

                                }else{
                                    arr[index]=false;
                                }
                        }else{
                            mData.get(0).setiId(R.drawable.muscle_select_2);
                            mData.get(1).setiId(R.drawable.muscle_select_2);
                            mData.get(2).setiId(R.drawable.muscle_select_2);
                            mData.get(3).setiId(R.drawable.muscle_select_2);
                            mData.get(4).setiId(R.drawable.muscle_select_2);
                            mData.get(5).setiId(R.drawable.muscle_select_2);
                            mData.get(6).setiId(R.drawable.muscle_select_2);
                            mData.get(7).setiId(R.drawable.muscle_select_2);
                            //mData.get(8).setiId(R.drawable.test_1);
                            mData.get(9).setiId(R.drawable.muscle_select_2);
                            mData.get(10).setiId(R.drawable.muscle_select_2);
                            mData.get(11).setiId(R.drawable.selectall_1);
                            for(int index=0;index<len_vero_arr;index++)
                                if(index==8){

                                }else{
                                    arr[index]=true;
                                }
                        }break;
                        //CASE NUOVI AGGIUNTI DA NOI

                case 12://SET SINE WAVE01
                    if(arr[12]==false){
                        arr[12]=true;
                        //accendo quella appropriata
                        mData.get(i).setiId(R.drawable.muscle_select_2);
                        //spengo le altre
                        mData.get(13).setiId(R.drawable.muscle_select);
                        arr[13]=false;
                        mData.get(14).setiId(R.drawable.muscle_select);
                        arr[14]=false;
                        mData.get(15).setiId(R.drawable.muscle_select);
                        arr[15]=false;
                        mData.get(16).setiId(R.drawable.muscle_select);
                        arr[16]=false;
                        Message msgWave = MyHandler.getInstance().obtainMessage();//发送数据 invia i dati
                        msgWave.what    = 0x05; //sine wave 01
                        MyHandler.getInstance().sendMessage(msgWave);
                    }break;
                case 13: //SET TRIANGLE WAVE
                    if(arr[13]==false){
                        arr[13]=true;
                        //accendo quella appropriata
                        mData.get(i).setiId(R.drawable.muscle_select_2);
                        //spengo le altre
                        mData.get(12).setiId(R.drawable.muscle_select);
                        arr[12]=false;
                        mData.get(14).setiId(R.drawable.muscle_select);
                        arr[14]=false;
                        mData.get(15).setiId(R.drawable.muscle_select);
                        arr[15]=false;
                        mData.get(16).setiId(R.drawable.muscle_select);
                        arr[16]=false;
                        Message msgWave = MyHandler.getInstance().obtainMessage();//发送数据 invia i dati
                        msgWave.what    = 0x06; //triangle wave 02
                        MyHandler.getInstance().sendMessage(msgWave);
                    }break;
                case 14:// SET SAWTOOL WAVE
                    if(arr[14]==false){
                        arr[14]=true;
                        //accendo quella appropriata
                        mData.get(i).setiId(R.drawable.muscle_select_2);
                        //spengo le altre
                        mData.get(12).setiId(R.drawable.muscle_select);
                        arr[12]=false;
                        mData.get(13).setiId(R.drawable.muscle_select);
                        arr[13]=false;
                        mData.get(15).setiId(R.drawable.muscle_select);
                        arr[15]=false;
                        mData.get(16).setiId(R.drawable.muscle_select);
                        arr[16]=false;
                        Message msgWave = MyHandler.getInstance().obtainMessage();//发送数据 invia i dati
                        msgWave.what    = 0x07; //sawtool wave 08
                        MyHandler.getInstance().sendMessage(msgWave);
                    }break;
                case 15://SET EXPONENTIAL WAVE
                    if(arr[15]==false){
                        arr[15]=true;
                        //accendo quella appropriata
                        mData.get(i).setiId(R.drawable.muscle_select_2);
                        //spengo le altre
                        mData.get(12).setiId(R.drawable.muscle_select);
                        arr[12]=false;
                        mData.get(13).setiId(R.drawable.muscle_select);
                        arr[13]=false;
                        mData.get(14).setiId(R.drawable.muscle_select);
                        arr[14]=false;
                        mData.get(16).setiId(R.drawable.muscle_select);
                        arr[16]=false;
                        Message msgWave = MyHandler.getInstance().obtainMessage();//发送数据 invia i dati
                        msgWave.what    = 0x08; //expo wave 10
                        MyHandler.getInstance().sendMessage(msgWave);
                    }break;
                case 16://SET SINE WAVE 04
                    if(arr[16]==false){
                        arr[16]=true;
                        //accendo quella appropriata
                        mData.get(i).setiId(R.drawable.muscle_select_2);
                        //spengo le altre
                        mData.get(12).setiId(R.drawable.muscle_select);
                        arr[12]=false;
                        mData.get(13).setiId(R.drawable.muscle_select);
                        arr[13]=false;
                        mData.get(14).setiId(R.drawable.muscle_select);
                        arr[14]=false;
                        mData.get(15).setiId(R.drawable.muscle_select);
                        arr[15]=false;
                        Message msgWave = MyHandler.getInstance().obtainMessage();//发送数据 invia i dati
                        msgWave.what    = 0x09; //sine wave 04
                        MyHandler.getInstance().sendMessage(msgWave);
                    }break;

                //programma 1 onda sinwave01 e muscolo 1
                case 17:
                    Log.d(TAG, "onItemClick: p1 clicked");
                    if(arr[17]==false){
                        arr[17]=true;
                        //accendo quella appropriata
                        mData.get(i).setiId(R.drawable.selectall_1);
                        //ONDA 1
                        setSineWave01();
                        //MUSCOLO 0
                        arr[0]=true;
                        //AVVIO
                        if(client != null) {
                            client.sendStartCommand();
                        } else {
                            Log.d(TAG, "onItemClick: no client");
                            Toast.makeText(MainActivity.this, R.string.connectbluetooth, Toast.LENGTH_SHORT).show();
                        }
                    }
                    else{
                        arr[17]=false;
                        mData.get(i).setiId(R.drawable.selectall);
                        arr[0]=false;
                        sendStopCommand();
                    }
                    break;

                //programma 2 onda triangle wave e muscolo 2
                case 18:
                    if(arr[18]==false){
                        arr[18]=true;
                        //accendo quella appropriata
                        mData.get(i).setiId(R.drawable.selectall_1);
                        arr[2]=true;
                        Message msgPrg = MyHandler.getInstance().obtainMessage();//发送数据 invia i dati
                        msgPrg.what    = 0x11; //programma 2
                        msgPrg.arg1=thread.strength;
                        MyHandler.getInstance().sendMessage(msgPrg);

                    }
                    else{
                        arr[2]=false;
                        arr[18]=false;
                        mData.get(i).setiId(R.drawable.selectall);
                        if (client != null)
                            client.sendStopCommand();
                        else
                            Toast.makeText(MainActivity.this, R.string.connectbluetooth, Toast.LENGTH_SHORT).show();

                    }
                    break;
                //programma 3 onda sawtool wave e muscolo 3
                case 19:
                    if(arr[19]==false){
                        arr[19]=true;
                        //accendo quella appropriata
                        mData.get(i).setiId(R.drawable.selectall_1);
                        //ONDA 3
                        setSawtoothWave();
                        //MUSCOLO 3
                        byte[] buff=new byte[10];
                        buff[0]=(byte)0xA8;
                        buff[1]=(byte)3; //muscolo
                        buff[2]=(byte)0x01; //setting data
                        buff[3]=0x01; // questo va capito bene, lui fa delle casistiche che non mi sono chiarissime
                        buff[4]=(byte)0x32;//强度指令 istruzioni per forza (valore di intensità)
                        //questi prima della chiusura stanno sempre a zero
                        buff[5]=(byte)0x00; //motion scheme
                        buff[6]=(byte)0x00; // number of actions group
                        buff[7]=(byte)0x00; // the nth group action (l'ennesimo group action?)
                        buff[8]=(byte)0x00; //action repetitions times
                        buff[9]=(byte)0x8A; //chiusura
                        writeRXCharacteristic(buff);
                        //AVVIO
                        boolean test= sendStartCommand();
                        Log.i("Luca",test+"return di sendStartCommand");

                    }
                    else{
                        arr[19]=false;
                        mData.get(i).setiId(R.drawable.selectall);
                        sendStopCommand();
                    }
                    break;

                //programma 4 onda exponential wave e muscolo 4
                case 20:
                    if(arr[20]==false){
                        arr[20]=true;
                        //accendo quella appropriata
                        mData.get(i).setiId(R.drawable.selectall_1);
                        //ONDA 4
                        setExponentialWave();
                        //MUSCOLO 4
                        byte[] buff=new byte[10];
                        buff[0]=(byte)0xA8;
                        buff[1]=(byte)4; //muscolo
                        buff[2]=(byte)0x01; //setting data
                        buff[3]=0x01; // questo va capito bene, lui fa delle casistiche che non mi sono chiarissime
                        buff[4]=(byte)0x32;//强度指令 istruzioni per forza (valore di intensità)
                        //questi prima della chiusura stanno sempre a zero
                        buff[5]=(byte)0x00; //motion scheme
                        buff[6]=(byte)0x00; // number of actions group
                        buff[7]=(byte)0x00; // the nth group action (l'ennesimo group action?)
                        buff[8]=(byte)0x00; //action repetitions times
                        buff[9]=(byte)0x8A; //chiusura
                        writeRXCharacteristic(buff);
                        //AVVIO
                        sendStartCommand();

                    }
                    else{
                        arr[20]=false;
                        mData.get(i).setiId(R.drawable.selectall);
                        sendStopCommand();
                    }
                    break;

                //programma 5 onda sinwave04 e muscolo 5
                case 21:
                    if(arr[21]==false) {
                        arr[21] = true;
                        //accendo quella appropriata
                        mData.get(i).setiId(R.drawable.selectall_1);
                        //ONDA 5
                        setSineWave04();
                        //MUSCOLO 1
                        byte[] buff = new byte[10];
                        buff[0] = (byte) 0xA8;
                        buff[1] = (byte) 5; //muscolo
                        buff[2] = (byte) 0x01; //setting data
                        buff[3] = 0x01; // questo va capito bene, lui fa delle casistiche che non mi sono chiarissime
                        buff[4] = (byte) 0x32;//强度指令 istruzioni per forza (valore di intensità)
                        //questi prima della chiusura stanno sempre a zero
                        buff[5] = (byte) 0x00; //motion scheme
                        buff[6] = (byte) 0x00; // number of actions group
                        buff[7] = (byte) 0x00; // the nth group action (l'ennesimo group action?)
                        buff[8] = (byte) 0x00; //action repetitions times
                        buff[9] = (byte) 0x8A; //chiusura
                        writeRXCharacteristic(buff);
                        //AVVIO
                        sendStartCommand();
                    } else{
                        arr[21]=false;
                        mData.get(i).setiId(R.drawable.selectall);
                        sendStopCommand();
                    }
                    break;
                }
                mAdapter.notifyDataSetChanged();
            }
        });


        btn_decrease.setOnClickListener(this);
        btn_increase.setOnClickListener(this);
        btn_start.setOnClickListener(this);
        btn_end.setOnClickListener(this);
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode){
            case 200://刚才的识别码
                if(grantResults[0] == PackageManager.PERMISSION_GRANTED){//用户同意权限,执行我们的操作
                    //startLocaion();//开始定位
                }else{//用户拒绝之后,当然我们也可以弹出一个窗口,直接跳转到系统设置页面
                    Toast.makeText(MainActivity.this,this.getString(R.string.remindopenlocation),Toast.LENGTH_SHORT).show();
                }
                break;
            default:break;
        }
    }

    public  void findDrive(View v){
        if (client == null) {
            client = new BluetoothFitnessClient(this);
            client.setListener(this);
            client.addNames("mbody", "BALANX");
        }
        client.startScan();
    }

    public boolean writeRXCharacteristic(byte[] text) {
        new Thread(() -> {
            try {
                Thread.sleep(30);
                Log.d(TAG, "writeRXCharacteristic: ");
                if (client == null) {
                    Log.d(TAG, "writeRXCharacteristic: no client");
                } else {
                    client.writeRXCharacteristic(text);
                }
            } catch (InterruptedException e) {
                Log.d(TAG, "run: " + e.toString());
                e.printStackTrace();
            }

        }).start();

        return true;
    }

    public boolean sendStartCommand(){
        if (client == null) {
            return false;
        } else {
             client.sendStartCommand();
             return true;
        }
    }
    public boolean sendStopCommand(){
        //todo: visto che stoppiamo il comando al client succede qualche cosa in particolare?
        if (client == null) {
            return false;
        } else {
            client.sendStopCommand();
            return true;
        }
    }


    public boolean setSineWave01(){
        Log.d(TAG, "setSineWave01: ");
        if (client == null) {
            Log.d(TAG, "setSineWave01: no client");
            return false;
        } else {
            client.setSinWave01();
            return true;
        }
    }

    public boolean setTrinagleWave(){
        Log.d(TAG, "setTrinagleWave: ");
        if (client == null) {
            Log.d(TAG, "setTrinagleWave: no client");
            return false;
        } else {
            client.setTrinagleWave();
            return true;
        }
    }

    public boolean setSawtoothWave(){
        Log.d(TAG, "setSawtoothWave: ");
        if (client == null) {
            Log.d(TAG, "setSawtoothWave: no client");
            return false;
        } else {
            client.setSawtoolWave();
            return true;
        }
    }
    public boolean setExponentialWave(){
        Log.d(TAG, "setExponentialWave: ");
        if (client == null) {
            Log.d(TAG, "setExponentialWave: no client");
            return false;
        } else {
            client.setExponentialWave();
            return true;
        }
    }
    public boolean setSineWave04(){
        Log.d(TAG, "setSineWave04: ");
        if (client == null) {
            Log.d(TAG, "setSineWave04: no client");
            return false;
        } else {
            client.setSineWave04();
            return true;
        }
    }




    @Override
    public void bleFlagChanged(boolean b) {
        this.ble_flag = b;
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        client.closeBleGatt();
    }

    // TODO: 2/16/21 da uesto metodo vengono inviati i comandi
    @Override
    public void onClick(View view) {

        Message msg = MyHandler.getInstance().obtainMessage();//发送数据 invia i dati

        switch(view.getId()){
            case R.id.btn_start:{
                if(ble_flag==false){
                    Toast.makeText(MainActivity.this,"Please connect Bluetooth",Toast.LENGTH_SHORT).show();
                    break;
                }
                start_flag=!start_flag;
                if(start_flag){
                    if(thread.flag==false){
                        msg.what    = 0x01;
                        thread.flag = true;
                        thread.state= 0x01;
                        thread.step = 0x00;
                        (new Thread(thread)).start();
                    }
                    btn_start.setImageResource(R.drawable.pause);
                }else{
                    thread.state=0x03;
                    btn_start.setImageResource(R.drawable.start);
                }
            }break;
            case R.id.btn_end:{ // se deve chiudere la sessione di allenamento sposta lo state a 3, in mythread poi porterà alla fine dell'attività
                thread.state=0x03;
                start_flag  =false;
                btn_start.setImageResource(R.drawable.start);
            }break;
            case R.id.btn_decrease:{
                if(thread.strength>0){
                    thread.strength-=5;
                }else return;
                thread.state=0x01;
                thread.step =0x01;
                tv_strength_num.setText(thread.strength+"");
            }break;
            case R.id.btn_increase:{
                if(thread.strength>=100){ //non vorrei dire una cosa errata, ma da documentazione la forza arriva a 127
                    return ;
                }else{
                    thread.strength+=5;
                }
                thread.state = 0x01;
                thread.step  = 0x01;
                tv_strength_num.setText(thread.strength+"");
            }break;
        }
        if(thread!=null){
            thread.arr=arr;
            msg.obj=thread.arr;
        }

        MyHandler.getInstance().sendMessage(msg);
    }



}
