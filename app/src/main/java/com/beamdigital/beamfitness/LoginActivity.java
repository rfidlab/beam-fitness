package com.beamdigital.beamfitness;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class LoginActivity extends AppCompatActivity {

    private Button login;
    private Button sign_up;
    private EditText email;
    private EditText password;

    protected void onCreate (Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView (R.layout.login);
        login.findViewById(R.id.loginButton);
        sign_up.findViewById(R.id.registratiButton);
        email.findViewById(R.id.fillEmail);
        password.findViewById(R.id.fillPsw);
    }


    private void click_login(View view) {
        if(email.length()>0 && password.length()>0)
        {
            if(email.toString().contains("@")){
                //todo: passa alla main Activity
            }
            else
            {
                Toast.makeText(LoginActivity.this, "Devi inserire un indirizzo email", Toast.LENGTH_SHORT).show();
            }

        }
        else
        {
            Toast.makeText(LoginActivity.this, "Uno dei due campi è vuoto", Toast.LENGTH_SHORT).show();
        }

    }

    private void click_sign_up(View view) {
        //todo: gestisce la fase di registrazione
    }

    private void forgot_psw(View view) {
        //todo: apre un'activity dove mettere un indirizzo email

    }

    private void resetPassword(View view){
        //todo: vai ad una activity dove inserire il tuo indirizzo email
    }
}
