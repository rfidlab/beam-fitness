package com.beamdigital.beamfitness;

import android.Manifest;
import android.app.Dialog;
import android.content.pm.PackageManager;
import android.nfc.Tag;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import eu.beamdigital.beamfitnesslibrary.BluetoothFitnessClient;
import eu.beamdigital.beamfitnesslibrary.Command;

import static eu.beamdigital.beamfitnesslibrary.Constants.COSINE_WAVE;
import static eu.beamdigital.beamfitnesslibrary.Constants.EXPONENTIAL_WAVE;
import static eu.beamdigital.beamfitnesslibrary.Constants.SAWTOOTH_WAVE;
import static eu.beamdigital.beamfitnesslibrary.Constants.SINE_WAVE;
import static eu.beamdigital.beamfitnesslibrary.Constants.TRIANGLE_WAVE;

public class BeamActivity extends AppCompatActivity implements BluetoothFitnessClient.ChangeBleFlagListener {
    private static final String TAG = BeamActivity.class.getSimpleName();

    //colonna di sinistra
    private TextView nome;
    private TextView frequenza;
    private TextView forma;
    private TextView potenza;
    private TextView muscolo;
    private TextView stimolazione;
    private TextView riposo;
    private TextView durata;

    //barra pulsanti sotto
    private ImageButton salva;
    private ImageButton play;
    private ImageButton pulisci;
    private ImageButton importa;
    private ImageButton pausa;
    private ImageButton riprendi;


    //bt connection box, flag connessione passa a true quando sei effettivamente collegato alla tuta
    private ImageView stato_connessione;
    private boolean flag_connessione;
    private TextView batteria;

    //workout box
    private TextView nome_scheda;
    private TextView countdown_esercizio;
    private ConstraintLayout workout_box;
    private TextView barra_percent;

    private BluetoothFitnessClient client;

    //Utile a gestire allenamento, prossimamente tutti espressi in timestap
    private long durata_scheda;
    private int stimolazione_scheda;
    private int riposo_scheda;
    private int backup_stimolazione_scheda;
    private int backup_riposo_scheda;
    //USO QUESTO BOOLEANO PER FARE PLAY/PAUSA
    private Boolean onTrack=null;

    //gestione del tempo espressa in timestamp
    int secondo = 1000;
    int minuto = 60*secondo;
    int ora = 60*minuto;


    //gestione dell'allenamento
    long fine=0;
    boolean stimolare=false;
    boolean riposare = false;
    long prossima_stimolazione = 0;
    long prossimo_riposo=0;

    //orologio nuovo
    private CountDownTimer CountDownTimer;
    private boolean isCountdowning;
    private boolean isOnPause;








    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_beam);

        if(ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED){//未开启定位权限
            //开启定位权限,200是标识码
            ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.ACCESS_FINE_LOCATION},200);
        }else{
            //startLocaion();//开始定位
            Toast.makeText(this,this.getString(R.string.openedlocation),Toast.LENGTH_LONG).show();
        }
        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.P) {
            Log.i("sdk_info", "sdk < 28 Q");
            if (ActivityCompat.checkSelfPermission(this,
                    Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                    || ActivityCompat.checkSelfPermission(this,
                    Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                String[] strings =
                        {Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION};
                ActivityCompat.requestPermissions(this, strings, 1);
            }
        } else {
            if (ActivityCompat.checkSelfPermission(this,
                    Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                    || ActivityCompat.checkSelfPermission(this,
                    Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED
                    || ActivityCompat.checkSelfPermission(this,
                    "android.permission.ACCESS_BACKGROUND_LOCATION") != PackageManager.PERMISSION_GRANTED) {
                String[] strings = {android.Manifest.permission.ACCESS_FINE_LOCATION,
                        android.Manifest.permission.ACCESS_COARSE_LOCATION,
                        "android.permission.ACCESS_BACKGROUND_LOCATION"};
                ActivityCompat.requestPermissions(this, strings, 2);
            }
        }

        //colonna di sinistra
        nome = findViewById(R.id.allenamento_nome_campo);
        frequenza = findViewById(R.id.allenamento_frequenza_campo);
        forma = findViewById(R.id.allenamento_forma_campo);
        potenza = findViewById(R.id.allenamento_potenza_campo);
        muscolo = findViewById(R.id.allenamento_muscolo_campo);
        stimolazione = findViewById(R.id.allenamento_stimolazione_campo);
        riposo = findViewById(R.id.allenamento_riposo_campo);
        durata = findViewById(R.id.allenamento_durata_campo);
        barra_percent = findViewById(R.id.barra);

        //barra pulsanti sotto
        salva = findViewById(R.id.primo);
        play = findViewById(R.id.secondo);
        pulisci = findViewById(R.id.terzo);
        importa = findViewById(R.id.quarto);
        pausa = findViewById(R.id.quinto);
        riprendi = findViewById(R.id.sesto);

        pulisci.setOnLongClickListener(view -> {
            if(client != null) {
                Toast.makeText(BeamActivity.this, "Ferma tutti i muscoli", Toast.LENGTH_SHORT).show();
                client.stopAllMuscles();


            } else {
                Toast.makeText(BeamActivity.this, R.string.connectbluetooth, Toast.LENGTH_SHORT).show();
            }
            return true;
        });

        //pulsante collegamento bt
//        collega_bt = findViewById(R.id.connection_button);

        //bt connection box
        stato_connessione = findViewById(R.id.stato_connessione);
        flag_connessione=false;
        batteria = findViewById(R.id.percentage);

        //workout box
        nome_scheda = findViewById(R.id.nome);
        countdown_esercizio = findViewById(R.id.tempo_residuo);
        workout_box = findViewById(R.id.workout_box);


    }

    private final Command command = new Command();

    @Override
    protected void onResume() {
        super.onResume();
    }

    public void buttonClick(View v) {
        //primo
        if(v.getId()==salva.getId()){
            Toast.makeText(BeamActivity.this,
                    "Salvataggio scheda", Toast.LENGTH_SHORT).show();

        }
        //secondo
        else if(v.getId()==play.getId()) {
            Log.d("TAG", "buttonClick: " + command.toString());
            if(!flag_connessione){
                Toast.makeText(BeamActivity.this,
                        "Non sei ancora connesso", Toast.LENGTH_SHORT).show();
            }
            else{
                if(nome.length()>0 && frequenza.length()>0 && forma.length()>0 && potenza.length()>0 && muscolo.length()>0 &&
                        stimolazione.length()>0 && riposo.length()>0 && durata.length()>0 )
                {


                    if( onTrack==null || fine == 0 ) {
                        nome_scheda.setText(nome.getText().toString());
                        //countdown_esercizio.setText(durata.getText().toString());
                        workout_box.setVisibility(View.VISIBLE);
                        aggiornaCountDown(Calendar.getInstance().getTime().getTime()+durata_scheda+1);
                        //startTimer();
                        //pauseTimer();
                        //dico che non mi sto allenando
                        onTrack = false;
                        backup_riposo_scheda=riposo_scheda;
                        backup_stimolazione_scheda=stimolazione_scheda;
                    }
                    else{
                        Toast.makeText(BeamActivity.this,
                                "Scheda già selezionata ", Toast.LENGTH_SHORT).show();

                    }

                }
                else {
                    Toast.makeText(BeamActivity.this,
                            "Mancano dei campi nella scheda ", Toast.LENGTH_SHORT).show();
                }
            }
        }
        //terzo
        else if(v.getId()==pulisci.getId()) {
            Toast.makeText(BeamActivity.this,
                    "Pulizia campi", Toast.LENGTH_SHORT).show();

            nome.setText("");
            frequenza.setText("");
            forma.setText("");
            potenza.setText("");
            muscolo.setText("");
            stimolazione.setText("");
            riposo.setText("");
            durata.setText("");
            nome_scheda.setText("");
            countdown_esercizio.setText("");
            workout_box.setVisibility(View.GONE);
            //stoppo l'allenamento
            client.sendStopCommand();
            onTrack=null;
            fine=0;
        }
        //quarto
        else if(v.getId()==importa.getId()) {
            click_import();

        }
        //quinto
        else if(v.getId()==pausa.getId()) {

                if(client != null && onTrack!=null) {
                    //client.sendStopCommand();

                    if (onTrack==true){
                        Toast.makeText(BeamActivity.this,
                                "Pausa", Toast.LENGTH_SHORT).show();
                        CountDownTimer.cancel();

                        onTrack=false;

                        durata_scheda=  fine - Calendar.getInstance().getTime().getTime();
                        barra_percent.setText("Pausa");


                        //SE STIAMO STIMOLANDO
                        if(stimolare==true && riposare == false){
                            stimolazione_scheda= (int)((prossima_stimolazione+(stimolazione_scheda)) - Calendar.getInstance().getTime().getTime());

                            Log.d("Pause","Stimultation to complete: " +stimolazione_scheda);
                        }
                        //SE STIAMO RIPOSANDO
                        else if(riposare==true && stimolare==false){
                            riposo_scheda= (int)((prossimo_riposo+(riposo_scheda)) - Calendar.getInstance().getTime().getTime());

                            Log.d("Pause","Stimultation to complete:" +riposo_scheda);
                        }

                    }
                    else
                    {
                        Toast.makeText(BeamActivity.this,
                                "You are already in pause", Toast.LENGTH_SHORT).show();

                    }


                }
                else{
                    Toast.makeText(BeamActivity.this,
                            "Ancora non hai avviato una scheda o ancora devi scegliare la scheda", Toast.LENGTH_SHORT).show();
                }


        }
        //todo parte anche se non attivi il workouto box, sistemalo
        //sesto
        else if(v.getId()==riprendi.getId()) {

                if(client != null && onTrack!=null && onTrack!=true) {
                    Toast.makeText(BeamActivity.this,
                            "Start", Toast.LENGTH_SHORT).show();

                    startTimer();




                    new Thread(()-> {
                        try {


                            //QUESTO BOOLEANO SEGNA SE STIAMO STIMOLANDO O NO
                            onTrack =true;

                            Date inizio;
                            inizio=Calendar.getInstance().getTime();


                            if(backup_riposo_scheda==riposo_scheda) {
                                prossima_stimolazione = inizio.getTime();
                                //todo: quel poccolissimo +1? Pareri^
                                prossimo_riposo = prossima_stimolazione + (stimolazione_scheda) + 1;
                            }
                            else
                            {
                                prossimo_riposo = inizio.getTime();
                            }

                            Log.d (TAG, "workout duration"+durata_scheda);

                            fine = inizio.getTime()+durata_scheda;

                            Log.d (TAG, "start time"+inizio.getTime());
                            Log.d (TAG, "end time" + fine);

                             stimolare=false;
                             riposare = false;


                            //todo: metterlo in un thread
                            while(inizio.getTime() <fine && onTrack)
                            {
                                //todo: meglio usare cal2 o fare sempre Calendar.getInstance()?
                                Calendar cal2 = Calendar.getInstance();

                                Date now;
                                now=Calendar.getInstance().getTime();

                                //aggiornaCountDown(fine);


                                while((prossima_stimolazione <= now.getTime() && now.getTime() <= prossima_stimolazione+(stimolazione_scheda) && onTrack) && backup_riposo_scheda==riposo_scheda)
                                {
                                    barra_percent.setText("Stimolazione");
                                    if(stimolare==false)
                                    {
                                        Log.d(TAG,"working "+ now.getTime()+" (sec) "+stimolazione_scheda);

                                        stimolare = true;
                                        riposare = false;
                                        client.sendStartCommand(command);
                                        //todo: pareri su quel +1?
                                        prossimo_riposo=prossima_stimolazione+(stimolazione_scheda)+1;
                                    }

                                    now=Calendar.getInstance().getTime();
                                    if(now.getTime() >= fine){ barra_percent.setText("Finito"); break;}

                                }
                                if(onTrack==true){stimolazione_scheda=backup_stimolazione_scheda;}
                                //aggiornaCountDown(fine);

                                while(prossimo_riposo <= now.getTime() && now.getTime() <= prossimo_riposo+(riposo_scheda) && onTrack)
                                {
                                    barra_percent.setText("Riposo");
                                    if(riposare==false)
                                    {
                                        Log.d(TAG,"rest "+ now.getTime()+" (sec) "+riposo_scheda);

                                        riposare = true;
                                        stimolare = false;
                                        client.sendStopCommand();
                                        prossima_stimolazione=prossimo_riposo+(riposo_scheda)+1;
                                    }

                                    now=Calendar.getInstance().getTime();
                                    if(now.getTime() >= fine){ barra_percent.setText("Finito"); break;}


                                }
                                if(onTrack==true){stimolazione_scheda=backup_stimolazione_scheda;riposo_scheda=backup_riposo_scheda;}
                                //aggiornaCountDown(fine);

                                inizio=Calendar.getInstance().getTime();

                            }
                            Log.d(TAG,"Done ");

                            client.sendStopCommand();



                        } catch (Exception e) {
                            Log.d(TAG, "Something went wrong");
                        }
                    }).start();

                }
                else{
                    Toast.makeText(BeamActivity.this,
                            "Ancora non hai caricato inserito una scheda o l'hai già avviata", Toast.LENGTH_SHORT).show();
                }


        }
    }

    private Dialog set_default_dialog(int res_id_label) {
        Log.d("TAG", "set_default_dialog: " + getString(res_id_label));
        final Dialog d = new Dialog(BeamActivity.this);
        d.setContentView(R.layout.prova_picker);

        TextView label = d.findViewById(R.id.picker_label);
        label.setText(res_id_label);
        Button b2 = d.findViewById(R.id.annulla);
        b2.setOnClickListener(v -> d.dismiss());

        return d;
    }

    public void click_nome(View view) {
        final Dialog d = new Dialog(BeamActivity.this);
        d.setContentView(R.layout.name_workout);
        Button b1 = (Button) d.findViewById(R.id.si);
        Button b2 = (Button) d.findViewById(R.id.no);

        //final NumberPicker np = (NumberPicker) d.findViewById(R.id.numberPicker1);
        final EditText nw = (EditText) d.findViewById(R.id.allenamento);




        b1.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                nome.setText(nw.getText().toString());
                d.dismiss();
            }
        });
        b2.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                d.dismiss();
            }
        });

        d.show();
    }

    public void click_frequenza(View view) {
        final Dialog d = set_default_dialog(R.string.frequency_label);
        Button b1 = d.findViewById(R.id.accetta);
        final NumberPicker np = d.findViewById(R.id.picker);
        np.setMaxValue(100);
        np.setMinValue(1);
        np.setWrapSelectorWheel(false);

        b1.setOnClickListener(v -> {
            frequenza.setText(String.format(Locale.ITALIAN, "%d Hz", np.getValue()));
            command.frequency = (byte) np.getValue();
            d.dismiss();
        });

        d.show();
    }

    public void click_potenza(View view) {
        final Dialog d = set_default_dialog(R.string.strength_label);
        Button b1 = d.findViewById(R.id.accetta);
        final NumberPicker np = d.findViewById(R.id.picker);
        np.setMaxValue(100);
        np.setMinValue(0);
        np.setWrapSelectorWheel(false);

        b1.setOnClickListener(v -> {
            potenza.setText(String.valueOf(np.getValue()));
            command.strength = (byte) np.getValue();
            d.dismiss();
        });

        d.show();
    }


    public void click_stimolazione(View view) {
        final Dialog d = set_default_dialog(R.string.time_label);
        Button b1 = d.findViewById(R.id.accetta);
        final NumberPicker np = d.findViewById(R.id.picker);
        np.setMaxValue(10);
        np.setMinValue(0);
        np.setWrapSelectorWheel(false);

        b1.setOnClickListener(v -> {
            stimolazione.setText(String.format(Locale.ITALIAN, "%d secondi", np.getValue()));
            stimolazione_scheda=np.getValue()*secondo;
            d.dismiss();
        });

        d.show();
    }


    public void click_riposo(View view) {

        final Dialog d = set_default_dialog(R.string.rest_label);
        Button b1 = d.findViewById(R.id.accetta);
        final NumberPicker np = d.findViewById(R.id.picker);
        np.setMaxValue(60);
        np.setMinValue(0);
        np.setWrapSelectorWheel(false);

        b1.setOnClickListener(v -> {
            riposo.setText(String.format(Locale.ITALIAN, "%d secondi", np.getValue()));
            riposo_scheda=np.getValue()*secondo;
            d.dismiss();
        });

        d.show();
    }

    public void click_durata(View view) {

        final Dialog d = set_default_dialog(R.string.program_label);
        Button b1 = d.findViewById(R.id.accetta);
        final NumberPicker np = d.findViewById(R.id.picker);
        np.setMaxValue(60);
        np.setMinValue(1);
        np.setWrapSelectorWheel(false);

        b1.setOnClickListener(v -> {
            durata.setText(String.format(Locale.ITALIAN, "%d minuti", np.getValue()));
            durata_scheda=np.getValue()*minuto;
            d.dismiss();
        });

        d.show();
    }

    public void click_muscolo(View view) {

        final Dialog d = set_default_dialog(R.string.muscle_label);
        Button b1 = d.findViewById(R.id.accetta);
        final NumberPicker np = d.findViewById(R.id.picker);
        np.setMaxValue(Const.muscle.length-1);
        np.setMinValue(0);
        np.setDisplayedValues(Const.muscle);
        np.setWrapSelectorWheel(false);

        b1.setOnClickListener(v -> {
            muscolo.setText(Const.muscle[np.getValue()]);
            command.muscle = (byte) Const.muscle_library_id[np.getValue()];
            d.dismiss();
        });

        d.show();
    }

    public void click_wave(View view) {
        final String[] wave = new String[]{ "Sine", "Triangle", "Cosine", "Sawtooth", "Exponential" };
        final Dialog d = set_default_dialog(R.string.wave_form_label);
        Button b1 = d.findViewById(R.id.accetta);
        final NumberPicker np = d.findViewById(R.id.picker);
        np.setMaxValue(wave.length-1);
        np.setMinValue(0);
        np.setDisplayedValues(wave);
        np.setWrapSelectorWheel(false);

        b1.setOnClickListener(v -> {
            forma.setText(wave[np.getValue()]);
            if (np.getValue()==0){
                command.wave_form = SINE_WAVE;
            }
            else if (np.getValue()==1){
                command.wave_form = TRIANGLE_WAVE;
            }
            else if (np.getValue()==2){
                command.wave_form = COSINE_WAVE;
            }
            else if (np.getValue()==3){
                command.wave_form = SAWTOOTH_WAVE;
            }
            else if (np.getValue()==4){
                command.wave_form = EXPONENTIAL_WAVE;
            }
            d.dismiss();
        });

        d.show();
    }


    private void click_import() {

        final String[] scheda = { "Lunedi", "Mercoledì", "Venerdì",};
        final Dialog d = set_default_dialog(R.string.import_label);
        Button b1 = d.findViewById(R.id.accetta);
        final NumberPicker np = (NumberPicker) d.findViewById(R.id.picker);
        np.setMaxValue(scheda.length-1);
        np.setMinValue(0);
        np.setDisplayedValues(scheda);
        np.setWrapSelectorWheel(false);

        b1.setOnClickListener(v -> {
            nome.setText(scheda[np.getValue()]);
            nome_scheda.setText(scheda[np.getValue()]);
            if (np.getValue()==0){
                command.frequency = 20;
                command.wave_form = SINE_WAVE;
                command.strength = 50;
                command.muscle = (byte) 8;
                /// muscle

                frequenza.setText("20 Hz");
                forma.setText("Sine");
                potenza.setText("50");
                muscolo.setText("Spalle");
                stimolazione.setText("9 secondi");
                riposo.setText("25 secondi");
                durata.setText("15 minuti");
                countdown_esercizio.setText("15 minuti");

                stimolazione_scheda=9*secondo;
                riposo_scheda=25*secondo;
                durata_scheda=15*minuto;
                
            }
            else if (np.getValue()==1){
                command.frequency = 20;
                command.wave_form = TRIANGLE_WAVE;
                command.strength = 20;
                command.muscle = (byte) 2;
                /// muscle

                frequenza.setText("50 Hz");
                forma.setText("Triangle");
                potenza.setText("20");
                muscolo.setText("Petto");
                stimolazione.setText("4 secondi");
                riposo.setText("15 secondi");
                durata.setText("5 minuti");
                countdown_esercizio.setText("5 minuti");

                stimolazione_scheda=4*secondo;
                riposo_scheda=15*secondo;
                durata_scheda=5*minuto;

            }
            else if (np.getValue()==2){
                command.frequency = 80;
                command.wave_form = SAWTOOTH_WAVE;
                command.strength = 35;
                command.muscle = (byte) 4;
                /// muscle

                frequenza.setText("80 Hz");
                forma.setText("Sawtooth");
                potenza.setText("35");
                muscolo.setText("Lombari");
                stimolazione.setText("2 secondi");
                riposo.setText("6 secondi");
                durata.setText("9 minuti");
                countdown_esercizio.setText("9 minuti");

                stimolazione_scheda=2*secondo;
                riposo_scheda=6*secondo;
                durata_scheda=9*minuto;

            }

            d.dismiss();
        });

        d.show();
    }

    //METODO CHE SCATTA QUANDO PREMI IL TASTO PER COLLEGARTI ALLA TUTA
    public void findDrive(View view) {
        if (client == null) {
            client = new BluetoothFitnessClient(this);
            client.setListener(this);
            client.addNames("mbody", "BALANX");
        }
        client.startScan();
    }

    @Override
    public void bleFlagChanged(boolean b) {

        stato_connessione.setImageResource(b ? R.drawable.ic_connesso : R.drawable.ic_non_connesso);
        if (b) {
            if(client != null) {
                client.getBattery();
            }
        } else {
            batteria.setText("");
            workout_box.setVisibility(View.GONE);
        }
        flag_connessione = b;
    }


    private void aggiornaCountDown(long fine){

        Date now=Calendar.getInstance().getTime();

        long timestamp_residuo = (fine-now.getTime())%ora;;
        long countdown_minuti=0;
        long countdown_secondi=0;


        countdown_minuti =  timestamp_residuo/minuto;
        countdown_secondi =  timestamp_residuo % minuto;
        countdown_secondi =  countdown_secondi/secondo;
        countdown_esercizio.setText(String.format("%02d m %02d s", countdown_minuti, countdown_secondi));

        Log.d(TAG,"Countdown :"+ countdown_minuti+" min "+countdown_secondi+" sec");
    }





    public void startTimer()
    {
        if(isCountdowning == false)
        {
            CountDownTimer = new CountDownTimer(durata_scheda,secondo){

                @Override
                public void onTick(long millisUntilFinished) {
                    String tempoRestante = String.format(Locale.ITALIAN, "%02d : %02d",
                            TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished),
                            TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) -
                                    TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished)));
                    countdown_esercizio.setText(tempoRestante);

                }

                @Override
                public void onFinish() {
                    String tempoRestante = String.format(Locale.ITALIAN, "%02d : %02d",
                            TimeUnit.MILLISECONDS.toMinutes(0),
                            TimeUnit.MILLISECONDS.toSeconds(0) -
                                    TimeUnit.MINUTES.toSeconds(0));
                    countdown_esercizio.setText(tempoRestante);

                }

            }.start();
        }

    }

    public void pauseTimer()
    {
        CountDownTimer.cancel();


    }


}