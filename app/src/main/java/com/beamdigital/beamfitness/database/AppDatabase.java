package com.beamdigital.beamfitness.database;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

import com.beamdigital.beamfitness.model.ExerciseEntity;
import com.beamdigital.beamfitness.model.LSTypeConverter;
import com.beamdigital.beamfitness.model.Plan;

@Database(entities = {ExerciseEntity.class, Plan.class}, version = 1)
@TypeConverters(LSTypeConverter.class)
public abstract class AppDatabase extends RoomDatabase {
    private static AppDatabase INSTANCE;

    public abstract ExerciseDao ExerciseDao();
    public abstract PlanDao getPlanDao();

    public static AppDatabase getInMemoryDatabase(Context context) {
        if (INSTANCE == null) {
            INSTANCE = Room.inMemoryDatabaseBuilder(context.getApplicationContext(), AppDatabase.class)
                    .allowMainThreadQueries()
                    .build();
        }
        return INSTANCE;
    }

    /// @Luca per ottenere un'istanza del db devi usare questo metodo:
    /// AppDatabase.getDatabase(context)
    public static AppDatabase getDatabase(Context context) {
        if (INSTANCE == null) {
            INSTANCE = Room.databaseBuilder(context.getApplicationContext(), AppDatabase.class, "database-exercise")
                    .allowMainThreadQueries()
                    .build();
        }
        return INSTANCE;
    }

    public static void destroyInstance() {
        INSTANCE = null;
    }

}