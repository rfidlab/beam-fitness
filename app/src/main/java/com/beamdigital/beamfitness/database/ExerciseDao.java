package com.beamdigital.beamfitness.database;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.beamdigital.beamfitness.model.ExerciseEntity;
import com.beamdigital.beamfitness.model.Plan;

import java.util.List;

@Dao
public interface ExerciseDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(ExerciseEntity exercise);

    @Update
     void update(ExerciseEntity exercise);

    @Delete
    void delete(ExerciseEntity exercise);

    @Query("SELECT name FROM ExerciseEntity")
    String[] loadNameExercises();

    @Query("SELECT * FROM ExerciseEntity WHERE name LIKE :first ")
    ExerciseEntity findByName(String first);

    @Query("SELECT id_exercise FROM ExerciseEntity WHERE name LIKE :first ")
    int findPKByName(String first);

    @Query("SELECT * FROM ExerciseEntity")
    LiveData<List<ExerciseEntity>> findAll();

    @Query("SELECT * FROM ExerciseEntity ")
            ExerciseEntity[] getAll();

    @Query("SELECT * FROM ExerciseEntity WHERE id_exercise= :id ")
    ExerciseEntity getID(int id);


/*    @Query("SELECT * FROM ExerciseEntity WHERE id_exercise=:id")
    JsonElement getMusclesFromID( int id);*/




    //todo: manca l'update o @insert oppure un insert che in caso di conflitto deve aggiornare
}
