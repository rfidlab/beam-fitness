package com.beamdigital.beamfitness.database;

import android.content.Context;
import android.util.Log;

import com.beamdigital.beamfitness.R;
import com.beamdigital.beamfitness.model.ExerciseEntity;
import com.beamdigital.beamfitness.model.Plan;

import java.util.ArrayList;

public class PlanManager {

    private static final String TAG = "PlanManager";
    private Plan plan;

    public void createNewPlan() {
        Log.d(TAG, "createNewPlan: ");
        plan = null;
        plan = new Plan();
        Log.d(TAG, "createNewPlan: " + plan);
    }
    public void setPlan(Plan plan)
    {
        this.plan=plan;
    }
    public Plan getPlan(){return plan;}

    public void destroyPlan(){
        Log.d(TAG, "destroyPlan: ");
        plan = null;
    }

    public void savePlan(Context context) {
        /// planDao.saveToDb(plan)
        if(plan != null) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    if(plan.getId_plan()!=0)
                    {
                        AppDatabase.getDatabase(context).getPlanDao().update(plan);
                        destroyPlan();
                    }
                    else
                    {
                        AppDatabase.getDatabase(context).getPlanDao().insert(plan);
                        destroyPlan();
                    }

                }
            }).start();
        }
    }

    public void validatePlan(Context context) {
        if(plan == null)
            throw new IllegalStateException(context.getString(R.string.no_plan));
        if(duration() == 0)
            throw new IllegalStateException(context.getString(R.string.no_exercises));
        if(plan.getName() == null || plan.getName().length() == 0)
            throw new IllegalStateException(context.getString(R.string.no_name));
        if(getPreTraining().size() == 0)
            throw new IllegalStateException(context.getString(R.string.no_pre_training));
        if(getTraining().size() == 0)
            throw new IllegalStateException(context.getString(R.string.no_training));
        if(getPostTraining().size() == 0)
            throw new IllegalStateException(context.getString(R.string.no_post_training));
    }

    public long duration() {
        if(plan == null)
            return 0;
        return plan.getDuration();
    }

    public void addPreTrainingExercise(ExerciseEntity e) {
        ArrayList<ExerciseEntity> pretraining = plan.getPretraining();
        if(pretraining == null)
            pretraining = new ArrayList<>(0);

        pretraining.add(e);
        plan.setPretraining(pretraining);
        debug("addPreTrainingExercise");
    }

    public void addPostTrainingExercise(ExerciseEntity e) {
        ArrayList<ExerciseEntity> posttraining = plan.getPosttraining();
        if(posttraining == null)
            posttraining = new ArrayList<>(0);

        posttraining.add(e);
        plan.setPosttraining(posttraining);
        debug("addPostTrainingExercise");
    }

    public void addTrainingExercise(ExerciseEntity e) {
        ArrayList<ExerciseEntity> training = plan.getTraining();
        if(training == null)
            training = new ArrayList<>(0);

        training.add(e);
        plan.setTraining(training);
        debug("addTrainingExercise");
    }

    public void removePreTrainingExercise(ExerciseEntity e) {
        ArrayList<ExerciseEntity> pretraining = plan.getPretraining();
        if(pretraining == null)
            pretraining = new ArrayList<>(0);
        ArrayList<ExerciseEntity> newPt = new ArrayList<>(0);
        for(ExerciseEntity e1 : pretraining){
            if(e1.getId_exercise() != e.getId_exercise())
                newPt.add(e1);
        }
        plan.setPretraining(newPt);
        debug("removePreTrainingExercise");
    }

    public void removePostTrainingExercise(ExerciseEntity e) {
        ArrayList<ExerciseEntity> postTraining = plan.getPosttraining();
        if(postTraining == null)
            postTraining = new ArrayList<>(0);

        ArrayList<ExerciseEntity> newPt = new ArrayList<>(0);
        for(ExerciseEntity e1 : postTraining){
            if(e1.getId_exercise() != e.getId_exercise())
                newPt.add(e1);
        }
        plan.setPosttraining(newPt);
        debug("removePostTrainingExercise");
    }

    public void removeTrainingExercise(ExerciseEntity e) {
        ArrayList<ExerciseEntity> training = plan.getTraining();
        if(training == null)
            training = new ArrayList<>(0);

        ArrayList<ExerciseEntity> newPt = new ArrayList<>(0);
        for(ExerciseEntity e1 : training){
            if(e1.getId_exercise() != e.getId_exercise())
                newPt.add(e1);
        }
        plan.setTraining(newPt);
        debug("removeTrainingExercise");
    }

    public void setName(String name) {
        if(plan != null)
            plan.setName(name);
    }

    public enum TrainingPhase {
        Training,
        PreTraining,
        PostTraining
    }

    public boolean hasExercise(TrainingPhase type, ExerciseEntity e) {
        if(plan == null)
            return false; /// dovrebbe lanciare un'eccezione

        switch (type) {
            case Training:
                return hasExercise(plan.getTraining(), e);
            case PostTraining:
                return hasExercise(plan.getPosttraining(), e);
            case PreTraining:
                return hasExercise(plan.getPretraining(), e);
            default:
                return false;
        }
    }

    private boolean hasExercise(ArrayList<ExerciseEntity> list, ExerciseEntity e) {
        if(list == null)
            return false;

        for(ExerciseEntity e1 : list){
            if(e1.getId_exercise() == e.getId_exercise())
                return true;
        }

        return false;
    }

    public void debug(String tag){
        Log.d(TAG, "debug: " + tag + ": " + plan);
    }

    public ArrayList<ExerciseEntity> getPreTraining(){
        if(plan == null || plan.getPretraining() == null)
            return new ArrayList<>(0);

        return plan.getPretraining();
    }

    public ArrayList<ExerciseEntity> getTraining() {
        if(plan == null || plan.getTraining() == null)
            return new ArrayList<>(0);

        return plan.getTraining();
    }

    public ArrayList<ExerciseEntity> getPostTraining() {
        if(plan == null || plan.getPosttraining() == null)
            return new ArrayList<>(0);

        return plan.getPosttraining();
    }

    public void setURLCover(String cover)
    {
        plan.setCover(cover);
    }
}
