package com.beamdigital.beamfitness.database;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.beamdigital.beamfitness.model.Plan;

import java.util.List;

@Dao
public interface PlanDao {

    @Insert
    void insert(Plan exercise);

    @Update
     void update(Plan exercise);

    @Delete
    void delete(Plan exercise);

    @Query("SELECT * FROM Plan")
    LiveData<List<Plan>> findAll();

    @Query("SELECT * FROM Plan WHERE id_plan= :id ")
    Plan getID(int id);



    //todo: manca l'update o @insert oppure un insert che in caso di conflitto deve aggiornare
}
