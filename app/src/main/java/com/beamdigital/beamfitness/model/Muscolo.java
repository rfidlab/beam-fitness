package com.beamdigital.beamfitness.model;

import com.google.gson.Gson;

import eu.beamdigital.beamfitnesslibrary.InfoMuscolo;

// todo: luca InfoMuscolo non va bene da solo per memorizzare dei dati
// ControllerMuscoloDX e SX gestiscono la grafica, ma non sono concettualmente adatti per mantenere
// i dati
public class Muscolo implements InfoMuscolo {

    private int code;
    private boolean active;
    private int intensity;
    private String name;

    public Muscolo(String name, int code, boolean active, int intensity) {
        this.code = code;
        this.active = active;
        this.intensity = intensity;
        this.name = name;
    }

    public Muscolo(int code, boolean active, int intensity) {
        this.code = code;
        this.active = active;
        this.intensity = intensity;
    }

    @Override
    public byte getTheCode() {
        return (byte) code;
    }

    @Override
    public byte getTheIntesity() {
        return (byte) intensity;
    }

    @Override
    public boolean getIsActive() {
        return active;
    }

    @Override
    public void setTheCode(int code) {
        this.code = code;
    }

    @Override
    public void setIsActive(boolean active) {
        this.active = active;
    }

    @Override
    public void setIntensity(int val) {
        this.intensity = val;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}
