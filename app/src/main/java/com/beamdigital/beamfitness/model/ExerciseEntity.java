package com.beamdigital.beamfitness.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.google.gson.Gson;

import java.util.ArrayList;

@Entity
public class ExerciseEntity {

    //todo vogliamo fare una chiave primavaria int che fa autogenerate? (autoGenerate = true)

    @PrimaryKey(autoGenerate = true)
    public int  id_exercise; //ho messo quesso in char perche deve essere nonNullable

    @ColumnInfo(name = "name")
    public String name;

    @ColumnInfo(name = "frequency")
    public byte frequency=-1;

    @ColumnInfo(name = "wave_form")
    public byte wave_form=-1;

    @ColumnInfo(name = "stimulation_sec")
    public int stimulation_sec=-1;

    @ColumnInfo(name = "rest_sec")
    public int rest_sec=-1;

    @ColumnInfo(name = "duration_mi")
    public int duration_min=-1;

    @ColumnInfo(name = "video")
    public String video;

    @ColumnInfo(name = "muscle_list")
    public ArrayList<Muscolo> muscle_list;

    //todo: inserire l'eventuale video? Mettere il nome


    public int getId_exercise() {
        return id_exercise;
    }

    public void setId_exercise(int id_exercise) {
        this.id_exercise = id_exercise;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public byte getFrequency() {
        return frequency;
    }

    public void setFrequency(byte frequency) {
        this.frequency = frequency;
    }

    public byte getWave_form() {
        return wave_form;
    }

    public void setWave_form(byte wave_form) {
        this.wave_form = wave_form;
    }

    public int getStimulation_sec() {
        return stimulation_sec;
    }

    public void setStimulation_sec(int stimulation_sec) {
        this.stimulation_sec = stimulation_sec;
    }

    public int getRest_sec() {
        return rest_sec;
    }

    public void setRest_sec(int rest_sec) {
        this.rest_sec = rest_sec;
    }

    public int getDuration_min() {
        return duration_min;
    }

    public void setDuration_min(int duration_min) {
        this.duration_min = duration_min;
    }

    public String getVideo() {
        return video;
    }

    public void setVideo(String video) {
        this.video = video;
    }

    public ArrayList<Muscolo> getMuscle_list() {
        return muscle_list;
    }

    public void setMuscle_list(ArrayList<Muscolo> muscle_list) {
        this.muscle_list = muscle_list;
    }

    private static final String TAG = "ExerciseEntity";

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }

    public ExerciseEntity() {
    }

    public ExerciseEntity(int id_exercise, String name, byte frequency, byte wave_form, int stimulation_sec, int rest_sec, int duration_min, String video, ArrayList<Muscolo> muscle_list) {
        this.id_exercise = id_exercise;
        this.name = name;
        this.frequency = frequency;
        this.wave_form = wave_form;
        this.stimulation_sec = stimulation_sec;
        this.rest_sec = rest_sec;
        this.duration_min = duration_min;
        this.video = video;
        this.muscle_list = muscle_list;
    }
}
