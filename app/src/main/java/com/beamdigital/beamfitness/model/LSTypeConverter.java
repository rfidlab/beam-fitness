package com.beamdigital.beamfitness.model;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;

public class LSTypeConverter {

    @androidx.room.TypeConverter
    public static ArrayList<Muscolo> fromStringToMuscles(String value){
        Type listType = new TypeToken<ArrayList<Muscolo>>() {}.getType();
        return  new Gson().fromJson(value, listType);
    }

    @androidx.room.TypeConverter
    public static String fromMusclesToString(ArrayList<Muscolo> value) {
        return new Gson().toJson(value);
    }

    @androidx.room.TypeConverter
    public static ArrayList<ExerciseEntity> fromStringToExercises(String value){
        Type listType = new TypeToken<ArrayList<ExerciseEntity>>() {}.getType();
        return  new Gson().fromJson(value, listType);
    }

    @androidx.room.TypeConverter
    public static String fromExercisesToString(ArrayList<ExerciseEntity> value) {
        return new Gson().toJson(value);
    }
}
