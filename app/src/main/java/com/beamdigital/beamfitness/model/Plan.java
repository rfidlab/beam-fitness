package com.beamdigital.beamfitness.model;

import android.util.Log;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.google.gson.Gson;

import java.util.ArrayList;

@Entity
public class Plan {

    @PrimaryKey(autoGenerate = true)
    public int id_plan; //ho messo quesso in char perche deve essere nonNullable

    @ColumnInfo(name = "name")
    private String name;

    @ColumnInfo(name = "cover")
    private String cover;

    @ColumnInfo(name = "pre_training")
    private ArrayList<ExerciseEntity> pretraining = new ArrayList<>(0);

    @ColumnInfo(name = "training")
    private ArrayList<ExerciseEntity> training;

    @ColumnInfo(name = "post_training")
    private ArrayList<ExerciseEntity> posttraining;


    public int getId_plan() {
        return id_plan;
    }

    public void setId_plan(int id_plan) {
        this.id_plan = id_plan;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<ExerciseEntity> getPretraining() {
        return pretraining;
    }

    public void setPretraining(ArrayList<ExerciseEntity> pretraining) {
        this.pretraining = pretraining;
    }

    public ArrayList<ExerciseEntity> getTraining() {
        return training;
    }

    public void setTraining(ArrayList<ExerciseEntity> training) {
        this.training = training;
    }

    public ArrayList<ExerciseEntity> getPosttraining() {
        return posttraining;
    }

    public void setPosttraining(ArrayList<ExerciseEntity> posttraining) {
        this.posttraining = posttraining;
    }

    public String getCover() {
        return this.cover;
    }


    public void setCover(String copertina) {
        this.cover = copertina;
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }

    public long getDuration() {
        long sum = 0;

        if(pretraining != null) {
            for(ExerciseEntity e : pretraining)
                sum += e.duration_min;
        }

        if(training != null) {
            for(ExerciseEntity e : training)
                sum += e.duration_min;

        }

        if(posttraining != null) {
            for(ExerciseEntity e : posttraining)
                sum += e.duration_min;

        }

        return sum;
    }

    public void updatePreTraining(int i, ExerciseEntity ex){
        if(i<pretraining.size()){
            pretraining.set(i, ex);
        }

    }
    public void updateTraining(int i, ExerciseEntity ex){
        if(i<training.size()){
            training.set(i, ex);
        }

    }
    public void updatePostTraining(int i, ExerciseEntity ex){
        if(i<posttraining.size()){
            posttraining.set(i, ex);
        }

    }

    public void removePreTraining(int i){
        if(i<pretraining.size()){
            pretraining.remove(i);
        }
    }
    public void removeTraining(int i){
        if(i<training.size()){
            training.remove(i);
        }
    }
    public void removePostTraining(int i){
        if(i<posttraining.size()){
            posttraining.remove(i);
        }
    }


    public Plan removeExercise(ExerciseEntity ex)
    {
        boolean done=false;
        //preTraining
        for (int i =0; i<pretraining.size();i++)
        {
            if(pretraining.get(i).id_exercise==ex.id_exercise){
                removePreTraining(i);
                done=true;
            }
        }

        //Training
        for (int i =0; i<training.size();i++)
        {
            if(training.get(i).id_exercise==ex.id_exercise){
                removeTraining(i);
                done=true;

            }
        }

        //postTraining
        for (int i =0; i<posttraining.size();i++)
        {
            if(posttraining.get(i).id_exercise==ex.id_exercise){
                removePostTraining(i);
                done=true;

            }
        }
        if(done)
        {
            return this;
        }
        return null;
    }



    public Plan() {
    }

    public Plan(int id_plan, String name, String cover, ArrayList<ExerciseEntity> pretraining, ArrayList<ExerciseEntity> training, ArrayList<ExerciseEntity> posttraining) {
        this.id_plan = id_plan;
        this.name = name;
        this.cover=cover;
        this.pretraining = pretraining;
        this.training = training;
        this.posttraining = posttraining;
    }
}
