package com.beamdigital.beamfitness;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


public class RecyclerAdapterFilters extends RecyclerView.Adapter<RecyclerAdapterFilters.FilterViewHolder> {

    String TAG = "RecyclerAdapterFilters";
    Context context;

    private boolean[] tag = new boolean[5];

    public RecyclerAdapterFilters(Context context) {
        this.context = context;
    }

    @NonNull
    @Override
    public RecyclerAdapterFilters.FilterViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater= LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.recycler_view_filters,parent,false);
        RecyclerAdapterFilters.FilterViewHolder viewHolder = new RecyclerAdapterFilters.FilterViewHolder(view);
        viewHolder = new RecyclerAdapterFilters.FilterViewHolder(view);

        switch (viewType){
            case 0: {
                viewHolder.filterImage.setImageDrawable(context.getDrawable(R.drawable.ic_tag_forza_notsel));
                break;
            }
            case 1:{
                viewHolder.filterImage.setImageDrawable(context.getDrawable(R.drawable.ic_tag_relax_notsel));
                break;
            }
            case 2:{
                viewHolder.filterImage.setImageDrawable(context.getDrawable(R.drawable.ic_tag_cardio_notsel));
                break;
/*                view = layoutInflater.inflate(R.layout.carousel_two,parent,false);
                viewHolder = new RecyclerAdapterFilters.CarouselViewHolder(view);
                BFCManager bfcManager = ViewCentrale.get().getBfcManager();
                viewHolder.connect.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        bfcManager.connect(context);

                    }
                });

                //applico l'idea di Simonetta
                viewHolder.itemView.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        bfcManager.connect(context);
                    }
                });
                break;*/
            }
            case 3:{
                viewHolder.filterImage.setImageDrawable(context.getDrawable(R.drawable.ic_tag_snellente_notsel));
                break;
            }
            case 4:{
                viewHolder.filterImage.setImageDrawable(context.getDrawable(R.drawable.ic_tag_daily_notsel));
                break;
            }

        }

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerAdapterFilters.FilterViewHolder holder, int position) {





        switch (position){
            case 0:{
                if(holder.filterImage != null) {
                    holder.filterImage.setOnClickListener(view -> {
                        if(tag[position])
                        {
                            holder.filterImage.setImageDrawable(context.getDrawable(R.drawable.ic_tag_forza_notsel));
                            tag[position]=false;
                        }
                        else {
                            holder.filterImage.setImageDrawable(context.getDrawable(R.drawable.ic_tag_forza_sel));
                            tag[position] = true;
                        }

                    });
                }
                break;
            }
            case 1:{
                if(holder.filterImage != null) {
                    holder.filterImage.setOnClickListener(view -> {
                        if(tag[position])
                        {
                            holder.filterImage.setImageDrawable(context.getDrawable(R.drawable.ic_tag_relax_notsel));
                            tag[position]=false;
                        }
                        else {
                            holder.filterImage.setImageDrawable(context.getDrawable(R.drawable.ic_tag_relax_sel));
                            tag[position] = true;
                        }
                    });
                }
                break;
            }
            case 2:{
                if(holder.filterImage != null) {
                    holder.filterImage.setOnClickListener(view -> {
                        if(tag[position])
                        {
                            holder.filterImage.setImageDrawable(context.getDrawable(R.drawable.ic_tag_cardio_notsel));
                            tag[position]=false;
                        }
                        else {
                            holder.filterImage.setImageDrawable(context.getDrawable(R.drawable.ic_tag_cardio_sel));
                            tag[position] = true;
                        }
                    });
                }
                break;
            }
            case 3:{
                if(holder.filterImage != null) {
                    holder.filterImage.setOnClickListener(view -> {
                        if(tag[position])
                        {
                            holder.filterImage.setImageDrawable(context.getDrawable(R.drawable.ic_tag_snellente_notsel));
                            tag[position]=false;
                        }
                        else {
                            holder.filterImage.setImageDrawable(context.getDrawable(R.drawable.ic_tag_snellente_sel));
                            tag[position] = true;
                        }
                    });
                }
                break;
            }
            case 4:{
                if(holder.filterImage != null) {
                    holder.filterImage.setOnClickListener(view -> {
                        if(tag[position])
                        {
                            holder.filterImage.setImageDrawable(context.getDrawable(R.drawable.ic_tag_daily_notsel));
                            tag[position]=false;
                        }
                        else {
                            holder.filterImage.setImageDrawable(context.getDrawable(R.drawable.ic_tag_daily_sel));
                            tag[position] = true;
                        }
                    });
                }
                break;
            }
            case 5:{
                break;
            }
        }



    }

    @Override
    public int getItemCount() {
        return 5;
    }




    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public class FilterViewHolder extends RecyclerView.ViewHolder{

    ImageView filterImage;

        public FilterViewHolder(@NonNull View itemView) {

            super(itemView);
            filterImage = itemView.findViewById(R.id.filterImage);



        }
    }
}
