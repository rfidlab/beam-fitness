package com.beamdigital.beamfitness;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.beamdigital.beamfitness.realtime.RTDBManager;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.appbar.AppBarLayout;
import com.beamdigital.beamfitness.fragment.HomeFragment;
import com.beamdigital.beamfitness.fragment.PrivateFragment;
import com.beamdigital.beamfitness.fragment.WalletFragment;
import com.beamdigital.beamfitness.model.Plan;
import com.google.android.material.badge.BadgeDrawable;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;

import eu.beamdigital.beamfitnesslibrary.BluetoothFitnessClient;

public class ViewCentrale extends AppCompatActivity implements BFCManager.BfcInterface, RecyclerAdapterVertical.StartPlanOnClickListener {

    private static final String TAG = ViewCentrale.class.getSimpleName();
    boolean demo=false;
    RecyclerView carosello;

    RecyclerAdapterHorizontal recyclerAdapterCarousel;

    String[] pezziCarosello = {"0","1","2"};

    //TOPBAR
    ImageView avatar;

    //bottombar
    Button buttonbar1;
    Button buttonbar2;
    Button buttonbar3;
    Button buttonbar4;
    ImageView fab;
    ImageView settings;


    AppBarLayout appBar;
    NestedScrollView scrollView;

    int height = 520;

    boolean blueButtonFlag = false;
    ConstraintLayout trainerBox;
    ImageView addedToTrainerBox;


    //GESTIONE CONNESSIONE
    //connessione alla libreria
    BluetoothFitnessClient client;
    boolean flag_connessione;
    TextView connectionState;

    private BFCManager bfcManager = new BFCManager();


    void home() {
        FragmentManager supportFragmentManager = getSupportFragmentManager();
        supportFragmentManager.beginTransaction()
                /// R.id.grid_view deve diventare il container che ti ho indicato nel layout
                .replace(R.id.fragment, new HomeFragment())
                .commit();


    }

    void esplora() {
        HomeFragment esplora = new HomeFragment();
        esplora.setMessaggio("EsploraFragment");
        FragmentManager supportFragmentManager = getSupportFragmentManager();
        supportFragmentManager.beginTransaction()
                /// R.id.grid_view deve diventare il container che ti ho indicato nel layout
                .replace(R.id.fragment, esplora)
                .commit();
    }

    void privateFragment() {
        FragmentManager supportFragmentManager = getSupportFragmentManager();
        supportFragmentManager.beginTransaction()
                /// R.id.grid_view deve diventare il container che ti ho indicato nel layout
                .replace(R.id.fragment, new PrivateFragment())
                .commit();
    }

    void wallet() {
        FragmentManager supportFragmentManager = getSupportFragmentManager();
        supportFragmentManager.beginTransaction()
                /// R.id.grid_view deve diventare il container che ti ho indicato nel layout
                .replace(R.id.fragment, new WalletFragment())
                .commit();
    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.view_centrale);



        //bfcManager
        bfcManager.setBfcInterface(this);

        //topBar
        avatar= findViewById(R.id.avatar);

        //BadgeDrawable badgeDrawable = BadgeDrawable.createFromResource(this,avatar.getId());
        



        //bottombar
        buttonbar1= findViewById(R.id.bar1);
        buttonbar2= findViewById(R.id.bar2);
        buttonbar3= findViewById(R.id.bar3);
        buttonbar4 = findViewById(R.id.bar4);
        buttonbar1.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor("#1b98e0")));
        fab = findViewById(R.id.imageView7);
        settings = findViewById(R.id.settings);




        instance=this;

        //bBox
        trainerBox = findViewById(R.id.boxBlueButton);
        addedToTrainerBox = findViewById(R.id.boxBlueButtonII);

        //texview per lo stato della connessione
        connectionState = findViewById(R.id.connection_status);

        //getSupportActionBar().hide();
        carosello = findViewById(R.id.carousel);
        appBar = findViewById(R.id.appBar);
        scrollView = findViewById(R.id.scrollView);


        carosello.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));
        recyclerAdapterCarousel = new RecyclerAdapterHorizontal(this, pezziCarosello);
        carosello.setAdapter(recyclerAdapterCarousel);

        height = carosello.getLayoutParams().height;
        Log.d("Luca", "" + height);

        appBar.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                ViewGroup.LayoutParams layoutParams = carosello.getLayoutParams();
                int range = -appBarLayout.getTotalScrollRange();

                Log.d(TAG, "onOffsetChanged: getTotalScrollRange: " + appBarLayout.getTotalScrollRange());
                if (verticalOffset == 0) {
                    layoutParams.height = height;
                    if (recyclerAdapterCarousel != null)
                        recyclerAdapterCarousel.showImage(layoutParams.height);
                } /*else if (verticalOffset < 0 && verticalOffset >= (range / 5)) {
                    layoutParams.height = height * 4 / 5;
                    if (recyclerAdapterCarousel != null)
                        recyclerAdapterCarousel.showImage(layoutParams.height);
                }*/ else {
                    layoutParams.height = height / 2;
                    if (recyclerAdapterCarousel != null)
                        recyclerAdapterCarousel.hideImage(layoutParams.height);
                }
                carosello.setLayoutParams(layoutParams);

                if (appBarLayout.isLifted()) {
                    Log.d(TAG, "onOffsetChanged: lifted: " + verticalOffset + "; " + layoutParams.height);
                } else {
                    Log.d(TAG, "onOffsetChanged: NOT lifted: " + verticalOffset + "; " + layoutParams.height);
                }
            }
        });
/*
        scrollView.setOnScrollChangeListener(new View.OnScrollChangeListener() {
            @Override
            public void onScrollChange(View v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                Log.d(TAG, "onScrollChange: " + scrollX + "; " + oldScrollX + "; " + (oldScrollX - scrollX) + "; ");
            }
        });*/

        //permessi
        if(ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED){//未开启定位权限
            //开启定位权限,200是标识码
            ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.ACCESS_FINE_LOCATION},200);
        }else{
            //startLocaion();//开始定位
            Toast.makeText(this,this.getString(R.string.openedlocation),Toast.LENGTH_LONG).show();
        }
        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.P) {
            Log.i("sdk_info", "sdk < 28 Q");
            if (ActivityCompat.checkSelfPermission(this,
                    Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                    || ActivityCompat.checkSelfPermission(this,
                    Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                String[] strings =
                        {Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION};
                ActivityCompat.requestPermissions(this, strings, 1);
            }
        } else {
            if (ActivityCompat.checkSelfPermission(this,
                    Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                    || ActivityCompat.checkSelfPermission(this,
                    Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED
                    || ActivityCompat.checkSelfPermission(this,
                    "android.permission.ACCESS_BACKGROUND_LOCATION") != PackageManager.PERMISSION_GRANTED) {
                String[] strings = {android.Manifest.permission.ACCESS_FINE_LOCATION,
                        android.Manifest.permission.ACCESS_COARSE_LOCATION,
                        "android.permission.ACCESS_BACKGROUND_LOCATION"};
                ActivityCompat.requestPermissions(this, strings, 2);
            }
        }

        home();

    }

    public void creaEsercizio(View view) {
        //todo: la chiudo l'activity che lascio?
        startActivity(new Intent(this, ViewCreaEsercizio.class));
        mostraScompai(view.getRootView());
        return;

    }
    public void creaScheda(View view) {
        startActivity(new Intent(this, ViewCreaScheda.class));
        mostraScompai(view.getRootView());
        return;
    }

    private static ViewCentrale instance;

    public static ViewCentrale get() {
        return instance;
    }
    public BFCManager getBfcManager() {
        return this.bfcManager;
    }



    @Override
    public void startPlanOnClick(Plan p) {

        if(demo)
        {
            Intent intent = new Intent(this, ViewAllenamento.class);
            intent.putExtra("plan", p.id_plan);
            intent.putExtra("demo", demo);
            startActivity(intent);
        }
        else{
            //LANCIO L'ALLENAMENTO NON IN MODALITà DEMO
            if(bfcManager.flag_connessione) {
                Intent intent = new Intent(this, ViewAllenamento.class);
                intent.putExtra("plan", p.id_plan);
                intent.putExtra("demo", demo);
                startActivity(intent);

            }
            else
            {
                Toast.makeText(this, "Non sei ancora connesso alla tuta ", Toast.LENGTH_SHORT).show();

            }
        }



        return;
    }

    @Override
    public void onConnectionStateEvent(boolean state) {
        Log.d("Luca", "onConnectionStateEvent: " + state);

        ((Activity) this).runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Log.d("Luca", "runOnUiThread: " + state);

                recyclerAdapterCarousel.notifyDataSetChanged();
            }
        });
    }

    public void mostraScompai(View view) {
        //controlla lo stato
        if(blueButtonFlag==false)
        {
            //mostra
            blueButtonFlag=true;
            trainerBox.setVisibility(View.VISIBLE);
            addedToTrainerBox.setVisibility(View.VISIBLE);
            //cambio tasto crea
            fab.setImageDrawable(getDrawable(R.drawable.ic_tasto_crea_selezionato));


        }
        else
        {
            //nascondi
            blueButtonFlag=false;
            trainerBox.setVisibility(View.GONE);
            addedToTrainerBox.setVisibility(View.GONE);
            //cambio tasto crea
            fab.setImageDrawable(getDrawable(R.drawable.ic_tasto_crea));




        }

    }



    @SuppressLint("ResourceType")
    public void onClick_home(View view) {

        //parte di animazione buttom
        buttonbar1.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor("#1b98e0")));
        buttonbar2.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor("#969D9B")));
        buttonbar3.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor("#969D9B")));
        buttonbar4.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor("#969D9B")));

        home();
    }

    public void onClick_esplora(View view) {

        //parte di animazione buttom
        buttonbar1.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor("#969D9B")));
        buttonbar2.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor("#1b98e0")));
        buttonbar3.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor("#969D9B")));
        buttonbar4.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor("#969D9B")));

        esplora();
    }

    public void onClick_private(View view) {

        //parte di animazione buttom
        buttonbar1.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor("#969D9B")));
        buttonbar2.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor("#969D9B")));
        buttonbar3.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor("#1b98e0")));
        buttonbar4.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor("#969D9B")));
        privateFragment();
    }

    public void onClick_wallet(View view) {

        //parte di animazione buttom
        buttonbar1.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor("#969D9B")));
        buttonbar2.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor("#969D9B")));
        buttonbar3.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor("#969D9B")));
        buttonbar4.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor("#1b98e0")));
        wallet();
    }


    public void click_settings(View view) throws Exception {
        demo = demo ? !demo:!demo;
        if(demo)
        {
            settings.setImageDrawable(getDrawable(R.drawable.ic_settings_demo));
            Toast.makeText(this, "Modalità DEMO attivata", Toast.LENGTH_SHORT).show();
        }
        else
        {
            settings.setImageDrawable(getDrawable(R.drawable.ic_settings));
            Toast.makeText(this, "Modalità DEMO disattivata", Toast.LENGTH_SHORT).show();
        }
    }
}
