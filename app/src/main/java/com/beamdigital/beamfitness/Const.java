package com.beamdigital.beamfitness;

public class Const {
    public final static String[] muscle = { "Addominali Laterali", "Petto", "Braccio Sinistro", "Lombari", "Quadricipite Sinistro","Quadricipite Destro", "Glutei", "Spalle", "Braccio Destro", "Addominali" };
    public final static int[] muscle_library_id = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };


    public static int fromMuscleToCode(String m) {
        if(m != null) {
            for (int i = 0; i < muscle.length; i++) {
                if (m.equals(muscle[i]))
                    return muscle_library_id[i];
            }
        }

        return -1;
    }
}
