package com.beamdigital.beamfitness;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class ViewStarting extends AppCompatActivity {

    TextView proceedButton;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.view_starting);
        proceedButton=findViewById(R.id.proceedButton);
    }


    public void proceed(View view) {
        //passo alla prossima activity
        startActivity(new Intent(this, ViewRegistration.class));
        finish();
    }
}
