package com.beamdigital.beamfitness;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.gson.Gson;
import com.beamdigital.beamfitness.model.ExerciseEntity;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

public class ViewSetupMuscles extends AppCompatActivity {

    //progress bar
    TextView progressBartext2;
    ProgressBar progressBar2;
    ImageView workoutVideo;

    //muscle list è un intero esercizio, ma a me importa solo la lista dei muscoli
    ExerciseEntity exercise;


    RecyclerView setupMuscleList;
    RecyclerAdapterSetupMuscles recyclerAdapterSetupMuscles;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.view_setup_muscles_2);
        getWindow(). addFlags (WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);



        //scarico la lista dei muscoli partendo dal to string che mi sono inviato
        //con il metodo new Gson().fromJson(stringa, ExerciseEntity.class);
        //https://stackoverflow.com/questions/10407159/how-to-manage-startactivityforresult-on-android
        Bundle bundle = getIntent().getExtras();
        exercise = new Gson().fromJson(   bundle.getString("muscleList")  , ExerciseEntity.class);


        //preparo la recycler view
        setupMuscleList = findViewById(R.id.setupMuscleList);
        setupMuscleList.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
        recyclerAdapterSetupMuscles = new RecyclerAdapterSetupMuscles(this, exercise);
        setupMuscleList.setAdapter(recyclerAdapterSetupMuscles);

        //preparo la progress bar
        progressBar2 = findViewById(R.id.progressBar2);
        progressBartext2 = findViewById(R.id.progressBartext2);

        //todo: sta roba non va bene, non mi fa vedere i secondi


        String tempoRestante =  String.format("%02d:%02d",
                TimeUnit.MILLISECONDS.toMinutes(bundle.getLong("time")),
                TimeUnit.MILLISECONDS.toSeconds(bundle.getLong("time")) -
                        TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(bundle.getLong("time")))
        );
        progressBartext2.setText(tempoRestante);
        progressBar2.setProgress(bundle.getInt("progress"));




        ViewAllenamento.indexMuscle = new ArrayList<Integer>();
        workoutVideo = findViewById(R.id.workoutVideo);

        //INIZIO PEZZO CHE CARICA GIF
        StorageReference storageReference = FirebaseStorage.getInstance().getReference();
        StorageReference ref = storageReference.child("gif/"+bundle.getString("gif"));

        ref.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                Glide.with(getApplicationContext()).asGif().load(uri).into(workoutVideo);
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Glide.with(getApplicationContext()).asGif().load("https://media.giphy.com/media/nqCH5dsIn99cmSPVrd/giphy.gif").into(workoutVideo);

            }
        });


    }

    @Override
    public void onBackPressed() {
        /// do nothing
    }



    public void click_abort(View view) {
        //todo: serve avere questo intent se tanto non consideriamo il RESULT_CANCELED?
        Intent returnIntent = new Intent();
        setResult(ViewSetupMuscles.RESULT_CANCELED,returnIntent);
        finish();
    }

    public void saveSetupMuscles(View view) {
        Intent returnIntent = new Intent();
        //rimando all'activity precende
        returnIntent.putExtra("result", exercise.toString());
        setResult(ViewSetupMuscles.RESULT_OK,returnIntent);
        finish();
    }

    public void increaseMuscle(View view) {
        //todo: al momento faccio l'increase del primo della lista
        for (Integer i : ViewAllenamento.indexMuscle)
        {
            int intensity = (int) exercise.getMuscle_list().get(i).getTheIntesity();
            if(intensity+5<=100)
            {
                intensity= intensity+5;

            }
            exercise.getMuscle_list().get(i).setIntensity(intensity);

            Log.d("ViewSetupMuscles",""+ViewAllenamento.indexMuscle.size());

        }
        recyclerAdapterSetupMuscles.notifyDataSetChanged();


        //todo: in questo modo la recycler view resta aggioranta? No ma quando riapri si
    }

    public void decreaseMuscle(View view) {
        //todo: al momento faccio l'increase del primo della lista

        for (Integer i : ViewAllenamento.indexMuscle)
        {
            int intensity = (int) exercise.getMuscle_list().get(i).getTheIntesity();
            if(intensity-5>=0)
            {
                intensity= intensity-5;

            }
            exercise.getMuscle_list().get(i).setIntensity(intensity);

        }
        recyclerAdapterSetupMuscles.notifyDataSetChanged();

        //todo: in questo modo la recycler view resta aggioranta?
    }




}
