package com.beamdigital.beamfitness;


import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.Locale;


public class RecyclerAdapterHorizontal extends RecyclerView.Adapter<RecyclerAdapterHorizontal.LucaViewHolder>{

    String TAG = "RecyclerAdapter";
    String data[];
    Context context;
    private int externalHeight = -1;
    int numero =5;

    public void externalReset(int height) {
        notifyDataSetChanged();
        this.externalHeight = height;
    }

    public RecyclerAdapterHorizontal(Context context, String[] data ) {
        this.data = data;
        this.context = context;
    }

    @NonNull
    @Override
    public LucaViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater= LayoutInflater.from(parent.getContext());
        //View view = layoutInflater.inflate(R.layout.carousel,parent,false);
        LucaViewHolder viewHolder; // = new LucaViewHolder(view);;
        View view;
        switch (viewType){
             default: {
                view = layoutInflater.inflate(R.layout.carousel_replica,parent,false);
                viewHolder = new LucaViewHolder(view);
                break;
            }
            case 1:{
                view = layoutInflater.inflate(R.layout.carousel_one_replica,parent,false);
                viewHolder = new LucaViewHolder(view);

                break;
            }
            case 2:{
                view = layoutInflater.inflate(R.layout.carousel_two,parent,false);
                viewHolder = new LucaViewHolder(view);
                BFCManager bfcManager = ViewCentrale.get().getBfcManager();

                viewHolder.connect.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        bfcManager.connect(context);
                    }
                });

                //applico l'idea di Simonetta

                viewHolder.itemView.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        bfcManager.connect(context);
                    }
                });

                break;
            }

        }

        return viewHolder;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public void onBindViewHolder(@NonNull LucaViewHolder holder, int position) {
        ViewGroup.LayoutParams layoutParams = holder.itemView.getLayoutParams();
        Log.d(TAG, "onBindViewHolder: " + layoutParams.height);

        if (holder.height != null) {
            holder.height.setText(String.format(Locale.ITALIAN, "%dpx", layoutParams.height));
        }

        if (holder.textView != null) {
            holder.textView.setText(data[position]);
            holder.textView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(context, "ciao", Toast.LENGTH_SHORT).show();

                }
            });
        }

        if (imageShown) {
            //primo carosello
            //togliamo subito il collapsed
            if (holder.number != null) {
                holder.number.setVisibility(View.GONE);
            }
            if (holder.text != null) {
                holder.text.setVisibility(View.GONE);
            }
            if (holder.image != null) {
                holder.image.setVisibility(View.GONE);
            }
            //facciamo comparire tutti i pezzi
            //immagine ticket
            if (holder.ticketImage != null) {
                holder.ticketImage.setVisibility(View.VISIBLE);
            }
            //numero ticket
            if (holder.countTicket != null) {
                holder.countTicket.setVisibility(View.VISIBLE);
            }
            //SCritta ticket
            if (holder.textTicket != null) {
                holder.textTicket.setVisibility(View.VISIBLE);
            }
            //logo network
            if (holder.imageView != null) {
                holder.imageView.setVisibility(View.VISIBLE);
            }
            //numero
            if (holder.countDisponibili != null) {
                holder.countDisponibili.setVisibility(View.VISIBLE);
            }
            //scritta disponibili
            if (holder.textDisponibiliNetwork != null) {
                holder.textDisponibiliNetwork.setVisibility(View.VISIBLE);
            }
            //bottone
            if (holder.primo != null) {
                holder.primo.setVisibility(View.VISIBLE);
            }

            //secondo carosello
            //sezione collapse
            if (holder.imageView2Collapsed != null) {
                holder.imageView2Collapsed.setVisibility(View.GONE);
            }
            if (holder.invitationProgressCollapsed != null) {
                holder.invitationProgressCollapsed.setVisibility(View.GONE);
            }
            if (holder.invitationTextCollapsed != null) {
                holder.invitationTextCollapsed.setVisibility(View.GONE);
            }
            //l'immagine di newtork sparisce perchè ha lo stesso id con quella del primo carosello
            //3/4
            if (holder.invitationProgress != null) {
                holder.invitationProgress.setVisibility(View.VISIBLE);
            }
            //testo
            if (holder.invitationText != null) {
                holder.invitationText.setVisibility(View.VISIBLE);
            }
            //progressBar
            if (holder.determinateBar != null) {
                holder.determinateBar.setVisibility(View.VISIBLE);
            }
            //progressBar
            if (holder.determinateBarText != null) {
                holder.determinateBarText.setVisibility(View.VISIBLE);
            }
            //pulsante
            if (holder.riscattaPremio != null) {
                holder.riscattaPremio.setVisibility(View.VISIBLE);
            }



            //terzo carosello
            if (holder.titoloEMS != null) {
                holder.titoloEMS.setVisibility(View.VISIBLE);
            }
            if (holder.connection_status != null) {
                holder.connection_status.setVisibility(View.VISIBLE);
            }
            if (holder.tastConnetti != null) {
                holder.tastConnetti.setVisibility(View.VISIBLE);
            }



        } else {
            //primo carosello
            //togliamo subito il collapsed
            if (holder.number != null) {
                holder.number.setVisibility(View.VISIBLE);
            }
            if (holder.text != null) {
                holder.text.setVisibility(View.VISIBLE);
            }
            if (holder.image != null) {
                holder.image.setVisibility(View.VISIBLE);
            }
            //facciamo comparire tutti i pezzi
            //immagine ticket
            if (holder.ticketImage != null) {
                holder.ticketImage.setVisibility(View.GONE);
            }
            //numero ticket
            if (holder.countTicket != null) {
                holder.countTicket.setVisibility(View.GONE);
            }
            //SCritta ticket
            if (holder.textTicket != null) {
                holder.textTicket.setVisibility(View.GONE);
            }
            //logo network
            if (holder.imageView != null) {
                holder.imageView.setVisibility(View.GONE);
            }
            //numero
            if (holder.countDisponibili != null) {
                holder.countDisponibili.setVisibility(View.GONE);
            }
            //scritta disponibili
            if (holder.textDisponibiliNetwork != null) {
                holder.textDisponibiliNetwork.setVisibility(View.GONE);
            }
            //bottone
            if (holder.primo != null) {
                holder.primo.setVisibility(View.GONE);
            }


            holder.imageView.setVisibility(View.GONE);
            if (holder.countDisponibili != null) {
                holder.countDisponibili.setVisibility(View.GONE);
            }
            if (holder.textDisponibiliNetwork != null) {
                holder.textDisponibiliNetwork.setVisibility(View.GONE);
            }


            //secondo carosello
            //sezione collapse
            if (holder.imageView2Collapsed != null) {
                holder.imageView2Collapsed.setVisibility(View.VISIBLE);
            }
            if (holder.invitationProgressCollapsed != null) {
                holder.invitationProgressCollapsed.setVisibility(View.VISIBLE);
            }
            if (holder.invitationTextCollapsed != null) {
                holder.invitationTextCollapsed.setVisibility(View.VISIBLE);
            }
            //l'immagine di newtork sparisce perchè ha lo stesso id con quella del primo carosello
            //3/4
            if (holder.invitationProgress != null) {
                holder.invitationProgress.setVisibility(View.GONE);
            }
            //testo
            if (holder.invitationText != null) {
                holder.invitationText.setVisibility(View.GONE);
            }
            //progressBar
            if (holder.determinateBar != null) {
                holder.determinateBar.setVisibility(View.GONE);
            }
            //progressBar
            if (holder.determinateBarText != null) {
                holder.determinateBarText.setVisibility(View.GONE);
            }
            //pulsante
            if (holder.riscattaPremio != null) {
                holder.riscattaPremio.setVisibility(View.GONE);
            }


/*            //terzo carosello
            if (holder.titoloEMS != null) {
                holder.titoloEMS.setVisibility(View.GONE);
            }
            if (holder.connection_status != null) {
                holder.connection_status.setVisibility(View.GONE);
            }*/
            if (holder.tastConnetti != null) {
                holder.tastConnetti.setVisibility(View.GONE);
            }



        }

        BFCManager bfcManager = ViewCentrale.get().getBfcManager();
        //imposto lo stato della connessione nel carosello
        if (bfcManager.isFlag_connessione())
        {
            Log.d("Luca","notify" + holder.connection_status);
            //connesso
            if (holder.connection_status != null) {
                holder.connection_status.setText("Connesso");
                holder.connection_status.setTextColor(Color.GREEN);

                holder.connect.setText("Disconnetti");
            }
        }
        else
        {
            if (holder.connection_status != null) {
                holder.connection_status.setText("Disconnesso");
                holder.connection_status.setTextColor(Color.RED);

                holder.connect.setText("Connetti");


            }

        }
    }

    @Override
    public int getItemCount() {
        return data.length;
    }

    boolean imageShown = true;
    public void showImage(int height) {
        imageShown = true;
        notifyDataSetChanged();
    }

    public void hideImage(int height) {
        imageShown = false;
        notifyDataSetChanged();
    }


    //todo: cambiare il nome di questa classe
    public class LucaViewHolder extends RecyclerView.ViewHolder{


        //todo:sistemare questa parte perchè è collegata alla vecchia recycler
        TextView textView;
        TextView height;
        ImageView imageView;
        Button connect;

        //primo carousel
        ImageView ticketImage;
        TextView countTicket;
        TextView textTicket;
        TextView countDisponibili;
        TextView textDisponibiliNetwork;
        Button primo;
        //versione collapsing
        TextView number;
        TextView text;
        ImageView image;


        //secondo carousel
        TextView invitationProgress;
        TextView invitationText;
        ProgressBar determinateBar;
        TextView determinateBarText;
        Button riscattaPremio;


        //Versioine collapsing
        ImageView imageView2Collapsed;
        TextView invitationProgressCollapsed;
        TextView invitationTextCollapsed;

        //terzo carosello
        TextView connection_status;
        TextView titoloEMS;
        Button tastConnetti;




        public LucaViewHolder(@NonNull View itemView) {
            super(itemView);
            textView = itemView.findViewById(R.id.mese);
            height = itemView.findViewById(R.id.height);
            imageView = itemView.findViewById(R.id.imageView2);
            connect = itemView.findViewById(R.id.tastConnetti);



            //1° carosello
            countDisponibili = itemView.findViewById(R.id.numberNetwork);
            textDisponibiliNetwork = itemView.findViewById(R.id.textDisponibiliNetwork);
            primo = itemView.findViewById(R.id.ricarica);
            textTicket = itemView.findViewById(R.id.textDisponibiliTicket);
            countTicket = itemView.findViewById(R.id.numberTicket);
            ticketImage = itemView.findViewById(R.id.ticketImage);

            //versione collapse
            number = itemView.findViewById(R.id.numberTicketCollapsed);
            text = itemView.findViewById(R.id.textDisponibiliTicketCollapsed);
            image = itemView.findViewById(R.id.TicketCollapsed);

            //2° carosello
            invitationProgress = itemView.findViewById(R.id.invitationProgress);
            invitationText = itemView.findViewById(R.id.invitationText);
            determinateBar = itemView.findViewById(R.id.determinateBar);
            determinateBarText = itemView.findViewById(R.id.determinateBarText);
            riscattaPremio = itemView.findViewById(R.id.riscattaPremio);

            //versione collapse
            imageView2Collapsed = itemView.findViewById(R.id.imageView2Collapsed);
            invitationProgressCollapsed = itemView.findViewById(R.id.invitationProgressCollapsed);
            invitationTextCollapsed = itemView.findViewById(R.id.invitationTextCollapsed);




            //3° carosello
            connection_status = itemView.findViewById(R.id.connection_status);
            titoloEMS = itemView.findViewById(R.id.titoloEMS);
            tastConnetti = itemView.findViewById(R.id.tastConnetti);


        }
    }



}
