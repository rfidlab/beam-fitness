package com.beamdigital.beamfitness.dto;

import com.beamdigital.beamfitness.model.ExerciseEntity;
import com.beamdigital.beamfitness.model.Muscolo;

import java.util.ArrayList;

public class ExerciseDTO {

    private int  id; //ho messo quesso in char perche deve essere nonNullable
    private String name;
    private int frequency=-1;
    private int waveForm=-1;
    private int stimulationSec=-1;
    private int restSec=-1;
    private int durationMin=-1;
    private String video;
    private ArrayList<MuscoloDTO> muscleList;

    public ExerciseDTO() {
    }

    public ExerciseDTO(int id, String name, int frequency, int waveForm, int stimulationSec, int restSec, int durationMin, String video, ArrayList<MuscoloDTO> muscleList) {
        this.id = id;
        this.name = name;
        this.frequency = frequency;
        this.waveForm = waveForm;
        this.stimulationSec = stimulationSec;
        this.restSec = restSec;
        this.durationMin = durationMin;
        this.video = video;
        this.muscleList = muscleList;
    }

    /// from object to DTO
    public ExerciseDTO(ExerciseEntity e) {
        this(
                e.id_exercise,
                e.name,
                (int) e.frequency,
                (int) e.wave_form,
                e.stimulation_sec,
                e.rest_sec,
                e.duration_min,
                e.video,
                MuscoloDTO.convertAll(e.muscle_list)
        );
    }

    /// from object to DTO
    public static ArrayList<ExerciseDTO> convertAll(ArrayList<ExerciseEntity> list) {
        ArrayList<ExerciseDTO> l = new ArrayList<>();
        if(list != null) {
            for(ExerciseEntity e : list) {
                l.add(new ExerciseDTO(e));
            }
        }

        return l;
    }

    /// from DTO to object
    public ExerciseEntity revertDTO() {
        return new ExerciseEntity(
                this.id,
                this.name,
                (byte) this.frequency,
                (byte) this.waveForm,
                this.stimulationSec,
                this.restSec,
                this.durationMin,
                this.video,
                MuscoloDTO.revertAll(this.muscleList)
        );
    }

    /// from DTO to object
    public static ArrayList<ExerciseEntity> revertAll(ArrayList<ExerciseDTO> list) {
        ArrayList<ExerciseEntity> l = new ArrayList<>();
        if(list != null) {
            for(ExerciseDTO e : list) {
                l.add(e.revertDTO());
            }
        }

        return l;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getFrequency() {
        return frequency;
    }

    public void setFrequency(int frequency) {
        this.frequency = frequency;
    }

    public int getWaveForm() {
        return waveForm;
    }

    public void setWaveForm(int waveForm) {
        this.waveForm = waveForm;
    }

    public int getStimulationSec() {
        return stimulationSec;
    }

    public void setStimulationSec(int stimulationSec) {
        this.stimulationSec = stimulationSec;
    }

    public int getRestSec() {
        return restSec;
    }

    public void setRestSec(int restSec) {
        this.restSec = restSec;
    }

    public int getDurationMin() {
        return durationMin;
    }

    public void setDurationMin(int durationMin) {
        this.durationMin = durationMin;
    }

    public String getVideo() {
        return video;
    }

    public void setVideo(String video) {
        this.video = video;
    }

    public ArrayList<MuscoloDTO> getMuscleList() {
        return muscleList;
    }

    public void setMuscleList(ArrayList<MuscoloDTO> muscleList) {
        this.muscleList = muscleList;
    }
}
