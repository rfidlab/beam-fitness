package com.beamdigital.beamfitness.dto;

import com.beamdigital.beamfitness.model.Plan;

import java.util.ArrayList;

public class PlanDTO {

    public int id;
    private String name;
    private String cover;
    private ArrayList<ExerciseDTO> pretraining = new ArrayList<>(0);
    private ArrayList<ExerciseDTO> training = new ArrayList<>(0);
    private ArrayList<ExerciseDTO> posttraining = new ArrayList<>(0);

    public PlanDTO() {
    }

    public PlanDTO(int id, String name, String cover, ArrayList<ExerciseDTO> pretraining, ArrayList<ExerciseDTO> training, ArrayList<ExerciseDTO> posttraining) {
        this.id = id;
        this.name = name;
        this.cover = cover;
        this.pretraining = pretraining;
        this.training = training;
        this.posttraining = posttraining;
    }

    /// from object to DTO
    public PlanDTO(Plan p) {
        this(
                p.id_plan,
                p.getName(),
                p.getCover(),
                ExerciseDTO.convertAll(p.getPretraining()),
                ExerciseDTO.convertAll(p.getTraining()),
                ExerciseDTO.convertAll(p.getPosttraining())
        );
    }

    /// from DTO to object
    public Plan revertDTO() {
        return new Plan(
                this.id,
                this.name,
                this.cover,
                ExerciseDTO.revertAll(pretraining),
                ExerciseDTO.revertAll(training),
                ExerciseDTO.revertAll(posttraining)
        );
    }

    public String getCover() {
        return cover;
    }

    public void setCover(String cover) {
        this.cover = cover;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<ExerciseDTO> getPretraining() {
        return pretraining;
    }

    public void setPretraining(ArrayList<ExerciseDTO> pretraining) {
        this.pretraining = pretraining;
    }

    public ArrayList<ExerciseDTO> getTraining() {
        return training;
    }

    public void setTraining(ArrayList<ExerciseDTO> training) {
        this.training = training;
    }

    public ArrayList<ExerciseDTO> getPosttraining() {
        return posttraining;
    }

    public void setPosttraining(ArrayList<ExerciseDTO> posttraining) {
        this.posttraining = posttraining;
    }
}
