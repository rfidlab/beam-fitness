package com.beamdigital.beamfitness.dto;

import com.beamdigital.beamfitness.model.Muscolo;

import java.util.ArrayList;

public class MuscoloDTO {

    private int code;
    private boolean active;
    private int intensity;
    private String name;

    public MuscoloDTO() {
    }

    public MuscoloDTO(int code, boolean active, int intensity, String name) {
        this.code = code;
        this.active = active;
        this.intensity = intensity;
        this.name = name;
    }

    /// from object to DTO
    public MuscoloDTO(Muscolo m) {
        this.code = m.getTheCode();
        this.active = m.getIsActive();
        this.intensity = m.getTheIntesity();
        this.name = m.getName();
    }

    /// from object to DTO
    public static ArrayList<MuscoloDTO> convertAll(ArrayList<Muscolo> list) {
        ArrayList<MuscoloDTO> l = new ArrayList<>();
        if(list != null){
            for(Muscolo m : list) {
                l.add(new MuscoloDTO(m));
            }
        }

        return l;
    }

    /// from DTO to object
    public Muscolo revertDTO() {
        return new Muscolo(this.name, this.code, this.active, this.intensity);
    }

    /// from DTO to object
    public static ArrayList<Muscolo> revertAll(ArrayList<MuscoloDTO> list) {
        ArrayList<Muscolo> l = new ArrayList<>();
        if (list != null) {
            for (MuscoloDTO m : list) {
                l.add(m.revertDTO());
            }
        }

        return l;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public int getIntensity() {
        return intensity;
    }

    public void setIntensity(int intensity) {
        this.intensity = intensity;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
