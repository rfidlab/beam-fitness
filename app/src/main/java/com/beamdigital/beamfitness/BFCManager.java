package com.beamdigital.beamfitness;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import eu.beamdigital.beamfitnesslibrary.BluetoothFitnessClient;

public class BFCManager implements BluetoothFitnessClient.ChangeBleFlagListener{

    private static final String TAG = BFCManager.class.getSimpleName();

    BluetoothFitnessClient client;
    boolean flag_connessione;
    BfcInterface listener;

    public void setBfcInterface (BfcInterface listener)
    {
        this.listener=listener;
    }

    public interface BfcInterface{
        public void onConnectionStateEvent (boolean state);
    }

    public boolean isFlag_connessione() {
        return flag_connessione;
    }

    @Override
    public void bleFlagChanged(boolean b) {
        Log.d("Luca", "bleFlagChanged: " + b);
        if(listener!=null)
        {
            listener.onConnectionStateEvent(b);
        }
        flag_connessione = b;
        if (b) {
            if(client != null) {
                client.getBattery();
                //todo: se ci disconnettiamo, la tuta continua a stimolare, quindi mi ricollego e chiude quello che stava facendo
                client.sendStopCommand();

            }
        }

    }


    public void connect(Context context){

        if(flag_connessione!=true)
        {
            if (client == null) {
                client = new BluetoothFitnessClient(context);
                client.setListener(this);
                client.addNames("mbody", "BALANX");
            }
            client.startScan();
            //todo: facciamo diventare start scan boolean per avere una risposta da dare all'utente?
            Toast.makeText(context, "Mi collego alla tuta", Toast.LENGTH_SHORT).show();

        }
        else
        {
            Toast.makeText(context, "Sei già connesso alla tuta", Toast.LENGTH_SHORT).show();

        }

    }



}
