package com.beamdigital.beamfitness;

public class Workout {

    private String nome;
    private String frequenza;
    private String forma;
    private String potenza;
    private String muscolo;
    private String stimolazione;
    private String riposo;
    private String durata;

    public Workout(String nome, String frequenza, String forma, String potenza, String muscolo, String stimolazione, String riposo, String durata) {
        this.nome = nome;
        this.frequenza = frequenza;
        this.forma = forma;
        this.potenza = potenza;
        this.muscolo = muscolo;
        this.stimolazione = stimolazione;
        this.riposo = riposo;
        this.durata = durata;
    }

    public String getNome() {
        return nome;
    }

    public String getFrequenza() {
        return frequenza;
    }

    public String getForma() {
        return forma;
    }

    public String getPotenza() {
        return potenza;
    }

    public String getMuscolo() {
        return muscolo;
    }

    public String getStimolazione() {
        return stimolazione;
    }

    public String getRiposo() {
        return riposo;
    }

    public String getDurata() {
        return durata;
    }
}
