package com.beamdigital.beamfitness;

import android.app.Dialog;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Image;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.util.Base64InputStream;
import android.util.Base64OutputStream;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.DialogFragment;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.beamdigital.beamfitness.database.PlanDao;
import com.beamdigital.beamfitness.dto.PlanDTO;
import com.beamdigital.beamfitness.model.Plan;
import com.beamdigital.beamfitness.realtime.RTDBManager;
import com.bumptech.glide.Glide;
import com.bumptech.glide.GlideExperiments;
import com.bumptech.glide.load.Encoder;
import com.bumptech.glide.load.resource.gif.ByteBufferGifDecoder;
import com.bumptech.glide.load.resource.gif.GifDrawable;
import com.google.android.gms.common.util.Base64Utils;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputEditText;
import com.beamdigital.beamfitness.database.AppDatabase;
import com.beamdigital.beamfitness.database.ExerciseDao;
import com.beamdigital.beamfitness.dialog.MuscleSelectionDialog;
import com.beamdigital.beamfitness.model.ExerciseEntity;
import com.beamdigital.beamfitness.model.Muscolo;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.Buffer;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;


import static com.beamdigital.beamfitness.DemoActivityController.isDownloadsDocument;
import static com.beamdigital.beamfitness.DemoActivityController.isExternalStorageDocument;
import static com.beamdigital.beamfitness.DemoActivityController.isGooglePhotosUri;
import static com.beamdigital.beamfitness.DemoActivityController.isMediaDocument;
import static eu.beamdigital.beamfitnesslibrary.Constants.COSINE_WAVE;
import static eu.beamdigital.beamfitnesslibrary.Constants.EXPONENTIAL_WAVE;
import static eu.beamdigital.beamfitnesslibrary.Constants.SAWTOOTH_WAVE;
import static eu.beamdigital.beamfitnesslibrary.Constants.SINE_WAVE;
import static eu.beamdigital.beamfitnesslibrary.Constants.TRIANGLE_WAVE;

public class ViewCreaEsercizio extends AppCompatActivity implements MuscleSelectionDialog.MuscleSelectionInterface {

    private static final String TAG = ViewCreaEsercizio.class.getSimpleName();

    private AppDatabase db;
    private ExerciseDao exerciseDao;

    TextInputEditText workoutName;
    TextView frequency;
    TextView waveForm;
    TextView stimulation;
    TextView rest;
    TextView duration;
    ImageView video;
    TextView deleteEx;
    Uri gif;
    long timestamp;
    boolean update=false;

    //per la recyclerView dei muscoli
    RecyclerView muscleList;
    RecyclerAdapterMuscles recyclerAdapterMuscles;

    ExerciseEntity exercise = new ExerciseEntity();

    //ExerciseEntity usato per le modifiche e le rimozioni
    //todo: vedere se funziona la rimozione usando exercise, quello sopra
    ExerciseEntity  modifica;



    //per caricare le immagini
    ActivityResultLauncher<String> mGetContent = registerForActivityResult(new ActivityResultContracts.GetContent(),
            new ActivityResultCallback<Uri>() {
                @Override
                public void onActivityResult(Uri uri) {
                    gif = uri;
                    Glide.with(getApplicationContext()).asGif().load(uri).into(video);
                    video.setVisibility(View.VISIBLE);
                    timestamp=Calendar.getInstance().getTime().getTime();
                    exercise.video=timestamp+"_"+FileService.queryName(getApplicationContext(),gif);

                }
            });





    private MuscleSelectionDialog dialog;
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.view_crea_esercizio);
        workoutName = findViewById(R.id.workoutName);
        frequency = findViewById(R.id.pick_frequenza);
        waveForm = findViewById(R.id.pick_onda);
        stimulation = findViewById(R.id.pick_stimolazione);
        rest = findViewById(R.id.pick_riposo);
        duration = findViewById(R.id.pick_durata);
        video = findViewById(R.id.previewVideo);
        deleteEx = findViewById(R.id.deleteEx);

        //recyclerView
        muscleList = findViewById(R.id.listaMuscoli);
        //settiamo il LayoutManager
        muscleList.setLayoutManager((new LinearLayoutManager(this, RecyclerView.VERTICAL, false)));
        //abbiamo  2 costruttori per quando non vuoi far vedere la potenza
        recyclerAdapterMuscles = new RecyclerAdapterMuscles(this, new String[0],false);
        //intestiamo l'adapter alla nostra recycler view
        muscleList.setAdapter(recyclerAdapterMuscles);

        dialog = new MuscleSelectionDialog(this);
        //questo va bene qui
        db = AppDatabase.getDatabase(this);
        exerciseDao = db.ExerciseDao();


        //se faccio un modifica esercizio serve questa roba
        try {
            Bundle bundle = getIntent().getExtras();
            //todo: capire se fare una nuova variabiale o scrivere qualla principale di questa classe
            modifica= new Gson().fromJson(   bundle.getString("exercise")  , ExerciseEntity.class);
            //inizio a popolare la schermata
            workoutName.setText(modifica.getName());

            //Dati sulla stimolazione
            frequency.setText(String.format(Locale.ITALIAN, "%d Hz", modifica.getFrequency()));
            frequency.setTextSize(20);

            waveForm.setTextSize(15);
            if (modifica.getWave_form()==1){
                waveForm.setText("Sinusoidale");
            }
            else if (modifica.getWave_form()==2){
                waveForm.setText("Triangolare");
            }
            else if (modifica.getWave_form()==4){
                waveForm.setText("Quadra");
            }
            else if (modifica.getWave_form()==8){
                waveForm.setText("Seghettata");
            }
            else if (modifica.getWave_form()==16){
                waveForm.setText("Esponenziale");
            }

            stimulation.setText(String.format(Locale.ITALIAN, "%ds", modifica.getStimulation_sec()));
            stimulation.setTextSize(15);

            rest.setText(String.format(Locale.ITALIAN, "%ds", modifica.getRest_sec()));
            rest.setTextSize(15);

            duration.setText(String.format(Locale.ITALIAN, "%dm", modifica.getDuration_min()));
            duration.setTextSize(15);

            //Lista dei muscoli presenti
            String[] muscoli = new String[modifica.getMuscle_list().size()];
            for(int i=0;i<modifica.getMuscle_list().size();i++){
                muscoli[i]=modifica.getMuscle_list().get(i).getName();
            }

            recyclerAdapterMuscles.setDataset(muscoli);
            muscleList.setVisibility(View.VISIBLE);
            deleteEx.setVisibility(View.VISIBLE);


            //con questo pezzo mostro i pallini quando apri il dialog
            for(int i=0;i<muscoli.length;i++)
            {
                selectedMuscles.add(muscoli[i]);

            }
            dialog.setSelected(selectedMuscles);

            video.setVisibility(View.VISIBLE);


            //todo: ricontrolla se manca qualche cosa

            //todo: exerciseEntity è una variabile utile o no?

            //INIZIO PEZZO CHE CARICA GIF
            StorageReference storageReference = FirebaseStorage.getInstance().getReference();
            StorageReference ref = storageReference.child("gif/"+modifica.video);

            ref.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                @Override
                public void onSuccess(Uri uri) {
                    Glide.with(getApplicationContext()).asGif().load(uri).into(video);
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    //Toast.makeText(ViewCreaEsercizio.this, "FORSE MANCA IL VIDEO DELL'ESERCIZIO", Toast.LENGTH_SHORT).show();

                }
            });
            //FINE PEZZO CHE CARICA GIF

            exercise=modifica;
            update=true;


        } catch (Exception e) {
            e.printStackTrace();
        }



    }

/*    public void click_nome(View view) {
        //TODO: sfruttare quello già esistente in BeamActivity
        final Dialog d = new Dialog(ViewCreaEsercizio.this);
        d.setContentView(R.layout.name_workout);
        Button b1 = d.findViewById(R.id.si);
        Button b2 = d.findViewById(R.id.no);

        final EditText nw = d.findViewById(R.id.allenamento);

        b1.setOnClickListener(v -> {
            workoutName.setTextColor(getColor(R.color.black));
            workoutName.setText(nw.getText().toString()+" ");
            exercise.name=nw.getText().toString();
            d.dismiss();
        });
        b2.setOnClickListener(v -> d.dismiss());

        d.show();
    }*/


    //QUESTO METODO MI SEMPLIFICA LA PARTE DEI PICKER
    private Dialog set_default_dialog(int res_id_label) {
        Log.d("TAG", "set_default_dialog: " + getString(res_id_label));
        final Dialog d = new Dialog(ViewCreaEsercizio.this,android.R.style.Theme_DeviceDefault_Light_Dialog);
        d.setContentView(R.layout.prova_picker);
        TextView label = d.findViewById(R.id.picker_label);
        label.setText(res_id_label);
        Button b2 = d.findViewById(R.id.annulla);
        b2.setOnClickListener(v -> d.dismiss());

        return d;
    }

    public void click_frequenza(View view) {
        final Dialog d = set_default_dialog(R.string.frequency_label);
        Button b1 = d.findViewById(R.id.accetta);
        final NumberPicker np = d.findViewById(R.id.picker);
        np.setMaxValue(100);
        np.setMinValue(1);
        np.setWrapSelectorWheel(false);

        b1.setOnClickListener(v -> {
            frequency.setText(String.format(Locale.ITALIAN, "%d Hz", np.getValue()));
            frequency.setTextSize(20);
            exercise.frequency = (byte) np.getValue();
            d.dismiss();
        });

        d.show();
    }

    public void click_onda(View view) {
        final String[] wave = new String[]{ "Sinusoidale", "Triangolare", "Quadra", "Seghettata", "Esponenziale" };
        final Dialog d = set_default_dialog(R.string.wave_form_label);
        Button b1 = d.findViewById(R.id.accetta);
        final NumberPicker np = d.findViewById(R.id.picker);
        np.setMaxValue(wave.length-1);
        np.setMinValue(0);
        np.setDisplayedValues(wave);
        np.setWrapSelectorWheel(false);

        b1.setOnClickListener(v -> {
            waveForm.setText(wave[np.getValue()]);
            waveForm.setTextSize(15);
            if (np.getValue()==0){
                exercise.wave_form = SINE_WAVE;
            }
            else if (np.getValue()==1){
                exercise.wave_form = TRIANGLE_WAVE;
            }
            else if (np.getValue()==2){
                exercise.wave_form = COSINE_WAVE;
            }
            else if (np.getValue()==3){
                exercise.wave_form = SAWTOOTH_WAVE;
            }
            else if (np.getValue()==4){
                exercise.wave_form = EXPONENTIAL_WAVE;
            }
            d.dismiss();
        });
        d.show();

    }

    public void click_stimolazione(View view) {
        final Dialog d = set_default_dialog(R.string.time_label);
        Button b1 = d.findViewById(R.id.accetta);
        final NumberPicker np = d.findViewById(R.id.picker);
        np.setMaxValue(10);
        np.setMinValue(0);
        np.setWrapSelectorWheel(false);

        b1.setOnClickListener(v -> {
            if(np.getValue()==1)
            {
                stimulation.setText(String.format(Locale.ITALIAN, "%ds", np.getValue()));

            }
            else
            {
                stimulation.setText(String.format(Locale.ITALIAN, "%ds", np.getValue()));
            }
            stimulation.setTextSize(15);
            exercise.stimulation_sec= np.getValue();
            d.dismiss();
        });

        d.show();

    }

    public void click_riposo(View view) {
        final Dialog d = set_default_dialog(R.string.rest_label);
        Button b1 = d.findViewById(R.id.accetta);
        final NumberPicker np = d.findViewById(R.id.picker);
        np.setMaxValue(60);
        np.setMinValue(0);
        np.setWrapSelectorWheel(false);

        b1.setOnClickListener(v -> {
            if(np.getValue()==1)
            {
                rest.setText(String.format(Locale.ITALIAN, "%ds", np.getValue()));

            }
            else{
                rest.setText(String.format(Locale.ITALIAN, "%ds", np.getValue()));
            }
            rest.setTextSize(15);
            exercise.rest_sec= np.getValue();
            //todo: inserire anche riposo_scheda_backup??
            d.dismiss();
        });
        d.show();
    }

    public void click_durata(View view) {
        final Dialog d = set_default_dialog(R.string.program_label);
        Button b1 = d.findViewById(R.id.accetta);
        final NumberPicker np = d.findViewById(R.id.picker);
        np.setMaxValue(59);
        np.setMinValue(1);
        np.setWrapSelectorWheel(false);

        b1.setOnClickListener(v -> {
            if(np.getValue()==1){
                duration.setText(String.format(Locale.ITALIAN, "%dm", np.getValue()));
            }
            else{
                duration.setText(String.format(Locale.ITALIAN, "%dm", np.getValue()));
            }
            duration.setTextSize(15);
            exercise.duration_min=np.getValue();
            d.dismiss();
        });

        d.show();
    }





    public void click_salva(View view) {
        RTDBManager manager = RTDBManager.getInstance();
        //controlla che tutti i campi siano correttamente compilati tranne i muscoli, quelli non è obbligatorio controllarli
        if(isFilled(exercise))
        {

            //dobbiamo o no caricare una nuova gif?
            if(gif!=null)
            {
                FirebaseStorage storage = FirebaseStorage.getInstance();
                StorageReference storageRef = storage.getReference();
                try {

                    StorageReference imageRef = storageRef.child("gif/"+exercise.video);
                    UploadTask uploadTask = imageRef.putFile(  Uri.fromFile(new File(FileService.getFile(getApplicationContext(),gif).getPath())));

                    uploadTask.addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> task) {
                            if(task.isSuccessful())
                            {
                                Toast.makeText(ViewCreaEsercizio.this, "Salvato sul cloud", Toast.LENGTH_SHORT).show();

                            }
                            else{
                                Toast.makeText(ViewCreaEsercizio.this, "Errore salvataggio sul cloud", Toast.LENGTH_SHORT).show();

                            }
                            //tood:controlla se ha caricato o no la foto
                            //db.ExerciseDao().insert(exercise);
                            if(update)
                            {
                                exercise.name=workoutName.getText().toString();
                                manager.updateExerciseToRTDB(exercise);
                                db.ExerciseDao().update(exercise);
                            }
                            else
                            {
                                manager.addAllExercisesToRTDB(exercise);
                                db.ExerciseDao().insert(exercise);

                            }
                        }
                    });

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            else
            {
                if(update)
                {
                    exercise.name=workoutName.getText().toString();
                    manager.updateExerciseToRTDB(exercise);
                    db.ExerciseDao().update(exercise);
                }
                else
                {
                    manager.addAllExercisesToRTDB(exercise);
                    db.ExerciseDao().insert(exercise);
                }

            }
            finish();
            Toast.makeText(ViewCreaEsercizio.this, "Esercizio salvato!", Toast.LENGTH_SHORT).show();

        }
    }

    private boolean isFilled(ExerciseEntity exercise){
        boolean result = true;


        if(exercise.frequency == -1){
            Toast.makeText(ViewCreaEsercizio.this, "Campo frequenza assente!", Toast.LENGTH_SHORT).show();
            return false;
        } else if(exercise.wave_form == -1){
            Toast.makeText(ViewCreaEsercizio.this, "Campo onda assente!", Toast.LENGTH_SHORT).show();
            result=false;
        } else if(exercise.stimulation_sec == -1){
            Toast.makeText(ViewCreaEsercizio.this, "Campo stimolazione assente!", Toast.LENGTH_SHORT).show();
            result=false;
        } else if(exercise.rest_sec == -1){
            Toast.makeText(ViewCreaEsercizio.this, "Campo riposo assente!", Toast.LENGTH_SHORT).show();
            result=false;
        } else if(exercise.duration_min == -1) {
            Toast.makeText(ViewCreaEsercizio.this, "Campo durata assente!", Toast.LENGTH_SHORT).show();
            result = false;
        } else if(workoutName.getText().toString().length() == 0) {
            Toast.makeText(ViewCreaEsercizio.this, "Campo nome assente!", Toast.LENGTH_SHORT).show();
            result=false;
        } else if(exercise.muscle_list == null || exercise.muscle_list.size() == 0) {
            Toast.makeText(ViewCreaEsercizio.this, "Seleziona almeno un muscolo!", Toast.LENGTH_SHORT).show();
            result=false;
        }
        else if (exercise.stimulation_sec == 0 && exercise.rest_sec == 0){
            Toast.makeText(ViewCreaEsercizio.this, "Tempo di stimolazione e riposono = 0s", Toast.LENGTH_SHORT).show();
            result=false;
        }
        exercise.name=workoutName.getText().toString();
        return result;
    }


    public void click_muscolo(View view) {
        //todo: ho tolto questo set selected e spostata la logica dentro muscleSelectionDialog
        //dialog.setSelected(selectedMuscles);
        dialog.show(getSupportFragmentManager(), null);

    }

    public void click_add_video(View view) {
        Log.d(TAG,"video");
        //loadVideo();
        mGetContent.launch("image/*");
    }


    @Override
    public void onClose() {
        Log.d(TAG, "onClose: dialog closed");
    }

    private ArrayList<String> selectedMuscles = new ArrayList<>(0);

    /// quando clicchiamo su salva nel dialog
    @Override
    public void onSave(ArrayList<String> muscles) {
        Log.d(TAG, "onSave: " + Arrays.toString(muscles.toArray()));

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            muscles.removeIf(m -> Const.fromMuscleToCode(m) == -1);
        }

        /// aggiorniamo la lista dei muscoli selezionati
        selectedMuscles.clear();
        selectedMuscles = new ArrayList<>(muscles);
        Log.d(TAG, "onSave: selected: " + Arrays.toString(selectedMuscles.toArray()));

        /// aggiorniamo la recycler view
        String[] l = new String[muscles.size()];
        muscles.toArray(l);
        if(selectedMuscles.size()==0)
        {
            muscleList.setVisibility(View.GONE);
        }
        else
        {
            muscleList.setVisibility(View.VISIBLE);
        }
        recyclerAdapterMuscles.setDataset(l);

        /// aggiorniamo l'exercise
        if(exercise == null)
            exercise = new ExerciseEntity();
        if(exercise.muscle_list == null)
            exercise.muscle_list = new ArrayList<>(0);
        exercise.muscle_list.clear();

        for(String m : muscles) {
            int code = Const.fromMuscleToCode(m);
            if(code != -1) {
                exercise.muscle_list.add(new Muscolo(m, code, true, 20));
            }
        }

        Log.d(TAG, "onSave: ExerciseEntity: " + exercise.toString());
    }

    public void click_delete(View view) {
        RTDBManager manager = RTDBManager.getInstance();

        manager.getAllPlans(task -> {
            if(task.isComplete() && task.isSuccessful()) {
                DataSnapshot result = task.getResult();
                if(result != null) {
                    for (DataSnapshot next : result.getChildren()) {
                        PlanDTO value = next.getValue(PlanDTO.class);
                        Plan fine =value.revertDTO().removeExercise(modifica);
                        if (fine!=null)
                        {
                            //RIMUOVO L'ESERCIZIO DAI PLAN
                            //lo rimuovo dal telefono
                            PlanDao planDao = AppDatabase.getDatabase(this).getPlanDao();
                            planDao.update(fine);

                        }
                    }
                }
            }
        });

        //ho rimosso l'esercizio da ogni parte in RTDB
        manager.removeExerciseToRTDB(modifica);

        //lo rimuovo dall'elenco degli esercizi nel telefono
        AppDatabase.getDatabase(this).ExerciseDao().delete(modifica);


        finish();
        Toast.makeText(ViewCreaEsercizio.this, "Esercizio eliminato", Toast.LENGTH_SHORT).show();


    }
}
