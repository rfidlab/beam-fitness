package com.beamdigital.beamfitness;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.beamdigital.beamfitness.model.ExerciseEntity;

public class RecyclerAdapterSetupMuscles extends RecyclerView.Adapter<RecyclerAdapterSetupMuscles.SetupMusclesHolder>{


    Context context;
    ExerciseEntity exerciseEntity;

    public RecyclerAdapterSetupMuscles(Context context, ExerciseEntity exerciseEntity) {
        this.context=context;
        this.exerciseEntity=exerciseEntity;

    }

    @NonNull
    @Override
    public RecyclerAdapterSetupMuscles.SetupMusclesHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.recycler_view_muscoli,parent,false);
        RecyclerAdapterSetupMuscles.SetupMusclesHolder setupMusclesHolder = new RecyclerAdapterSetupMuscles.SetupMusclesHolder(view);
        return setupMusclesHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerAdapterSetupMuscles.SetupMusclesHolder holder, int position) {
        holder.muscle.setText(exerciseEntity.getMuscle_list().get(position).getName());
        holder.power.setText(""+(int)exerciseEntity.getMuscle_list().get(position).getTheIntesity());

        if (ViewAllenamento.indexMuscle.contains(position))
        {
            holder.background=true;
            holder.itemView.setBackgroundResource(R.drawable.ic_slot_riepilogo_muscoli_selected);
        }
        else
        {
            holder.background=false;
            holder.itemView.setBackgroundResource(R.drawable.ic_slot_riepilogo_muscoli);
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //todo: al posto del toast salvare muscoli selezionati, così poi premi + e meno e incrementi i selezioanti
                if(holder.background)
                {
                    holder.itemView.setBackgroundResource(R.drawable.ic_slot_riepilogo_muscoli);
                    holder.background=false;

                    if(ViewAllenamento.indexMuscle!= null && ViewAllenamento.indexMuscle.contains(position)) {
                        ViewAllenamento.indexMuscle.remove((Integer)position);
                    }


                }
                else
                {
                    holder.itemView.setBackgroundResource(R.drawable.ic_slot_riepilogo_muscoli_selected);
                    holder.background=true;
                    if(ViewAllenamento.indexMuscle!= null) {
                        ViewAllenamento.indexMuscle.add((Integer)position);
                    }

                }
            }
        });


    }

    @Override
    public int getItemCount() {
        return exerciseEntity.getMuscle_list().size();
    }

    public class SetupMusclesHolder extends RecyclerView.ViewHolder{

        TextView muscle;
        TextView power;
        boolean background =false;
        public SetupMusclesHolder(@NonNull View itemView) {
            super(itemView);
            muscle = itemView.findViewById(R.id.recyView_name_muscle);
            power = itemView.findViewById(R.id.recView_powerMuscle);
        }
    }
}
