package com.beamdigital.beamfitness;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.beamdigital.beamfitness.model.Plan;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;
import java.util.List;

import static androidx.core.content.ContextCompat.startActivity;

public class RecyclerAdapterVertical extends RecyclerView.Adapter<RecyclerAdapterVertical.PlanViewHolder> {

    StorageReference storageReference = FirebaseStorage.getInstance().getReference();
    private final static String TAG = "RecyclerAdapter";
    private final Context context;
    private ArrayList<Plan> dataset;
    private boolean heart_empty =false;
    public RecyclerAdapterVertical(Context context) {
        this.context = context;
        this.dataset = new ArrayList<>(0);
        if(context instanceof StartPlanOnClickListener)
            this.listener = (StartPlanOnClickListener) context;
    }

    @NonNull
    @Override
    public PlanViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        //todo inserisci uno switch per i 3 casi di viewtype, case etc etc e di conseguenza cambio il layout
        LayoutInflater layoutInflater= LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.recycler_view_exercise,parent,false);
        PlanViewHolder viewHolder = new PlanViewHolder(view);
        return viewHolder;
    }

    @Override
    public int getItemViewType(int position) {
        //todo da implementare le 3 casistite per il carosello
        return super.getItemViewType(position);
    }

    public interface StartPlanOnClickListener {
        void startPlanOnClick(Plan p);
    }

    private StartPlanOnClickListener listener;

    @Override
    public void onBindViewHolder(@NonNull PlanViewHolder holder, int position) {
        ViewGroup.LayoutParams layoutParams = holder.itemView.getLayoutParams();
        Log.d(TAG, "onBindViewHolder: " + layoutParams.height);

        final Plan p = this.dataset.get(position);

        if(holder.play != null) {
            holder.play.setOnClickListener(view -> {
                if(listener != null) {
                    listener.startPlanOnClick(p);
                }
            });
        }

        if(holder.heart != null) {
            holder.heart.setOnClickListener(view -> {
                if(heart_empty == false) {

                    holder.heart.setImageDrawable(context.getDrawable(R.drawable.ic_heart_empty));
                    heart_empty =true;
                }
                else{
                    holder.heart.setImageDrawable(context.getDrawable(R.drawable.ic_heart_full));
                    heart_empty =false;
                }
            });
        }

        if (holder.planName != null) {
            holder.planName.setText(p.getName());
        }

        //INIZIO PEZZO CHE CARICA GIF
        StorageReference ref = storageReference.child("copertina/"+dataset.get(position).getCover());

        ref.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                Glide.with(context).load(uri).dontAnimate().into(holder.exercise_background);
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Glide.with(context).load("https://media.giphy.com/media/nqCH5dsIn99cmSPVrd/giphy.gif").dontAnimate().into(holder.exercise_background);
            }
        });

/*        if(position%2==0)
        {
            Glide.with(context).load("https://media.giphy.com/media/nqCH5dsIn99cmSPVrd/giphy.gif").dontAnimate().into(holder.exercise_background);


        }
        else
        {
            Glide.with(context).load("https://media.giphy.com/media/xOr2qyP3HHpLopMbLj/giphy.gif").dontAnimate().into(holder.exercise_background);

        }*/


        holder.itemView.setOnLongClickListener(v -> {
            Intent intent= new Intent(context, ViewCreaScheda.class);
            intent.putExtra("plan",dataset.get(holder.getBindingAdapterPosition()).toString());
            startActivity(context, intent , null);

            return false;
        });
    }

    @Override
    public int getItemCount() {
        return dataset.size();
    }

    public void setDataset(List<Plan> plans) {
        this.dataset = new ArrayList<>(plans);
        notifyDataSetChanged();
    }

    public class PlanViewHolder extends RecyclerView.ViewHolder{

        TextView planName;
        ImageButton heart;
        ImageButton play;
        ImageView exercise_background;

        public PlanViewHolder(@NonNull View itemView) {
            super(itemView);
            planName = itemView.findViewById(R.id.plan_name);
            play = itemView.findViewById(R.id.play);
            exercise_background = itemView.findViewById(R.id.exercise_background);
            heart = itemView.findViewById(R.id.like);


        }
    }
}
