package com.beamdigital.beamfitness.dialog;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.beamdigital.beamfitness.Const;
import com.beamdigital.beamfitness.R;

import java.util.ArrayList;
import java.util.Arrays;

import static android.app.AlertDialog.THEME_DEVICE_DEFAULT_LIGHT;

public class MuscleSelectionDialog extends DialogFragment {

    private final static String TAG = "MuscleSelectionDialog";
    private final Activity activity;
    private final ArrayList<String> muscles;
    private final boolean[] checked = new boolean[]{ false, false, false, false, false, false, false, false, false, false};

    public void setSelected(ArrayList<String> selectedMuscles) {
        Log.d(TAG, "setSelected: " + Arrays.toString(selectedMuscles.toArray()));
        for(int i = 0; i < Const.muscle.length; i++){
            checked[i] = selectedMuscles.contains(Const.muscle[i]);
            //todo:questa parte mi aiuta nel caso volessi aggiornare un esercizio
            if(selectedMuscles.contains(Const.muscle[i])&&!muscles.contains(Const.muscle[i]))
            {
                muscles.add(Const.muscle[i]);
            }
        }


        Log.d(TAG, "setSelected: checked" + Arrays.toString(checked));
    }

    public interface MuscleSelectionInterface {
        void onClose();
        void onSave(ArrayList<String> muscles);
    }

    private MuscleSelectionInterface listener;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if(context instanceof MuscleSelectionInterface)
            this.listener = (MuscleSelectionInterface) context;
        else
            throw new IllegalArgumentException("MuscleSelectionInterface not implemented");
    }

    public MuscleSelectionDialog(Activity activity) {
        this.activity = activity;
        muscles = new ArrayList<>(0);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        Log.d(TAG, "onCreateDialog");
        AlertDialog.Builder builder = new AlertDialog.Builder(activity,THEME_DEVICE_DEFAULT_LIGHT);
        builder.setTitle(R.string.add_muscles)
                .setMultiChoiceItems(
                        Const.muscle,
                        checked,
                        (dialogInterface, i, b) -> {
                            if(b) {
                                muscles.add(Const.muscle[i]);
                            } else {
                                muscles.remove(Const.muscle[i]);
                            }
                        }
                )
                .setPositiveButton(R.string.ok, (dialogInterface, i) -> {
                    if(listener != null)
                        listener.onSave(muscles);
                })
                .setNegativeButton(R.string.cancel, (dialogInterface, i) -> {
                    if(listener != null)
                        listener.onClose();
                })
                .setNeutralButton("Total Body",(dialogInterface,i) -> {
                    for(int j=0;j<checked.length;j++)
                    {
                        if(!checked[j])
                        {
                            checked[j]=true;
                            muscles.add(Const.muscle[j]);
                        }
                    }
                    builder.show();

                });
        return builder.create();
    }

    public void confirmChecked(ArrayList<String> muscles)
    {

    }
}
