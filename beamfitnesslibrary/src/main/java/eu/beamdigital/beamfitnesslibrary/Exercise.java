package eu.beamdigital.beamfitnesslibrary;


import com.google.gson.Gson;

import java.util.List;

//QUI SIAMO PASSATI DA PRIMITIVI AD OGGETTO E NEL COSTRUTTORE VUOTO DA TUTTI 0 A TUTTI NULL
public class Exercise {
    public String name;
    public Byte frequency;
    public Byte wave_form;
    public Integer stimulation_sec;
    public Integer rest_sec;
    public Integer duration_min;
    public String video;
    public List<InfoMuscolo> muscle_list;

    public Exercise() {
        this.frequency=null;
        this.wave_form=null;
        this.stimulation_sec=null;
        this.rest_sec=null;
        this.duration_min=null;
        this.video=null;
        this.muscle_list=null;
    }

    public Exercise(byte frequency, byte wave_form, int stimulation_sec, int rest_sec, int duration_min) {
        this.frequency = frequency;
        this.wave_form = wave_form;
        this.stimulation_sec = stimulation_sec;
        this.rest_sec = rest_sec;
        this.duration_min = duration_min;
    }

    public Exercise(byte frequency, byte wave_form, int stimulation_sec, int rest_sec, int duration_min, List<InfoMuscolo> muscle_list) {
        this.frequency = frequency;
        this.wave_form = wave_form;
        this.stimulation_sec = stimulation_sec;
        this.rest_sec = rest_sec;
        this.duration_min = duration_min;
        this.muscle_list = muscle_list;
    }
    //COSTRUTTORE PER L'APP SMARPHONE
    public Exercise(String name, byte frequency, byte wave_form, int stimulation_sec, int rest_sec, int duration_min) {
        this.name=name;
        this.frequency = frequency;
        this.wave_form = wave_form;
        this.stimulation_sec = stimulation_sec;
        this.rest_sec = rest_sec;
        this.duration_min = duration_min;
    }

    //COSTRUTTORE PER L'APP SMARPHONE
    public Exercise(String name,byte frequency, byte wave_form, int stimulation_sec, int rest_sec, int duration_min, List<InfoMuscolo> muscle_list) {
        this.name=name;
        this.frequency = frequency;
        this.wave_form = wave_form;
        this.stimulation_sec = stimulation_sec;
        this.rest_sec = rest_sec;
        this.duration_min = duration_min;
        this.muscle_list = muscle_list;
    }

    public String getName() {
        return name;
    }

    public byte getFrequency() {
        return frequency;
    }

    public void setFrequency(byte frequency) {
        this.frequency = frequency;
    }

    public byte getWave_form() {
        return wave_form;
    }

    public void setWave_form(byte wave_form) {
        this.wave_form = wave_form;
    }

    public int getStimulation_sec() {
        return stimulation_sec;
    }

    public void setStimulation_sec(int stimulation_sec) {
        this.stimulation_sec = stimulation_sec;
    }

    public int getRest_sec() {
        return rest_sec;
    }

    public void setRest_sec(int rest_sec) {
        this.rest_sec = rest_sec;
    }

    public int getDuration_min() {
        return duration_min;
    }

    public void setDuration_min(int duration_min) {
        this.duration_min = duration_min;
    }

    public List<InfoMuscolo> getMuscle_list() {
        return muscle_list;
    }

    public void setMuscle_list(List<InfoMuscolo> muscle_list) {
        this.muscle_list = muscle_list;
    }

    public void cleanMuscle(){
        this.frequency=null;
        this.wave_form=null;
        this.stimulation_sec=null;
        this.rest_sec=null;
        this.duration_min=null;
        this.video=null;
        for (InfoMuscolo a : muscle_list){
            a.setIntensity(0);
        }

    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}
