package eu.beamdigital.beamfitnesslibrary;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanResult;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.RequiresApi;

import java.lang.reflect.Executable;
import java.util.List;
import java.util.UUID;

import static eu.beamdigital.beamfitnesslibrary.Constants.COSINE_WAVE;
import static eu.beamdigital.beamfitnesslibrary.Constants.EXPONENTIAL_WAVE;
import static eu.beamdigital.beamfitnesslibrary.Constants.SAWTOOTH_WAVE;
import static eu.beamdigital.beamfitnesslibrary.Constants.SINE_WAVE;
import static eu.beamdigital.beamfitnesslibrary.Constants.TRIANGLE_WAVE;

@RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
public class BluetoothFitnessClient extends ScanCallback {

    private static final String TAG  = BluetoothFitnessClient.class.getSimpleName();
    public static final String ACTION_BATTERY_LEVEL  = "eu.beamdigital.beamfitnesslibrary.action.BATTERY_LEVEL";
    public static final UUID BATTERY_SERVICE_UUID  = UUID.fromString("0000180f-0000-1000-8000-00805f9b34fb");
    public static final UUID BATTERY_CHARACTERISTICS_UUID  = UUID.fromString("00002a19-0000-1000-8000-00805f9b34fb");
    public static final UUID SERVICE_UUID  = UUID.fromString("0000ffe0-0000-0000-0000-000000000000");
    public static final UUID WT_UUID       = UUID.fromString("0000ffe2-0000-0000-0000-000000000000");
    public static final UUID RD_UUID       = UUID.fromString("0000ffe1-0000-0000-0000-000000000000");

    public BluetoothAdapter bluetoothAdapter;
    public BluetoothGatt mBtGatt;
    private final Context context;
    private String[] names;
    private String[] addresses;

    private ChangeBleFlagListener listener;
    public interface ChangeBleFlagListener {
        void bleFlagChanged(boolean b);
    }

    public void setListener(ChangeBleFlagListener listener) {
        this.listener = listener;
    }
    
    private final BluetoothGattCallback mGattCallback = new BluetoothGattCallback() {
        //获取连接状态方法，BLE设备连接上或断开时，会调用到此方法
        @Override
        public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
            if (status == BluetoothGatt.GATT_SUCCESS) {
                if (newState == BluetoothProfile.STATE_CONNECTED) {
                    boolean b = gatt.discoverServices();    //
                    Log.e(TAG,"Dispositivo connesso. Discovery servizi: " + b);
                    if (listener != null) {
                        listener.bleFlagChanged(true);
                    }
                } else if (newState == BluetoothProfile.STATE_DISCONNECTED) {   //断开蓝牙连接状态
                    Log.e(TAG, "Dispositivo disconnesso");
                    closeBleGatt();
                    if (listener != null) {
                        listener.bleFlagChanged(false);
                    }
                }
            } else {
                Log.e(TAG, "GATT ERROR: " + status);
                closeBleGatt();
                if (listener != null) {
                    listener.bleFlagChanged(false);
                }
            }
        }
        //成功发现设备的services时，调用此方法
        @Override
        public void onServicesDiscovered(BluetoothGatt gatt, int status) {
            if (status == BluetoothGatt.GATT_SUCCESS) {
                BluetoothGattService RxService = gatt.getService(SERVICE_UUID);
                if (RxService == null) {
                    Log.e( TAG,"Servizio non trovato");
                    return;
                }

                BluetoothGattCharacteristic TxChar = RxService.getCharacteristic(RD_UUID);
                if (TxChar == null) {
                    Log.e(TAG,"Caratteristica non trovato");
                    return;
                }

                // vengono abilitate le notifiche non si sa bene perché
                gatt.setCharacteristicNotification(TxChar, true);
                BluetoothGattDescriptor descriptor = TxChar.getDescriptor(UUID.fromString("0000ffe0-0000-0000-0000-000000000000"));
                if (descriptor == null) {
                    Log.d(TAG, "onServicesDiscovered: no descriptor");
                    return;
                }
                descriptor.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
                gatt.writeDescriptor(descriptor);

            } else {
                Log.e(TAG, "servizio ricevuto: " + status);
            }
        }
        //连接到characteristic时会调用到以下方法
        @Override
        public void onCharacteristicRead(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {

            if(characteristic.getUuid().compareTo(BATTERY_CHARACTERISTICS_UUID) == 0) {
                if(status == BluetoothGatt.GATT_SUCCESS) {
                    broadcastUpdate("BATTERY_LEVEL", characteristic);
                }
            } else {
                if (status == BluetoothGatt.GATT_SUCCESS) {
                    if (RD_UUID.equals(characteristic.getUuid())) {
                        // String text = byteToHex(characteristic.getValue());
                        // Log.e( TAG,"连接到的数据:" + text);
                    }
                }
            }
        }

        @Override
        public void onCharacteristicChanged(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) { }

        @Override
        public void onCharacteristicWrite(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
            if (WT_UUID.equals(characteristic.getUuid())) {
                String text = byteToHex(characteristic.getValue());
                Log.e( TAG,"dati scritti:" + text);
            }
        }

        @Override
        public void onDescriptorWrite(BluetoothGatt gatt, BluetoothGattDescriptor descriptor, int status) {
            super.onDescriptorWrite(gatt, descriptor, status);
        }
    };

    private void broadcastUpdate(final String action, final BluetoothGattCharacteristic characteristic) {

        final Intent intent = new Intent(action);
        Log.v(TAG, "characteristic.getStringValue(0) = " + characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, 0));
        intent.putExtra("battery", characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, 0));
        context.sendBroadcast(intent);
    }

    public BluetoothFitnessClient(Context context) {
        this.context = context;
        BluetoothManager manager  = (BluetoothManager) context.getSystemService(Context.BLUETOOTH_SERVICE);
        this.bluetoothAdapter = manager.getAdapter();
    }

    public void addNames(String ...names) {
        this.names = names;
    }

    public void addAddresses(String ...addresses) {
        this.addresses = addresses;
    }

    private boolean isScanning = false;

    public void startScan() {
        if (bluetoothAdapter == null) {
            Toast.makeText(context, R.string.no_adapter, Toast.LENGTH_SHORT).show();
            return;
        }

        if(bluetoothAdapter.isEnabled()) {

            if (isScanning) {
                Log.d(TAG, "startScan: is scanning");
                return;
            }

            isScanning = true;

            bluetoothAdapter.getBluetoothLeScanner().startScan(this);

            new Handler(Looper.myLooper()).postDelayed(() -> {
                bluetoothAdapter.getBluetoothLeScanner().stopScan(BluetoothFitnessClient.this);
                isScanning = false;
            }, 20000);
        } else {
            Toast.makeText(context, R.string.open_bluetooth, Toast.LENGTH_SHORT).show();
        }
    }

    public void closeBleGatt() {
        Log.d(TAG, "closeBleGatt: closing");
        if (mBtGatt != null) {
            mBtGatt.disconnect();
            mBtGatt.close();
            mBtGatt = null;
        }
    }

    @Override
    public void onScanResult(int callbackType, ScanResult result) {
        super.onScanResult(callbackType, result);
        BluetoothDevice device = result.getDevice();
        if (device == null) {
            Log.d(TAG, "onScanResult: no device");
            return;
        }

        String name = device.getName();
        Log.d(TAG, "onScanResult: name: " + name);
        if (names != null) {
            if (name == null) {
                return;
            }
            
            for (String n : names) {
                if (n.equals(name)) {
                    Log.d(TAG, String.format("onScanResult: connecting to %s (%s)", device.getName(), device.getAddress()));
                    mBtGatt = device.connectGatt(context, true, mGattCallback, BluetoothDevice.TRANSPORT_LE);

                    // fermiamo la scansione al primo risultato
                    bluetoothAdapter.getBluetoothLeScanner().stopScan(this);
                    return;
                }
            }
        }

        String address = device.getAddress();
        Log.d(TAG, "onScanResult: address: " + address);
        if (addresses != null) {
            if (address == null) {
                return;
            }

            for (String a : addresses) {
                if (a.equals(address)) {
                    mBtGatt = device.connectGatt(context, true, mGattCallback);
                    // fermiamo la scansione al primo risultato
                    bluetoothAdapter.getBluetoothLeScanner().stopScan(this);
                    return;
                }
            }
        }
    }


    public void getBattery() {

        BluetoothGattService batteryService = mBtGatt.getService(BATTERY_SERVICE_UUID);
        if(batteryService == null) {
            Log.d(TAG, "Battery service not found!");
            return;
        }

        BluetoothGattCharacteristic batteryLevel = batteryService.getCharacteristic(BATTERY_CHARACTERISTICS_UUID);
        if(batteryLevel == null) {
            Log.d(TAG, "Battery level not found!");
            return;
        }

        Log.v(TAG, "batteryLevel: " + mBtGatt.readCharacteristic(batteryLevel));
//        Log.v(TAG, "batteryLevel = " + mBtGatt.readCharacteristic(batteryLevel));
    }

    @Override
    public void onBatchScanResults(List<ScanResult> results) {
        super.onBatchScanResults(results);
    }

    @Override
    public void onScanFailed(int errorCode) {
        super.onScanFailed(errorCode);
    }

    public static String byteToHex(byte[] bytes) {
        String strHex = "";
        StringBuilder sb = new StringBuilder("");
        for (int n = 0; n < bytes.length; n++) {
            strHex = Integer.toHexString(bytes[n] & 0xFF);
            sb.append((strHex.length() == 1) ? "0" + strHex : strHex);
        }
        return sb.toString().trim();
    }

    synchronized public boolean writeRXCharacteristic(byte[] text) {
        boolean status = true;
        new Thread(() -> {
            try {
                Thread.sleep(100);
        if (mBtGatt == null) {
            Log.d(TAG, "writeRXCharacteristic: no btGatt");
//            return false;
            return;
        }
        BluetoothGattService service = mBtGatt.getService(SERVICE_UUID);
        if (service == null) {
            Log.d(TAG, "writeRXCharacteristic: no service");
//            return false;
            return;
        }
        BluetoothGattCharacteristic RxChar = service.getCharacteristic(WT_UUID);
        if (RxChar == null) {
            Log.d(TAG, "writeRXCharacteristic: no characteristic");
//            return false;
            return;
        }

        RxChar.setValue(text);



                boolean b = mBtGatt.writeCharacteristic(RxChar);
                Log.d(TAG, "writeRXCharacteristic: " + b);
            } catch (InterruptedException e) {
                Log.d(TAG, "run: " + e.toString());
                e.printStackTrace();
            }

        }).start();
        return status;
    }

    public void stopAllMuscles() {
        new Thread(()-> {
            try {
                final int[] muscle_library_id = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
                for(int i : muscle_library_id) {
                    Thread.sleep(60);
                    sendCommandToMuscle((byte) i, (byte) 0x00, false);
                }
                Thread.sleep(60);
                sendStopCommand();
            } catch (Exception e) {
                Log.d(TAG, "stopAllMuscles: comando non inviato");
            }
        }).start();
    }

    public void sendStartCommand(Command command) {
        new Thread(()-> {
            try {
                Thread.sleep(60);
                sendStopCommand();
                Thread.sleep(60);
                setWaveFromCommand(command.wave_form);
                Thread.sleep(60);
                setFrequency(command.frequency);
                Thread.sleep(60);
                sendCommandToMuscle(command.muscle, command.strength, true);
                Thread.sleep(60);
                sendStartCommand();
            } catch (Exception e) {
                Log.d(TAG, "sendStartCommand: comando non inviato: " + command.toString());
            }
        }).start();
    }

    private void setWaveFromCommand(byte wave) {
        switch (wave) {
            default:
            case SINE_WAVE:
                setSinWave01();
                break;
            case TRIANGLE_WAVE:
                setTrinagleWave();
                break;
            case COSINE_WAVE:
                setSineWave04();
                break;
            case SAWTOOTH_WAVE:
                setSawtoolWave();
                break;
            case EXPONENTIAL_WAVE:
                setExponentialWave();
                break;

        }
    }


    //prestart lo usiamo quando carichiamo l'esercizio e quindi ci basta dirgli quale sarà l'intensità e la forma d'onda. Ho notato che in caso di stop questi valori non vengono dimenticati
    public void preStart(Exercise exercise)
    {
        new Thread(()-> {
            try {
                Thread.sleep(60);
                setWaveFromCommand(exercise.wave_form);
                //Thread.sleep(60);
                //setFrequency(exercise.frequency);
            } catch (Exception e) {
                Log.d(TAG, "errore nel prestart");
            }
        }).start();
    }

    //metodo che viene usato ogni volta che bisogna stimolare
    public void sendStartExercise(Exercise exercise) {
        new Thread(()-> {
            try {
                //setFrequency(exercise.frequency);
                //todo: fare qualcosa di più bellinoscariche cosmic
                for(int i=0; i< exercise.muscle_list.size();i++)
                {
                    if(exercise.muscle_list.get(i).getIsActive()==true)
                    {
                        Thread.sleep(60);
                        sendCommandToMuscle(exercise.muscle_list.get(i).getTheCode(), exercise.muscle_list.get(i).getTheIntesity(), true);

                    }
                }
                Thread.sleep(60);
                sendStartCommand();
            } catch (Exception e) {
                Log.d(TAG, "errore in sendStartExercise ");
            }
        }).start();
    }


    public void sendStartCommand(){
        Log.d(TAG, "sendStartCommand: ");
        //da documentazione tabella 1
        byte[] buff=new byte[4];
        buff[0]=(byte)0xA5;
        buff[1]=(byte)0x0F; //nel doc qui troviamo un 0b
        buff[2]=(byte)0x01; //start to output waveform command (this is used under short time waverform output)
        buff[3]=(byte)0x5A;
        writeRXCharacteristic(buff);
    }


    public void sendCommandToMuscle(byte muscle, byte strength, boolean on) {
        Log.d(TAG, "sendCommandToMuscle");
        byte[] buff=new byte[10];
        buff[0]=(byte)0xA8;
        buff[1]=muscle;
        buff[2]=(byte)0x01; //setting data
        buff[3]= on ? (byte)0x01 : (byte)0x00; // questo va capito bene, lui fa delle casistiche che non mi sono chiarissime
        buff[4]=strength;
        buff[5]=(byte)0x00; //motion scheme
        buff[6]=(byte)0x00; // number of actions group
        buff[7]=(byte)0x00; // the nth group action (l'ennesimo group action?)
        buff[8]=(byte)0x00; //action repetitions times
        buff[9]=(byte)0x8A; //chiusura
        writeRXCharacteristic(buff);
    }

    public void setFrequency(byte frequency){
        Log.d(TAG, "setFrequency: ");
        if(frequency > 100)
            frequency = 100;
        if(frequency < 1)
            frequency = 1;

        //da documentazione tabella 1
        byte[] buff=new byte[3];
        buff[0]=(byte)0xA2;
        buff[1]=(byte)frequency; //nel doc qui troviamo un 0b
        buff[2]=(byte)0x2A;
        writeRXCharacteristic(buff);
    }

    public void sendStopCommand(){
        Log.d(TAG, "sendStopCommand: ");
        //da tabella 1
        byte[] buff=new byte[4];
        buff[0]=(byte)0xA5;
        buff[1]=(byte)0x0F;
        buff[2]=(byte)0x03;
        buff[3]=(byte)0x5A;
        writeRXCharacteristic(buff);
    }

    public void setWaveForm(byte msg){
        Log.d(TAG, "setSinWave01: ");
        byte[] wave = new byte[3];
        wave[0] = (byte) 0xA6;
        wave[1] = msg;
        wave[2] = (byte) 0x6A;
        writeRXCharacteristic(wave);
    }

    public void setSinWave01(){
        Log.d(TAG, "setSinWave01: ");
        byte[] wave = new byte[3];
        wave[0] = (byte) 0xA6;
        wave[1] = (byte) (0x01);//sinewave
        wave[2] = (byte) 0x6A;
        writeRXCharacteristic(wave);
    }

    public void setTrinagleWave(){
        Log.d(TAG, "setTrinagleWave: ");
        byte[] wave = new byte[3];
        wave[0] = (byte) 0xA6;
        wave[1] = (byte) (0x02);//tringelwave
        wave[2] = (byte) 0x6A;
        writeRXCharacteristic(wave);    }


    public void setSawtoolWave(){
        Log.d(TAG, "setSawtoolWave: ");
        byte[] wave = new byte[3];
        wave[0] = (byte) 0xA6;
        wave[1] = (byte) (0x08);//sawtoolwave
        wave[2] = (byte) 0x6A;
        writeRXCharacteristic(wave);    }

    public void setExponentialWave(){
        byte[] wave = new byte[3];
        wave[0] = (byte) 0xA6;
        wave[1] = (byte) (0x10);//exponentialwave
        wave[2] = (byte) 0x6A;
        writeRXCharacteristic(wave);
    }

    public void setSineWave04(){
        Log.d(TAG, "setSineWave04: ");
        byte[] wave = new byte[3];
        wave[0] = (byte) 0xA6;
        wave[1] = (byte) (0x04);//exponentialwave
        wave[2] = (byte) 0x6A;
        writeRXCharacteristic(wave);
    }

}
