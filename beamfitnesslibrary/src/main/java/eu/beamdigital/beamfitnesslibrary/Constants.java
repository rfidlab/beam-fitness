package eu.beamdigital.beamfitnesslibrary;

public class Constants {
    public static final byte SINE_WAVE = 0x01; // 1
    public static final byte TRIANGLE_WAVE = 0x02; // 2
    public static final byte COSINE_WAVE = 0x04; // 4
    public static final byte SAWTOOTH_WAVE = 0x08; // 8
    public static final byte EXPONENTIAL_WAVE = 0x10; // 16
}
