package eu.beamdigital.beamfitnesslibrary;

public class Command {
    public byte wave_form;
    public byte frequency;
    public byte muscle;
    public byte strength;

    @Override
    public String toString() {
        return "Command{" +
                "wave_form=" + wave_form +
                ", frequency=" + frequency +
                ", muscle=" + muscle +
                ", strength=" + strength +
                '}';
    }
}