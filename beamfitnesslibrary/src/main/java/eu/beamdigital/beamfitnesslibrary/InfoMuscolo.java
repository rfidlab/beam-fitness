package eu.beamdigital.beamfitnesslibrary;

public interface InfoMuscolo {


    byte getTheCode();
    byte getTheIntesity();
    boolean getIsActive();

    void setTheCode(int code);
    void setIsActive(boolean active);
    void setIntensity(int val);
}
