package eu.beamdigital.beamfitnesslibrary;

public class Muscle {
    private int iId;
    private String iName;

    public Muscle() {
    }
    public Muscle(int iId, String iName) {
        this.iId = iId;
        this.iName = iName;
    }
    public int getiId() {
        return iId;
    }
    public String getiName() {
        return iName;
    }

    public void setiId(int iId) {
        this.iId = iId;
    }

    public void setiName(String iName) {
        this.iName = iName;
    }




}
