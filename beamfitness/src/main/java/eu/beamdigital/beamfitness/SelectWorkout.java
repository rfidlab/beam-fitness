package eu.beamdigital.beamfitness;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import eu.beamdigital.beamfitness.wear.R;

public class SelectWorkout extends AppCompatActivity {
    private static final String TAG = MainActivity.class.getSimpleName();


    // tasto di collegamento

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.chose_workout);
    }


    public void avviaSelezione(View view) {

        //MUSCOLO ADDOMINALE
        byte[] buff = new byte[10];
        buff[0] = (byte) 0xA8;
        buff[1] = (byte) 0x01; //muscolo
        buff[2] = (byte) 0x01; //setting data
        buff[3] = 0x01; // questo va capito bene, lui fa delle casistiche che non mi sono chiarissime
        buff[4] = (byte) 0x32;//强度指令 istruzioni per forza (valore di intensità)
        //questi prima della chiusura stanno sempre a zero
        buff[5] = (byte) 0x00; //motion scheme
        buff[6] = (byte) 0x00; // number of actions group
        buff[7] = (byte) 0x00; // the nth group action (l'ennesimo group action?)
        buff[8] = (byte) 0x00; //action repetitions times
        buff[9] = (byte) 0x8A; //chiusura
        MainActivity.Main.writeRXCharacteristic(buff);



        //AVVIO
        MainActivity.Main.sendStartCommand();

        //ONDA
        MainActivity.Main.setSineWave01();

        startActivity(new Intent(this, Workout.class));
    }

    public void avviaPetto(View view) {


        //MUSCOLO Pettorale
        byte[] buff = new byte[10];
        buff[0] = (byte) 0xA8;
        buff[1] = (byte) 0x02; //muscolo
        buff[2] = (byte) 0x01; //setting data
        buff[3] = 0x01; // questo va capito bene, lui fa delle casistiche che non mi sono chiarissime
        buff[4] = (byte) 0x20;//强度指令 istruzioni per forza (valore di intensità)
        //questi prima della chiusura stanno sempre a zero
        buff[5] = (byte) 0x00; //motion scheme
        buff[6] = (byte) 0x00; // number of actions group
        buff[7] = (byte) 0x00; // the nth group action (l'ennesimo group action?)
        buff[8] = (byte) 0x00; //action repetitions times
        buff[9] = (byte) 0x8A; //chiusura
        MainActivity.Main.writeRXCharacteristic(buff);



        //AVVIO
        MainActivity.Main.sendStartCommand();

        //ONDA
        MainActivity.Main.setExponentialWave();

        startActivity(new Intent(this, Workout.class));
    }

    public void avviaAltro(View view) {


        //MUSCOLO Lombare
        byte[] buff = new byte[10];
        buff[0] = (byte) 0xA8;
        buff[1] = (byte) 0x04; //muscolo
        buff[2] = (byte) 0x01; //setting data
        buff[3] = 0x01; // questo va capito bene, lui fa delle casistiche che non mi sono chiarissime
        buff[4] = (byte) 0x32;//强度指令 istruzioni per forza (valore di intensità)
        //questi prima della chiusura stanno sempre a zero
        buff[5] = (byte) 0x00; //motion scheme
        buff[6] = (byte) 0x00; // number of actions group
        buff[7] = (byte) 0x00; // the nth group action (l'ennesimo group action?)
        buff[8] = (byte) 0x00; //action repetitions times
        buff[9] = (byte) 0x8A; //chiusura
        MainActivity.Main.writeRXCharacteristic(buff);

        //AVVIO
        MainActivity.Main.sendStartCommand();

        //ONDA
        MainActivity.Main.setSawtoothWave();

        startActivity(new Intent(this, Workout.class));
    }
}


