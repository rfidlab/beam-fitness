package eu.beamdigital.beamfitness;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

import eu.beamdigital.beamfitness.wear.R;
import eu.beamdigital.beamfitnesslibrary.BluetoothFitnessClient;

public class Workout extends AppCompatActivity {

    private ImageButton play;
    private ImageButton pausa;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.workout);
        play=findViewById(R.id.play);
        pausa=findViewById(R.id.pausa);
    }


    public  void pausa(View v){
        MainActivity.Main.sendStopCommand();

    }

    public  void play(View v){MainActivity.Main.sendStartCommand();
    }

    public void programma(View view) {

        byte[] buff = new byte[10];
        buff[0] = (byte) 0xA8;
        buff[1] = (byte) 0x01; //muscolo
        buff[2] = (byte) 0x01; //setting data
        buff[3] = 0x01; // questo va capito bene, lui fa delle casistiche che non mi sono chiarissime
        buff[4] = (byte) 50;//强度指令 istruzioni per forza (valore di intensità)
        //questi prima della chiusura stanno sempre a zero
        buff[5] = (byte) 0x00; //motion scheme
        buff[6] = (byte) 0x00; // number of actions group
        buff[7] = (byte) 0x00; // the nth group action (l'ennesimo group action?)
        buff[8] = (byte) 0x00; //action repetitions times
        buff[9] = (byte) 0x8A; //chiusura
        MainActivity.Main.writeRXCharacteristic(buff);
    }

    public void extra(View view) {
        byte[] buff = new byte[10];
        buff[0] = (byte) 0xA8;
        buff[1] = (byte) 10; //muscolo
        buff[2] = (byte) 0x01; //setting data
        buff[3] = 0x01; // questo va capito bene, lui fa delle casistiche che non mi sono chiarissime
        buff[4] = (byte) 20;//强度指令 istruzioni per forza (valore di intensità)
        //questi prima della chiusura stanno sempre a zero
        buff[5] = (byte) 0x00; //motion scheme
        buff[6] = (byte) 0x00; // number of actions group
        buff[7] = (byte) 0x00; // the nth group action (l'ennesimo group action?)
        buff[8] = (byte) 0x00; //action repetitions times
        buff[9] = (byte) 0x8A; //chiusura
        MainActivity.Main.writeRXCharacteristic(buff);
    }
}