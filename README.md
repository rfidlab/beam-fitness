# Introduction
Poi la scriveremo
# Gestione BLE Tuta
## Muscles
The suit has 10 stimulation areas, in the list above you can find the area code and the relative name
01 lateral abdominal
02 chest
03 left arm
04 lumbar
05 left quadriceps
06 right quadriceps
07 gluteus
08 shoulders
09 right arm
10 abdominal

## Waveform
The suit can generate 5 differents kind of waveform, in the list above you can find the waveform code and the relative name
01 SineWave
02 TriangleWave
04 SineWave
08 SawtoothWave
10 ExponentialWave
In the library there is the command setWaveForm(byte msg) and the commands to set the specific wave, for example setSinWave01()

## Frequency
Waveforms have a frequency value [0,100], this value has to be comunicated to the suit in byte format.
Using the method setFrequency(Byte frequency) we setup this value.

## Strength
The muscle areas can generate a strength from 0 to 100, also this command has to be converted in byte.
When you create an exercise the default strength values is 20/100

## connessione
Questa parte la scriveremo poi

## Commands
All the commands are in BluetoothFitnessClient.
Once you have connected to the suite, it is possibile to send commands. Above the list of interesting commands
- sendStartCommand()
  It turns on the suite
- sendCommandToMuscle(byte muscle, byte strength, boolean on)
  Using this command it is possibile to send info about a specific muscle
- sendStopCommand()
  It stops all the muscles, (it turns off the suite)
- setFrequency(Byte frequency)
  mentioned before
- setWaveForm(byte msg)
  mentioned before
- setSinWave01()
  mentioned before
- sendStartExercise(Exercise exercise)
  This method is useful to start an exercise
  1 sets the frequency (this part is commented because if you start/stop the suit does not lose this information)
  2 calls sendCommandToMuscle for every muscle of the exercise
  3 sendStartCommand

-stopAllMuscles
it sends sendCommandToMuscle((byte) i, (byte) 0x00, false); turning off the muscles

# Plan manage
A Plan is a complete workout, it is divided in 3 parts {preTraining, training e postTraininng} and it is composed of the following fields:
- name: the name of the plan
- cover: the cover picture of the plan
- preTraining: a group of exercises that composes the warm-up workout
- training: a group of exercises that composes the effective workout
- postTraining: a group of exercises that composes the cool-down workout
  Launching a plan, the software will reproduce the exercises in a specif order decided by the user
  All these information are in the class PlanDTO
## exercises
For the exercise we have the class ExerciseDTO.
In this class we have all the information useful to reproduce an exercise with the relative video/gif and the list of the activated muscles
## muscles
The classe MuscoloDTO is used in the class ExerciseDTO
Here we have the information about a specific muscle, the fields are:
- code: the code that represents the specific muscle area on the suit
- active: a flag
- intensity: [0-100] the strong intesity of that specific muscle ares
- name: the muscle area name

# Database
## Room Database
ExerciseDAO
## Firebase Realtime Database
We implemented a temporary database on firebase, in order to save:
- The cover picture of plands and exercises.
- The exercises, the plans and the relative active muscles


# REST API
non sono state realizzate perché non abbiamo un backend
ma queste sono quelle che saranno necessarie
- CRUD schede
- CRUD esercizi
- notifiche

# startExercise method
We use this method to "move" the suit, maybe the correct name should be startPlan, in fact this method manages:
- which exercise of the plan reproduce
- stimulation phase
- rest phase
- time managing for plan, exercise, stimulation and rest in case of pause
  The method is well commented.
